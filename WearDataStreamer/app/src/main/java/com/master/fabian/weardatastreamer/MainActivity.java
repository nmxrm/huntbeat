// based on: https://gist.github.com/mjohnsullivan/557c2f19ba177312b1d7

package com.master.fabian.weardatastreamer;

/*import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.widget.TextView;

public class MainActivity extends WearableActivity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text);

        // Enables Always-on
        setAmbientEnabled();
    }
}*/

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
//import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends Activity implements SensorEventListener {

    private static final String TAG = "MainActivity";
    private TextView mTextViewStepCount;
    private TextView mTextViewStepDetect;
    private TextView mTextViewHeart;
    private TextView mDebug;


    private MulticastSocket socket;

   private UdpServerThread udpThread;

    //private boolean send = true;

    private final String multicastIp = "239.255.42.99";

    private final int port = 1511;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Keep the Wear screen always on (for testing only!)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextViewStepCount = (TextView) stub.findViewById(R.id.step_count);
                mTextViewStepDetect = (TextView) stub.findViewById(R.id.step_detect);
                mTextViewHeart = (TextView) stub.findViewById(R.id.heart);
                getStepCount();
            }
        });*/
        mTextViewHeart = (TextView) findViewById(R.id.heartbeat);
        mTextViewStepCount = (TextView) findViewById(R.id.stepcount);
        mTextViewStepDetect = (TextView) findViewById(R.id.stepdetection);
        // mDebug = (TextView) findViewById(R.id.debug);


        Button quitButton = findViewById(R.id.quitButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                udpThread.interrupt();
                onDestroy();
                finishAndRemoveTask();
            }
        });

        SetupSensors();
     //   SetupUdpServer();

        udpMessage = "";

        udpThread = new UdpServerThread();
        udpThread.start();

        counter = 0;
    }

    private void SetupSensors() {
        SensorManager mSensorManager = ((SensorManager) getSystemService(SENSOR_SERVICE));
        Sensor mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        Sensor mStepCountSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor mStepDetectSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        mSensorManager.registerListener(this, mHeartRateSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mStepCountSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mStepDetectSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private String currentTimeStr() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(c.getTime());
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged - accuracy: " + accuracy);
    }

    int counter;

    String udpMessage;

    public void onSensorChanged(SensorEvent event) {
        counter++;
        // mDebug.setText("times called: " + counter);

        if (event.sensor.getType() == Sensor.TYPE_HEART_RATE) {
            String msg = "Heartbeat: " + (int) event.values[0];
            mTextViewHeart.setText(msg);
            Log.d(TAG, msg);
                udpMessage=String.format("ibi:%s;%s#", (int) event.values[0], 0);

        } else if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            String msg = "Stepcount: " + (int) event.values[0];
            mTextViewStepCount.setText(msg);
            Log.d(TAG, msg);
        } else if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            String msg = "Detected at " + currentTimeStr();
            mTextViewStepDetect.setText(msg);
            Log.d(TAG, msg);
        } else
            Log.d(TAG, "Unknown sensor type");
    }

    //InetAddress group;

  /*  private void SetupUdpServer() {


        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiManager.MulticastLock multicastLock = wm.createMulticastLock("mydebuginfo");
        multicastLock.acquire();

        try {

            group = InetAddress.getByName(multicastIp);
            socket = new MulticastSocket(port);
            socket.joinGroup(group);
            // SocketAddress socketAdr = InetSocketAddress.createUnresolved("239.255.42.99", 1511);
            //s.bind(socketAdr);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void SendMessage(String msg) throws IOException {
        DatagramPacket messagePacket = new DatagramPacket(msg.getBytes(), msg.length(),
                group, port);

        socket.send(messagePacket);
    }*/


    private class UdpServerThread extends Thread {

        public UdpServerThread() {
            super();
        }

        @Override
        public void run() {

            InetAddress group;

            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiManager.MulticastLock multicastLock = wm.createMulticastLock("mydebuginfo");
            multicastLock.acquire();

            try {

                group = InetAddress.getByName(multicastIp);
                socket = new MulticastSocket(port);
                socket.joinGroup(group);
                // SocketAddress socketAdr = InetSocketAddress.createUnresolved("239.255.42.99", 1511);
                //s.bind(socketAdr);

                while (!interrupted()) {

                    if (udpMessage!="") {
                        DatagramPacket messagePacket = new DatagramPacket(udpMessage.getBytes(), udpMessage.length(),
                                group, port);

                        //MainActivity.Instance.updateStatusLabel("Sending message");

                        socket.send(messagePacket);
                        udpMessage = "";
                    }
                    //  MainActivity.Instance.updateStatusLabel("Sleeping ...");
                  //  Thread.sleep(50);
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

