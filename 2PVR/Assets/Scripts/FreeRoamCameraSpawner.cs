﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeRoamCameraSpawner : MonoBehaviour
{

    public GameObject CameraPrefab;

    private GameObject InstantiatedCam = null;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (InstantiatedCam == null)
            {
                InstantiatedCam = Instantiate(CameraPrefab);
            }
            else
            {
                Destroy(InstantiatedCam);
                InstantiatedCam = null;
            }
        }
    }
}
