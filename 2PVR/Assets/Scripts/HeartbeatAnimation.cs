﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartbeatAnimation : MonoBehaviour
{
    public static HeartbeatAnimation Instance;

    private Vector3 DefaultScale;
    private Vector3 BeatScale;

    void Update()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void OnDisable()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }

    void Start()
    {
        DefaultScale = transform.localScale;
        BeatScale = DefaultScale + new Vector3(0.1f, 0.1f, 0.1f);
    }

    [ContextMenu("Start animation")]
    public void StartHeartbeatAnimation(float heartbeat)
    {
        float percentage = 1;

        if (heartbeat < 90)
        {
            percentage = 1f;
        }
        else if (heartbeat >= 90 && heartbeat <= 100)
        {
            percentage = 0.9f;
        }
        else if (heartbeat > 100 && heartbeat <= 110)
        {
            percentage = 0.8f;
        }
        else if (heartbeat > 110 && heartbeat <= 125)
        {
            percentage = 0.7f;
        }
        else if (heartbeat > 125 && heartbeat <= 140)
        {
            percentage = 0.5f;
        }
        else if (heartbeat > 140 && heartbeat <= 165)
        {
            percentage = 0.4f;
        }
        else if (heartbeat > 165)
        {
            percentage = 0.3f;
        }

        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(FullHeartbeatAnimation(percentage));

        }
    }

    IEnumerator FullHeartbeatAnimation(float percentage)
    {
        yield return StartCoroutine(ScaleAnimation(0.15f * percentage, BeatScale));
        yield return StartCoroutine(ScaleAnimation(0.1f * percentage, DefaultScale));
        yield return StartCoroutine(ScaleAnimation(0.15f * percentage, BeatScale));
        yield return StartCoroutine(ScaleAnimation(0.15f * percentage, DefaultScale));
    }

    IEnumerator ScaleAnimation(float time, Vector3 endScale)
    {
        float i = 0;
        float rate = 1 / time;

        Vector3 startScale = transform.localScale;

        while (i < 1)
        {
            transform.localScale = Vector3.Lerp(startScale, endScale, i);
            i += rate * Time.deltaTime;
            yield return null;
        }

        transform.localScale = endScale;
    }
}
