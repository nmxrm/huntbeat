﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSoundPlayed : MonoBehaviour
{
    private AudioSource Audio;

    private bool WasPlaying = false;

    void Start()
    {
        Audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (WasPlaying && !Audio.isPlaying)
        {
            Destroy(gameObject);
        }

        WasPlaying = Audio.isPlaying;
    }
}
