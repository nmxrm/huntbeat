﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PitchRandomizer : MonoBehaviour
{

    public float MinPitch;
    public float MaxPitch;

    void Awake()
    {
        GetComponent<AudioSource>().pitch = Random.Range(MinPitch, MaxPitch);
    }
}
