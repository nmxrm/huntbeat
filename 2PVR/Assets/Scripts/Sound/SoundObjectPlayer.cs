﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SoundObjectPlayer : MonoBehaviour
{
    public static SoundObjectPlayer Instance;

    public List<string> SoundKeys = new List<string>();

    public List<AudioSource> SoundValues = new List<AudioSource>();

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        if (NetworkManager.singleton)
        {
            foreach (var sound in SoundValues)
            {
                NetworkManager.singleton.spawnPrefabs.Add(sound.gameObject);
            } 
        }
    }

    [ContextMenu("Play test networked")]
    public void PlayTestSoundNetworked()
    {
        PlaySound("test", GameManagerInteractor.Instance.transform.position, true);
    }

    public void PlaySound(string key, Vector3 position, bool networked)
    {
        if (networked)
        {
            GameManagerInteractor.Instance.CmdSpawnSoundEverywhere(key, position);
        }
        else
        {
            var index = SoundKeys.IndexOf(key);

            if (index == -1)
            {
                Debug.LogErrorFormat("Sound key: {0} does not exist!");
                return;
            }

            var sound = Instantiate(SoundValues[index].gameObject, position, Quaternion.identity).GetComponent<AudioSource>();

            sound.Play(); 
        }
    }
}
