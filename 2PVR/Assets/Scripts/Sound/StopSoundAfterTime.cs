﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class StopSoundAfterTime : MonoBehaviour
{
    public float Time;

    private AudioSource Source;

    void Awake()
    {
        Source = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Source.isPlaying)
        {
            if (Source.time >= Time) 
            {
                Source.Stop();
            }
        }
    }
}
