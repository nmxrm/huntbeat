﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class RandomFootstepPlayer : NetworkBehaviour
{
    [SerializeField]
    private List<AudioClip> Sounds = new List<AudioClip>();

    private AudioSource Source;

    private Player ConnectedPlayer;

    void Start()
    {
        Source = GetComponent<AudioSource>();

        ConnectedPlayer = GetComponent<Player>();
    }

    private void PlayRandomFootstep()
    {
        if (ConnectedPlayer.playerType == Player.PlayerType.Hunter)
        {
            Source.pitch = Random.Range(0.95f, 1.1f);
            Source.PlayOneShot(Sounds[Random.Range(0, Sounds.Count - 1)]);
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        Source.mute = !focus;
    }


}
