﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class StartSoundAtRandomPoint : MonoBehaviour
{

    void Start()
    {
        var source = GetComponent<AudioSource>();
        source.time = Random.Range(0, source.clip.length);
        source.Play();
    }

}
