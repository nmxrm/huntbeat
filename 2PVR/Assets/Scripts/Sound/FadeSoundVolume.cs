﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FadeSoundVolume : MonoBehaviour
{

    public AnimationCurve VolumeCurve;

    public float CustomLength = -1;

    private AudioSource Source;

    private float MaxVolume;
    private float Length;

    void Start()
    {
        Source = GetComponent<AudioSource>();
        MaxVolume = Source.volume;
        if (CustomLength < 0)
        {
            Length = Source.clip.length;
        }
        else
        {
            Length = CustomLength;
        }
    }

    void Update()
    {
        if (Source.isPlaying)
        {
            var percent = Source.time / Length;
            Source.volume = MaxVolume * VolumeCurve.Evaluate(percent);
        }
    }

}
