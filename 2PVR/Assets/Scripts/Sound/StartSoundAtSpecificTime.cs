﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class StartSoundAtSpecificTime : MonoBehaviour
{
    public float Time;

    void Awake()
    {
        GetComponent<AudioSource>().time = Time;
    }

}
