﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MuteOnFocusLost : MonoBehaviour
{
    public AudioMixer Mixer;

    private float ValueBeforeMute;

    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            Mixer.GetFloat("Volume", out ValueBeforeMute);
            Mixer.SetFloat("Volume", -80f); 
        }
        else
        {
            Mixer.SetFloat("Volume", ValueBeforeMute);
        }
    }


}
