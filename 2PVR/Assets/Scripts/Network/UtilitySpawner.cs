﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UtilitySpawner : NetworkBehaviour
{
    public GameManager UtilityPrefab;

    public override void OnStartServer()
    {
        var util = Instantiate(UtilityPrefab.gameObject);
        NetworkServer.Spawn(util);
    }
}
