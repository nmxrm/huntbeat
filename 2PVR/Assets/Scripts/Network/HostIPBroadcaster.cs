﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using ProtoBuf;
using UnityEngine;

public class HostIPBroadcaster : MonoBehaviour
{
    public static HostIPBroadcaster Instance;

    public int Port = 15000;

    public float MessageDelay = 3f;

    //private string mMatchName;

    private UdpClient mClient;

    private IPEndPoint mIp;

    private byte[] SerializedGameInfo;

    private Coroutine SendingRoutine;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    [ContextMenu("Start")]
    private void SartSendingIp()
    {
        StartSending();
    }

    public void StopSending()
    {
        if (SendingRoutine != null)
        {
            StopCoroutine(SendingRoutine);
        }

        mClient.Client.Close();
        mClient.Close();
        ((IDisposable)mClient).Dispose();

        mClient = new UdpClient();
    }

    public void StartSending()
    {
        mClient = new UdpClient();
        mIp = new IPEndPoint(IPAddress.Broadcast, Port);

        //mMatchName = matchName;
        SerializedGameInfo = SerializeGameInfo();

        SendingRoutine = StartCoroutine(Send());
    }

    private byte[] SerializeGameInfo()
    {
        var ms = new MemoryStream();
        Serializer.Serialize(ms, GameSettings.Instance.Info);

        return ms.ToArray();
    }

    IEnumerator Send()
    {
        //string localIp = GetLocalIPAddress();

        //string msg = string.Format("{0};{1};{2};{3}", mMatchName, localIp, (int)GameSettings.Instance.SelectedPlayerType, GameSettings.Instance.GameFull);

        //byte[] bytes = Encoding.ASCII.GetBytes(msg);

        while (true)
        {
            mClient.Send(SerializedGameInfo, SerializedGameInfo.Length, mIp);

            yield return new WaitForSeconds(MessageDelay);
        }
    }

    public string GetLocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());

        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }

        return localIP;
    }
}

[ProtoContract]
public class GameInfo
{
    [ProtoMember(1)]
    public string GameName;
    [ProtoMember(2)]
    public string IpAdress;
    [ProtoMember(3)]
    public int SelectedPlayerType;
    //[ProtoMember(4)]
    //public bool GameFull;
}