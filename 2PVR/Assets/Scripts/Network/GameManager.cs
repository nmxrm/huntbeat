﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    //[SyncVar/*(hook = "UpdateHunterDebugString")*/]
    private NetworkInstanceId hunterId;

    public NetworkInstanceId HunterId
    {
        get { return hunterId; }
        set
        {
            hunterId = value;
            PlayerJoined(hunterId);
        }
    }
    //[SyncVar/*(hook = "UpdatePreyDebugString")*/]

    private NetworkInstanceId preyId;

    public NetworkInstanceId PreyId
    {
        get { return preyId; }
        set
        {
            preyId = value;
            PlayerJoined(preyId);
        }
    }


    public bool IgnorePlayersReady;

    [SerializeField]
    private string DEBUG_HunterId, DEBUG_PreyId;

    private Coroutine CatchingRoutine;

    [SerializeField]
    private float CatchingCountdownLength = 5f;

    private int CatchingOccuredAmount = 0;
    //private NetworkIdentity Identity;

    //private GameObject HunterObj;
    //private GameObject PreyObj;

    private bool HunterReady;
    private bool PreyReady;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //if (!Application.isEditor)
        //{
        //    if (GameSettings.Instance.DebugMode)
        //    {
        //        IgnorePlayersReady = true;
        //    }
        //    else
        //    {
        //        IgnorePlayersReady = false;
        //    }
        //}

        //Identity = GetComponent<NetworkIdentity>();
    }

    //void Update()
    //{
    //    if (GameManagerInteractor.Instance != null)
    //    {
    //        GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Hunter: " + HunterId);
    //        GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Prey: " + PreyId);
    //    }

    //}

    private void PlayerJoined(NetworkInstanceId playerId)
    {
        if (!IgnorePlayersReady)
        {
            var player = NetworkServer.FindLocalObject(playerId).GetComponent<Player>();
            player.TargetBlockMovement(player.connectionToClient, true);


            //if (player.playerType == Player.PlayerType.Hunter)
            //{
            //    player.TargetPrintMessageToPlayerTypeDependent(player.GetComponent<NetworkIdentity>().connectionToClient, "Waiting for other player!", -1.5f, false);
            //}
        }

        if (!PreyId.IsEmpty() && !HunterId.IsEmpty())
        {
            var prey = NetworkServer.FindLocalObject(PreyId).GetComponent<Player>();
            prey.TargetAllPlayersJoined(prey.GetComponent<NetworkIdentity>().connectionToClient);

            var hunter = NetworkServer.FindLocalObject(HunterId).GetComponent<Player>();
            hunter.TargetAllPlayersJoined(hunter.GetComponent<NetworkIdentity>().connectionToClient);
        }
    }

    [ContextMenu("Set all players ready")]
    private void SetAllPlayersReady()
    {
        SetPlayerReady(Player.PlayerType.Hunter);
        SetPlayerReady(Player.PlayerType.Prey);
    }

    public void SetPlayerReady(Player.PlayerType pType)
    {
        switch (pType)
        {
            case Player.PlayerType.Hunter:
                HunterReady = true;
                break;
            case Player.PlayerType.Prey:
                PreyReady = true;
                break;
        }

        if (HunterReady && PreyReady)
        {
            var prey = NetworkServer.FindLocalObject(PreyId).GetComponent<Player>();
            prey.GetComponent<Player>()
                .TargetStartGameOnPlayer(prey.GetComponent<NetworkIdentity>().connectionToClient);


            var hunter = NetworkServer.FindLocalObject(HunterId).GetComponent<Player>();
            hunter.TargetStartGameOnPlayer(hunter.GetComponent<NetworkIdentity>().connectionToClient);
        }
    }

    public void StartCatchingCountdown()
    {
        if (GameSettings.Instance != null && GameSettings.Instance.DebugMode)
        {
            CatchingRoutine = StartCoroutine(CatchingCountdownRoutine(60f));
        }
        else
        {
            if (CatchingOccuredAmount == 0)
            {
                CatchingRoutine = StartCoroutine(CatchingCountdownRoutine(CatchingCountdownLength));
            }
            else if (CatchingOccuredAmount == 1)
            {
                CatchingRoutine = StartCoroutine(CatchingCountdownRoutine(Mathf.FloorToInt(CatchingCountdownLength * 0.75f)));
            }
            else if (CatchingOccuredAmount == 2)
            {
                CatchingRoutine = StartCoroutine(CatchingCountdownRoutine(Mathf.FloorToInt(CatchingCountdownLength * 0.5f)));
            }
            else if (CatchingOccuredAmount == 3)
            {
                CatchingRoutine = StartCoroutine(CatchingCountdownRoutine(Mathf.FloorToInt(CatchingCountdownLength * 0.25f)));
            }
            else if (CatchingOccuredAmount > 3)
            {
                CatchingRoutine =
                    StartCoroutine(CatchingCountdownRoutine(Mathf.FloorToInt(CatchingCountdownLength * 0.10f)));
            }
        }

        CatchingOccuredAmount++;
    }

    private IEnumerator CatchingCountdownRoutine(float length)
    {
        var prey = NetworkServer.FindLocalObject(PreyId).GetComponent<Player>();
        prey.TargetStartCountdownPlayerDependant(prey.connectionToClient, length - 1);
        var hunter = NetworkServer.FindLocalObject(HunterId).GetComponent<Player>();
        hunter.TargetStartCountdownPlayerDependant(hunter.connectionToClient, length);

        yield return new WaitForSeconds(length);

        prey.TargetDestroyAllChains(prey.connectionToClient);

        CatchingEndedHunter(false);
        prey.TargetSetActiveCatchingWarning(prey.connectionToClient, false);
        prey.TargetStopCountdownPlayerDependant(prey.connectionToClient);
        yield return new WaitForSeconds(1f);
        prey.TargetPrintMessageToPlayerTypeDependent(prey.connectionToClient, "Got caught! You lose! Thanks for playing!", -1.5f, false, false, 0f);
        prey.TargetDisconnectAfterTime(prey.connectionToClient, 15f);
        //GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Prey caught! (-1 life/Hunter wins?)");
    }

    public void StopCatchingRoutine()
    {
        if (CatchingRoutine != null)
        {
            StopCoroutine(CatchingRoutine);
        }

        var prey = NetworkServer.FindLocalObject(PreyId).GetComponent<Player>();
        prey.RpcResetCatching();
        prey.GetComponent<FirstPersonControls>().BlockMovement = false;
        prey.TargetBlockMovement(prey.connectionToClient, false);
        prey.TargetStopCountdownPlayerDependant(prey.connectionToClient);
        prey.TargetPrintMessageToPlayerTypeDependent(prey.connectionToClient, "You broke free! Get to cover!", 5f, false, true, 1f);
        prey.TargetSetInvisible(prey.connectionToClient, 15f);
        prey.TargetSetActiveCatchingWarning(prey.connectionToClient, false);

        CatchingEndedHunter(true);
    }

    private void CatchingEndedHunter(bool failed)
    {
        var hunterObj = NetworkServer.FindLocalObject(HunterId);
        var hunterPlayer = hunterObj.GetComponent<Player>();
        //var hunterId = hunterObj.GetComponent<NetworkIdentity>();

        hunterPlayer.CatchingEnded(/*hunterId.connectionToClient*/);
        //hunterPlayer.RpcResetPistolAnimation();
        hunterPlayer.TargetRemoveCapsuleStartCooldownUiAnimation(hunterPlayer.connectionToClient, hunterPlayer.CatchingCooldownTime, true);
        hunterPlayer.TargetStopCountdownPlayerDependant(hunterPlayer.connectionToClient);
        if (failed)
        {
            hunterPlayer.GetComponent<FirstPersonControls>().BlockMovement = false;
            hunterPlayer.TargetPrintMessageToPlayerTypeDependent(hunterPlayer.connectionToClient, "Catching failed: Prey broke free!", 5f, false, false, 0f);
        }
        else
        {
            hunterPlayer.TargetPrintMessageToPlayerTypeDependent(hunterPlayer.connectionToClient, "Success! You win! Thanks for playing!", -1.5f, false, false, 0f);
            hunterPlayer.TargetDisconnectAfterTime(hunterPlayer.connectionToClient, 15f);
        }
    }

    public void SetPreyId(NetworkIdentity id)
    {
        PreyId = id.netId;
        DEBUG_PreyId = string.Empty + PreyId.Value;

        //PreyObj = NetworkServer.FindLocalObject(PreyId);
    }

    public void SetHunterId(NetworkIdentity id)
    {
        HunterId = id.netId;
        DEBUG_HunterId = string.Empty + HunterId.Value;

        //HunterObj = NetworkServer.FindLocalObject(HunterId);
    }

    [ContextMenu("Print ids")]
    public void PrintIds()
    {
        GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Hunter: " + HunterId);
        GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Prey: " + PreyId);
    }

    public void PrintOnClient(Player.PlayerType playerType, string msg)
    {
        switch (playerType)
        {
            case Player.PlayerType.Hunter:
                var hunterInteractor = NetworkServer.FindLocalObject(HunterId).GetComponent<GameManagerInteractor>();
                hunterInteractor.TargetPrintGmMessageOnClient(hunterInteractor.GetComponent<NetworkIdentity>().connectionToClient, msg);
                break;
            case Player.PlayerType.Prey:
                var preyInteractor = NetworkServer.FindLocalObject(PreyId).GetComponent<GameManagerInteractor>();
                preyInteractor.TargetPrintGmMessageOnClient(preyInteractor.GetComponent<NetworkIdentity>().connectionToClient, msg);
                break;
        }
    }

    public void AllKeysSubmitted()
    {
        GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Game ended, prey wins!");

        var hunter = NetworkServer.FindLocalObject(HunterId).GetComponent<Player>();
        if (hunter != null)
        {
            hunter.TargetBlockMovement(hunter.connectionToClient, true);
            hunter.TargetPrintMessageToPlayerTypeDependent(hunter.connectionToClient, "The prey escaped! You lose!", -1.5f, false, false, 0f);
        }

        var prey = NetworkServer.FindLocalObject(PreyId).GetComponent<Player>();
        prey.TargetBlockMovement(prey.connectionToClient, true);
        prey.TargetPrintMessageToPlayerTypeDependent(prey.connectionToClient, "The gate is open! You win!", -1.5f, false, false, 0f);

        hunter.TargetDisconnectAfterTime(hunter.connectionToClient, 15f);
        prey.TargetDisconnectAfterTime(prey.connectionToClient, 15f);
    }

    public void SendHunterToastMessage(string msg, float time)
    {
        var hunter = NetworkServer.FindLocalObject(HunterId);
        if (hunter != null)
        {
            hunter.GetComponent<Player>().TargetPrintHunterToastMessage(hunter.GetComponent<NetworkIdentity>().connectionToClient, msg, time);
        }
    }
}
