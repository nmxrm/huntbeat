﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManagerInteractor : NetworkBehaviour
{
    public static GameManagerInteractor Instance;

    private GameManager GmInstance;

    private Coroutine EscapeRoutine;

    void Awake()
    {
        //if (isLocalPlayer)
        //{
        Instance = this;
        //}
    }

    void Start()
    {
        if (isServer)
        {
            GmInstance = GameManager.Instance;
        }
    }

    [Command]
    public void CmdStopCatchingRoutine()
    {
        GmInstance.StopCatchingRoutine();
    }

    //[Command]
    //public void CmdStartCatchingCountdown()
    //{
    //    GmInstance.StartCatchingCountdown();
    //}

    [Command]
    public void CmdSetPreyId(NetworkIdentity id)
    {
        if (GmInstance == null)
        {
            StartCoroutine(SetPreyIdRoutine(id));
        }
        else
        {
            GmInstance.SetPreyId(id);
        }
    }

    private IEnumerator SetPreyIdRoutine(NetworkIdentity id)
    {
        while (GmInstance == null)
        {
            yield return null;
        }

        GmInstance.SetPreyId(id);
    }

    private IEnumerator SetHunterIdRoutine(NetworkIdentity id)
    {
        while (GmInstance == null)
        {
            yield return null;
        }

        GmInstance.SetHunterId(id);
    }

    [Command]
    public void CmdSetHunterId(NetworkIdentity id)
    {
        if (GmInstance == null)
        {
            StartCoroutine(SetHunterIdRoutine(id));
        }
        else
        {
            GmInstance.HunterId = id.netId;
        }
    }

    void Update()
    {
        //RpcPrintGmMessageOnClients();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (EscapeRoutine == null)
            {
                EscapeRoutine = StartCoroutine(DisconnectAfterEscapeHeld());
            }
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (EscapeRoutine != null)
            {
                StopCoroutine(EscapeRoutine);
                EscapeRoutine = null;

                GetComponent<Player>().SetActiveDisconnectLabel(false);
            }
        }
    }

    IEnumerator DisconnectAfterEscapeHeld()
    {
        GetComponent<Player>().SetActiveDisconnectLabel(true);

        yield return new WaitForSeconds(3f);

        if (isServer)
        {
            CustomNetworkManager.singleton.StopHost();
            HostIPBroadcaster.Instance.StopSending();
        }
        else
        {
            CustomNetworkManager.singleton.StopClient();
        }

        E4DataRecorder.Instance.StopRecording();

        EscapeRoutine = null;
    }

    IEnumerator DisconnectOnEscapePressedThriceRoutine()
    {
        float startTime = Time.time;

        int escapePushed = 0;

        while (Time.time < startTime + 3f)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                escapePushed++;

                if (escapePushed >= 3)
                {
                    if (isServer)
                    {
                        CustomNetworkManager.singleton.StopHost();
                        HostIPBroadcaster.Instance.StopSending();
                    }
                    else
                    {
                        CustomNetworkManager.singleton.StopClient();
                    }
                }
            }

            yield return null;
        }

        EscapeRoutine = null;
    }

    [ClientRpc]
    public void RpcPrintGmMessageOnClients(string msg)
    {
        Debug.Log(msg);
    }

    [TargetRpc]
    public void TargetPrintGmMessageOnClient(NetworkConnection connection, string msg)
    {
        Debug.Log(msg);
    }

    [Command]
    public void CmdAllKeysSubmitted()
    {
        GameManager.Instance.AllKeysSubmitted();
        RpcPlayDoorAnimationOnClients();
    }

    [ClientRpc]
    private void RpcPlayDoorAnimationOnClients()
    {
        ExitGate.Instance.PlayDoorAnimationsOnAllKeysSubmitted();
    }

    [Command]
    public void CmdSendHunterToastMessage(string msg, float time)
    {
        GameManager.Instance.SendHunterToastMessage(msg, time);
    }

    [ContextMenu("TEST send hunter toast message")]
    private void TestSendHunterToast()
    {
        CmdSendHunterToastMessage("Test message from client!", -1.5f);
    }

    [ContextMenu("Request player ids")]
    private void RequestPlayerIds()
    {
        CmdRequestPlayerIds();
    }

    [Command]
    private void CmdRequestPlayerIds()
    {
        GameManager.Instance.PrintIds();
    }

    [Command]
    public void CmdSetPlayerReady(int pType)
    {
        GameManager.Instance.SetPlayerReady((Player.PlayerType)pType);
    }

    [Command]
    public void CmdSetStationDoneEverywhere(GameObject stationObject)
    {
        RpcSetStationDoneOnClient(stationObject);
    }

    [ClientRpc]
    private void RpcSetStationDoneOnClient(GameObject stationObject)
    {
        stationObject.GetComponent<Station>().SetStationDone();
    }

    [Command]
    public void CmdSpawnSoundEverywhere(string key, Vector3 position)
    {
        RpcSpawnSoundOnClients(key, position);
    }

    private void RpcSpawnSoundOnClients(string key, Vector3 position)
    {
        SoundObjectPlayer.Instance.PlaySound(key, position, false);
    }

    [Command]
    public void CmdSetBarAnimationEverywhere(int keyColor)
    {
        RpcSetBarAnimationOnClient(keyColor);
    }

    [ClientRpc]
    private void RpcSetBarAnimationOnClient(int keyColor)
    {
        ExitGate.Instance.SetBarAnimation((Key.KeyType)keyColor);
    }

    [Command]
    public void CmdSetIgnorePlayersReady(bool state)
    {
        GameManager.Instance.IgnorePlayersReady = state;
    }
}
