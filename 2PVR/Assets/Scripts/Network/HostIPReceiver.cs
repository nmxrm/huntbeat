﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using ProtoBuf;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class HostIPReceiver : MonoBehaviour
{
    public static HostIPReceiver Instance;

    public int RefreshTimoutTime = 5;

    private Dictionary<string, GameInfo> mReceivedMessages = new Dictionary<string, GameInfo>();

    private UdpClient mClient;

    private Coroutine mReceiveIPRoutine;

    private bool FirstStart = true;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        mClient = new UdpClient();
    }

    public void StartLookingForGames()
    {
        //IPAddress ip = IPAddress.Parse(mMulticastIP);
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, HostIPBroadcaster.Instance.Port);

        //if (FirstStart)
        //{
            mClient.Client.ExclusiveAddressUse = false;
            //FirstStart = false;

            // Otherwise the game is blocked until the client receives a message. 
            // Without blocking it throws an exception when not receiving a message.
            mClient.Client.Blocking = false;
            mClient.Client.Bind(RemoteIpEndPoint);
        //}


        //mClient.JoinMulticastGroup(ip);

        Debug.Log("Looking for host in LAN!");
        mReceivedMessages.Clear();

        mReceiveIPRoutine = StartCoroutine(LookForRunningGames(RemoteIpEndPoint));
        //StartCoroutine(ReconnectRoutine());
    }

    public void StopLookingForGames()
    {
        if (mReceiveIPRoutine != null)
        {
            StopCoroutine(mReceiveIPRoutine);
        }

        mReceiveIPRoutine = null;

        mClient.Client.Close();
        mClient.Close();
        ((IDisposable)mClient).Dispose();

        mClient = new UdpClient();
    }

    //IEnumerator ReconnectRoutine()
    //{
    //    while (Network.connections.Length < 1 && mReceiveIPRoutine != null)
    //    {
    //        yield return null;
    //    }

    //    Debug.Log("Trying to reconnect!");

    //    StartReceivingHostIp();
    //    StartCoroutine(ReconnectRoutine());
    //}

    private IEnumerator LookForRunningGames(IPEndPoint endPoint)
    {
        float starTime = Time.time;

        while (Time.time <= starTime + RefreshTimoutTime)
        {
            try
            {
                var bytes = mClient.Receive(ref endPoint);

                //string msg = Encoding.ASCII.GetString(bytes);

                //Debug.Log("Received host message: " + msg);
                var stream = new MemoryStream(bytes);
                var msg = Serializer.Deserialize<GameInfo>(stream);

                if (msg != null)
                {
                    //NetworkManager.singleton.networkAddress = ip;
                    //NetworkManager.singleton.StartClient();

                    //Debug.Log("Connected to host!");
                    //mReceivedMessages.Add(msg);
                    AddMessage(msg);
                }
            }
            catch (SocketException)
            {


            }

            yield return null;
        }

        StopLookingForGames();
    }

    private void AddMessage(GameInfo msg)
    {
        if (mReceivedMessages.ContainsKey(msg.IpAdress))
        {
            mReceivedMessages[msg.IpAdress] = msg;
        }
        else
        {
            mReceivedMessages.Add(msg.IpAdress, msg);
        }
    }

    public List<GameInfo> GetMessagesValues()
    {
        return mReceivedMessages.Values.ToList();
    }

    public Dictionary<string, GameInfo> GetMessages()
    {
        return mReceivedMessages;
    }
}
