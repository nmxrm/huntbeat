﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Valve.VR;

public class CustomNetworkManager : NetworkManager
{
    //public NetworkInstanceId HunterId, PreyId;

    //private Coroutine CatchingRoutine;

    //[Command]
    //public void CmdStartCatchingCountdown(float length)
    //{
    //    StartCoroutine(CatchingCountdownRoutine(length));
    //}

    //private IEnumerator CatchingCountdownRoutine(float length)
    //{
    //    yield return new WaitForSeconds(length);

    //    NetworkServer.FindLocalObject(PreyId).GetComponent<FirstPersonControls>().BlockMovement = false;
    //}

    //[Command]
    //public void CmdSetHunterId(NetworkInstanceId id)
    //{
    //    HunterId = id;
    //}

    //[Command]
    //public void CmdSetPreyId(NetworkInstanceId id)
    //{
    //    PreyId = id;
    //}

    //public static CustomNetworkManager GetSingleton()
    //{
    //    return singleton as CustomNetworkManager;
    //}

    //public GameManager utilityPrefab;

    //public override void OnStartServer()
    //{
    //    var util = Instantiate(UtilityPrefab.gameObject);
    //    NetworkServer.Spawn(util);
    //}

    public override void OnStartHost()
    {
        base.OnStartServer();

        StartCoroutine(SpawnStationsWhenPrefabHandlerExists());

        PlayerCount = 0;

        //StartCoroutine(StationPrefabHandler.Instance.SpawnStationsOnStart());

        //StartCoroutine(StationPrefabHandler.Instance.SpawnOnlyStationsOfType<MovementInTimeStation>());
    }

    private IEnumerator SpawnStationsWhenPrefabHandlerExists()
    {
        while (StationPrefabHandler.Instance == null)
        {
            yield return null;
        }

        switch (GameSettings.Instance.SpawnBehaviour)
        {
            case StationPrefabHandler.StationSpawnBehaviour.Default:
                StartCoroutine(StationPrefabHandler.Instance.SpawnStationsOnStart());
                break;
            case StationPrefabHandler.StationSpawnBehaviour.SpawnSpecificStationEverywhere:
                switch (GameSettings.Instance.SpawnOnlyType)
                {
                    case StationPrefabHandler.StationType.DestroyInTime:
                        StartCoroutine(StationPrefabHandler.Instance.SpawnOnlyStationsOfType<DestroyInTimeStation>());
                        break;
                    case StationPrefabHandler.StationType.MovementInTime:
                        StartCoroutine(StationPrefabHandler.Instance.SpawnOnlyStationsOfType<MovementInTimeStation>());
                        break;
                    case StationPrefabHandler.StationType.TetrisPuzzle:
                        StartCoroutine(StationPrefabHandler.Instance.SpawnOnlyStationsOfType<TetrisPuzzleStation>());
                        break;
                }
                break;
        }
    }

    //public override void OnServerConnect(NetworkConnection conn)
    //{
    //    foreach (var player in Player.Instances)
    //    {
    //        player.OnUpdateAllClients.Invoke();
    //    }
    //}

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        GameSettings.Instance.ServerDisconnected = true;
        E4DataRecorder.Instance.StopRecording();
        StopClient();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {

        //Player.Instances.ForEach(player => Destroy(player.gameObject));
        //HunterPowerUpSpawnPoint.ResetAll();

        base.OnClientDisconnect(conn);
        GameSettings.Instance.ClientDisconnected = true;
        HostIPBroadcaster.Instance.StopSending();
        E4DataRecorder.Instance.StopRecording();
        StopHost();
        //StopHost();
        //SceneHandler.Instance.RestartScene();
    }

    private int PlayerCount = 0;

    public override void OnServerConnect(NetworkConnection conn)
    {
        base.OnServerConnect(conn);

        PlayerCount++;

        if (PlayerCount >= 2)
        {
            HostIPBroadcaster.Instance.StopSending();
        }
    }
}
