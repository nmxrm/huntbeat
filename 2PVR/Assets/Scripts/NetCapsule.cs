﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetCapsule : MonoBehaviour
{
    public static NetCapsule Instance;

    private Renderer renderer;

    public Transform FollowTransform;

    void Awake()
    {
        renderer = GetComponent<Renderer>();
    }

    void OnEnable()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (FollowTransform)
        {
            transform.position = FollowTransform.position + Vector3.up;
        }
    }

    public void StartAlphaAnimation(bool fadeIn, float time, bool removeAfter)
    {
        StartCoroutine(AnimateAlpha(fadeIn, time, removeAfter));
    }

    IEnumerator AnimateAlpha(bool fadeIn, float time, bool removeAfter)
    {
        float i = 0;
        float rate = 1 / time;
        float start;
        float end;

        if (fadeIn)
        {
            start = 0;
            end = renderer.material.GetFloat("_Alpha");
        }
        else
        {
            start = renderer.material.GetFloat("_Alpha");
            end = 0;
        }


        while (i < 1)
        {
            renderer.material.SetFloat("_Alpha", Mathf.Lerp(start, end, i));
            i += rate * Time.deltaTime;
            yield return null;
        }

        renderer.material.SetFloat("_Alpha", end);

        if (removeAfter)
        {
            Destroy(gameObject);
        }
    }
}
