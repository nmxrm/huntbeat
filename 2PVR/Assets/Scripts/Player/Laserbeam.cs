﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laserbeam : MonoBehaviour
{
    public float Speed;
    public float MaxDistance;
    public bool Move;
    private Vector3 SpawnPosition;


    void Start()
    {
        SpawnPosition = transform.position;
    }

    void Update()
    {
        if (Move)
        {
            transform.Translate(Vector3.up * Speed * Time.deltaTime);

            if (Vector3.Distance(SpawnPosition, transform.position) >= MaxDistance)
            {
                Destroy(gameObject);
            } 
        }
    }
}
