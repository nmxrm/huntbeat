﻿using System.Collections;
using System.Collections.Generic;
using System.Resources;
using UnityEngine;

public class PlayerSpawnPoint : MonoBehaviour
{
    private static Dictionary<Player.PlayerType, List<PlayerSpawnPoint>> SpawnPoints = new Dictionary<Player.PlayerType, List<PlayerSpawnPoint>>();

    public Player.PlayerType CanSpawnType;

    void Awake()
    {
        List<PlayerSpawnPoint> points;

        SpawnPoints.TryGetValue(CanSpawnType, out points);

        if (points != null)
        {
            points.Add(this);
        }
        else
        {
            points = new List<PlayerSpawnPoint>();
            points.Add(this);
            SpawnPoints.Add(CanSpawnType, points);
        }
    }

    void OnDestroy()
    {
        SpawnPoints.Remove(CanSpawnType);
    }

    public static PlayerSpawnPoint GetRandomSpawnPoint(Player.PlayerType playerType)
    {
        List<PlayerSpawnPoint> points;
        SpawnPoints.TryGetValue(playerType, out points);

        if (points != null)
        {
            return points[Random.Range(0, points.Count)];
        }

        return null;
    }

    public Vector3 GetGroundPosition()
    {
        Ray ray = new Ray(transform.position, Vector3.down);

        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            return hitInfo.point;
        }

        Debug.LogWarning("Spawn point could not find a ground position!");
        return Vector3.zero;
    }
}
