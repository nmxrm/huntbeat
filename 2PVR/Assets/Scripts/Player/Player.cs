﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using HTC.UnityPlugin.Vive;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class Player : NetworkBehaviour
{
    //[SyncVar]
    public static List<Player> Instances = new List<Player>();

    public enum PlayerType
    {
        Prey,
        Hunter
    }

    //[HideInInspector]
    public PlayerType playerType { get; private set; }

    //[SyncVar(hook = "CmdSetPlayerType")]
    //public int PlayerTypeInt;

    //public bool isVr;

    public GameObject VrCameraObject;

    public GameObject CameraBlasterObject;
    public GameObject HunterHandBlasterObject;

    public GameObject DefaultCameraObject;

    public GameObject ChainPrefab;

    public Laserbeam LaserbeamPrefab;

    public GameObject GettingCaughtWarning;

    [SerializeField]
    private GameObject Model;

    [SyncVar/*(hook = "OnVisibilityChanged")*/]
    // percent 0 to 1
    private float Visibility = 0;

    [SerializeField]
    private float VisibilityReductionRate = 0.02f;

    //[SerializeField]
    //public Trace TracePrefab;

    private Coroutine VisibilityRoutine;

    private RaycastHit ClosestRayHit;

    public List<Renderer> VisibilityRenderers = new List<Renderer>();

    public Color HunterColor;

    private float Timer = 0;

    private Animator Anim;

    public Camera ActiveCam { get; private set; }

    public Decoy DecoyPrefab;
    public NetCapsule CatchingCapsulePrefab;

    public GameObject HeartbeatSoundPrefab;

    public Action OnUpdateAllClients;

    [SerializeField]
    private HunterUi HunterUiCanvas;

    private int HunterPowerUpsCollected = 0;

    private bool AllPlayersJoinedCanSetReady;

    private bool GameStarted;

    void Awake()
    {
        Instances.Add(this);

        Anim = GetComponent<Animator>();
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            // updates all newly connected clients
            CmdUpdateAllClients();

            var pType = GameSettings.Instance.SelectedPlayerType;
            //PlayerTypeInt = (int)pType;

            CmdSetPlayerType((int)pType);

            if (pType == PlayerType.Hunter)
            {
                CmdSetVisibilityPercent(1);
                GetComponentInChildren<Interactable>().Enabled = false;
            }

            GetComponentInChildren<ControllerMessageBox>().SetAsSingleton();

            switch (GameSettings.Instance.VR)
            {
                case true:
                    VrCameraObject.SetActive(true);
                    DefaultCameraObject.SetActive(false);
                    GetComponent<FirstPersonControls>().enabled = false;
                    //Model.transform.SetParent(VrCameraObject.transform);
                    var copyT = Model.AddComponent<CopyTransform>();
                    ActiveCam = VrCameraObject.GetComponentInChildren<Camera>();
                    Model.AddComponent<KeepGrounded>().RaycastObject = ActiveCam.gameObject;
                    //copyT.CopyPosition = ActiveCam.transform;
                    copyT.CopyOnlyYRotation = true;
                    //copyT.CopyOnlyXZPosition = true;
                    copyT.CopyRotation = ActiveCam.transform;
                    //copyT.YOffset = -2f;
                    GetComponent<NetworkTransformChild>().enabled = true;
                    var armSwinger = GetComponent<ArmSwinger>();
                    armSwinger.enabled = true;
                    armSwinger.OnMovedThisFrame += SetWalkingAnimationVR;
                    break;

                case false:
                    VrCameraObject.SetActive(false);
                    DefaultCameraObject.SetActive(true);
                    var rb = gameObject.AddComponent<Rigidbody>();
                    rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
                    var fpC = GetComponent<FirstPersonControls>();
                    fpC.enabled = true;
                    fpC.OnEnable();
                    fpC.movementSpeed = 150;
                    ActiveCam = DefaultCameraObject.GetComponent<Camera>();
                    gameObject.GetComponent<Collider>().enabled = true;
                    break;
            }

            var spawnPoint = PlayerSpawnPoint.GetRandomSpawnPoint(pType);

            if (spawnPoint)
            {
                if (GameSettings.Instance.VR)
                {
                    GetComponent<ArmSwinger>().moveCameraRig(spawnPoint.GetGroundPosition() /*+ new Vector3(0, 0.85f, 0)*/);
                }
                else
                {
                    transform.position = spawnPoint.GetGroundPosition() + new Vector3(0, 0.85f, 0);
                }
            }
            //if (playerType == PlayerType.Prey)
            //{
            //    VisibilityRoutine = StartCoroutine(ReduceVisibility());
            //} 

            foreach (var visibilityRenderer in VisibilityRenderers)
            {
                visibilityRenderer.enabled = false;
            }

            //GameManager.Instance.CmdAcquireLocalAuthority(GetComponent<NetworkIdentity>());
            //CmdAcquireOwnership(GameManager.Instance.GetComponent<NetworkIdentity>());

            switch (pType)
            {
                case PlayerType.Hunter:
                    //CustomNetworkManager.GetSingleton().CmdSetHunterId(GetComponent<NetworkIdentity>().netId);
                    //GameManager.Instance.HunterId = GetComponent<NetworkIdentity>().netId;
                    GameManagerInteractor.Instance.CmdSetHunterId(GetComponent<NetworkIdentity>());
                    HunterUiCanvas.gameObject.SetActive(true);
                    HunterHandBlasterObject.SetActive(false);
                    HunterPowerUpHandler.Instance.InstantiatePowerUps();
                    HunterUiCanvas.UpdatePowerUpUi(HunterPowerUpsCollected);
                    HunterUiCanvas.QueueToastMessage("Waiting for other player!", -1.5f);
                    break;

                case PlayerType.Prey:
                    //GameManager.Instance.PreyId = GetComponent<NetworkIdentity>().netId;
                    //GameManager.Instance.CmdSetPreyId(GetComponent<NetworkIdentity>().netId);
                    GameManagerInteractor.Instance.CmdSetPreyId(GetComponent<NetworkIdentity>());
                    //E4DataReceiver.Instance.StartReceivingWithThread();
                    E4DataReceiver.Instance.StartReceivingRecords();
                    ControllerMessageBox.Instance.ShowText("Waiting for heartbeat", -1.5f);
                    E4DataReceiver.Instance.OnInterbeatIntervalHeartbeatReceived.AddListener(HeartbeatReceived);
                    HunterUiCanvas.gameObject.SetActive(false);

                    if (GameSettings.Instance.DebugMode)
                    {
                        firstHeartbeat = false;
                        BeginAveraging = true;
                    }
                    break;
            }


            GameObject dummy;
            string dummyName = "[DummyRig]";

            if (SingletonObject.Singletons.TryGetValue(dummyName, out dummy))
            {
                dummy.SetActive(false);
            }
            else
            {
                Debug.LogFormat("Couldn't find singleton \"{0}\"", dummyName);
            }

            CmdSetupUpdateMethods();
            //StartCoroutine(StartArmSwingerRoutine());

        }
        else
        {
            DefaultCameraObject.SetActive(false);
            VrCameraObject.SetActive(false);
            HunterUiCanvas.gameObject.SetActive(false);
            CameraBlasterObject.SetActive(false);
        }
    }

    void OnDestroy()
    {
        if (!isLocalPlayer)
        {
            UnsubscribeUpdateMethods();
        }

        Instances.Remove(this);
    }

    private void UnsubscribeUpdateMethods()
    {
        OnUpdateAllClients -= UpdatePlayerTypeDelegate;
    }

    private Action UpdatePlayerTypeDelegate;

    [Command]
    private void CmdSetupUpdateMethods()
    {
        UpdatePlayerTypeDelegate = delegate { RpcUpdatePlayerType((int)playerType); };
        OnUpdateAllClients += UpdatePlayerTypeDelegate;
    }

    [Command]
    private void CmdUpdateAllClients()
    {
        foreach (var player in Instances)
        {
            if (player.OnUpdateAllClients != null)
            {
                player.OnUpdateAllClients.Invoke();
            }
        }
    }

    //IEnumerator StartArmSwingerRoutine()
    //{
    //    yield return null;

    //    GetComponent<ArmSwinger>().enabled = true;
    //}

    //[Command]
    //private void CmdTestSetHunterId(NetworkIdentity id)
    //{
    //    GameManager.Instance.CmdSetHunterId(id.netId);
    //}

    //[Command]
    //private void CmdTestSetPreyId(NetworkIdentity id)
    //{
    //    GameManager.Instance.CmdSetPreyId(id.netId);
    //}

    [Command]
    private void CmdSetPlayerType(int pType)
    {
        RpcUpdatePlayerType(pType);
    }

    [ClientRpc]
    private void RpcUpdatePlayerType(int pType)
    {
        playerType = (PlayerType)pType;

        if (playerType == PlayerType.Hunter)
        {
            //GameManagerInteractor.Instance.CmdSetHunterId(GetComponent<NetworkIdentity>());

            GetComponentInChildren<Interactable>().Enabled = false;

            for (int i = 0; i < VisibilityRenderers.Count; i++)
            {
                var visibilityRenderer = VisibilityRenderers[i];
                SetMaterialOpaque(visibilityRenderer.material);

                if (i == 1)
                {
                    visibilityRenderer.material.SetColor("_Color", HunterColor);
                }
            }

            GetComponent<FirstPersonControls>().PType = PlayerType.Hunter;
            SetPistolAnimation();
            //VisibilityRenderers[1].material.SetColor("_Color", HunterColor);
        }
        else
        {
            //GameManagerInteractor.Instance.CmdSetPreyId(GetComponent<NetworkIdentity>());
            //    GetComponent<RandomFootstepPlayer>().enabled = false;
            HunterHandBlasterObject.SetActive(false);
            CameraBlasterObject.SetActive(false);
        }
    }

    [ClientRpc]
    private void RpcOnVisibilityChanged(float value)
    {
        //if (!isServer)
        //{
        Visibility = value;
        //}
        //VisibilityRenderers.EnableKeyword("_ALBEDO");
        //Color c = /*VisibilityRenderers.material.GetColor("_Color");*/ Color.white;
        //c.a = Visibility;

        foreach (var visibilityRenderer in VisibilityRenderers)
        {
            var mat = visibilityRenderer.material;
            Color c = mat.GetColor("_Color");
            c.a = Visibility;
            visibilityRenderer.material.SetColor("_Color", c);

            if (Visibility >= 0.95f)
            {
                SetMaterialOpaque(mat);
            }
            else
            {
                SetMaterialFade(mat);
            }
        }
    }

    private void SetMaterialOpaque(Material mat)
    {
        // set opaque
        mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
        mat.SetInt("_ZWrite", 1);
        mat.DisableKeyword("_ALPHATEST_ON");
        mat.DisableKeyword("_ALPHABLEND_ON");
        mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        mat.renderQueue = -1;
    }

    private void SetMaterialFade(Material mat)
    {
        // set fade
        mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        mat.SetInt("_ZWrite", 0);
        mat.DisableKeyword("_ALPHATEST_ON");
        mat.EnableKeyword("_ALPHABLEND_ON");
        mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        mat.renderQueue = 3000;
    }

    public bool SimulateHeartbeatOff = false;

    [Command]
    private void CmdSetVisibilityPercent(float value)
    {
        if (OngoingCatching)
        {
            Visibility = 1f;
        }
        else if (IsInvisible)
        {
            Visibility = 0f;
        }
        else
        {
            Visibility = Mathf.Clamp01(value);
        }

        if (SimulateHeartbeatOff)
        {
            Visibility = 0;
        }

        RpcOnVisibilityChanged(Visibility);
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            CastInteractionRay();

            CheckForInputPrey();

            CheckForInputHunter();

            // SetAnimationIfPreyMovedViaTracking();

            if (playerType == PlayerType.Prey)
            {

                // Reduce Visibility after time. Workaround, because you can't call commands from coroutines.
                Timer += Time.deltaTime;

                if (Timer >= 0.2f)
                {
                    CmdSetVisibilityPercent(Visibility - VisibilityReductionRate);
                    Timer = 0;
                }

                if (Trace.CanBeSpawned(transform.position))
                {
                    CmdSpawnTrace(ActiveCam.transform.position);
                }
            }

            //if (playerType == PlayerType.Hunter)
            //{

            //Player otherPlayer = null;

            //if (ClosestRayHit.transform != null)
            //{
            //    otherPlayer = ClosestRayHit.transform.gameObject.GetComponent<Player>();
            //}

            //if (otherPlayer != null && otherPlayer.playerType == PlayerType.Prey)
            //{
            //    inRange = true;
            //    Debug.Log("prey found");
            //}
            //else
            //{
            //    inRange = false;
            //}
            //}
        }
    }

    private Vector3 LastPosition = Vector3.zero;

    private void SetAnimationIfPreyMovedViaTracking()
    {
        if (playerType == PlayerType.Prey)
        {
            if (!(ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Grip) ||
                  ViveInput.GetPress(HandRole.RightHand, ControllerButton.Grip)))
            {

                var distance = Vector3.Distance(LastPosition, transform.position);

                if (distance > 0.01f)
                {
                    SetWalkingAnimationVR(true);
                }
                else
                {
                    SetWalkingAnimationVR(false);
                }

                LastPosition = transform.position;
            }
        }
    }

    private void CheckForInputHunter()
    {
        if (playerType == PlayerType.Hunter)
        {
            if (AllPlayersJoinedCanSetReady)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    GameManagerInteractor.Instance.CmdSetPlayerReady((int)playerType);
                    HunterUiCanvas.QueueToastMessage("Ready!", -1.5f);
                    Debug.Log("Set Ready!");
                    AllPlayersJoinedCanSetReady = false;
                }
            }
            else
            {
                // possible R-key action
            }


            if (Input.GetKeyDown(KeyCode.U))
            {
                HunterUiCanvas.gameObject.SetActive(!HunterUiCanvas.gameObject.activeSelf);
            }

            if (Input.GetKeyDown(KeyCode.F12))
            {
                Directory.CreateDirectory(@"c:\huntbeatscreens\");
                ScreenCapture.CaptureScreenshot(@"c:\huntbeatscreens\" + DateTime.Now.ToString("dd-M-yyyy--HH-mm-ss") + ".png");
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                TryShowTraces();
            }

            if ((Input.GetMouseButtonDown(0) && GameStarted) || (Input.GetMouseButtonDown(0) && GameSettings.Instance.DebugMode))
            {
                Interactable interactable = null;

                if (ClosestRayHit.collider != null)
                {
                    interactable = ClosestRayHit.collider.GetComponent<Interactable>();

                    var prey = interactable.GetComponentInParent<Player>();

                    if (interactable != null && prey != null && prey.playerType == PlayerType.Prey)
                    {
                        //interactable.CheckInteractionPossibleAndInteract(this);
                        InstantiateLocalCapsule(prey.transform);
                        CmdInitiateCatchingPrey(/*prey.GetComponent<NetworkIdentity>()*/);
                    }
                    else
                    {
                        CmdCatchingMissed();
                    }
                }
                else
                {
                    CmdCatchingMissed();
                }
            }

            //if (Input.GetKeyDown(KeyCode.C))
            //{
            //    CmdDEBUGSetPistolAnimation();
            //}

            //if (Input.GetMouseButtonDown(0))
            //{

            //}
        }
    }

    [TargetRpc]
    private void TargetInstantiateLaserbeam(NetworkConnection conn)
    {
        var spawnT = CameraBlasterObject.transform.GetChild(0);
        Instantiate(LaserbeamPrefab, spawnT.position, spawnT.rotation);
        //beam.transform.LookAt(ActiveCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 100)), Vector3.forward);
        //beam.transform.right = (ActiveCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 100)) - beam.transform.position).normalized;
    }


    [Command]
    private void CmdCatchingMissed()
    {
        if (GameSettings.Instance != null && !GameSettings.Instance.DebugMode)
        {
            if (OngoingCatching || CatchingCoolingdownTime > 0)
            {
                return;
            }
        }

        var hunterConn = NetworkServer.FindLocalObject(GameManager.Instance.HunterId).GetComponent<NetworkIdentity>()
                .connectionToClient;

        TargetInstantiateLaserbeam(hunterConn);
        TargetRemoveCapsuleStartCooldownUiAnimation(hunterConn, CatchingMissedCooldownTime, false);
        StartCoroutine(CatchingCooldownRoutine(CatchingMissedCooldownTime));

        SoundObjectPlayer.Instance.PlaySound("lasershoot", CameraBlasterObject.transform.position, true);

    }

    private void InstantiateLocalCapsule(Transform preyTransform)
    {
        if (OngoingCatching || CatchingCoolingdownTime > 0)
        {
            return;
        }

        var preyBot = preyTransform.Find("Ybot");
        var capsule = Instantiate(CatchingCapsulePrefab.gameObject, preyBot.position + Vector3.up, Quaternion.identity).GetComponent<NetCapsule>();
        capsule.FollowTransform = preyBot;
        capsule.StartAlphaAnimation(true, 1.5f, false);
    }

    private void CheckForInputPrey()
    {
        if (playerType == PlayerType.Prey)
        {
            if (AllPlayersJoinedCanSetReady)
            {
                if (((Input.GetKeyDown(KeyCode.R) || ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.FullTrigger) || ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.FullTrigger)) && HeartbeatAveraged) || GameSettings.Instance.DebugMode)
                {
                    GameManagerInteractor.Instance.CmdSetPlayerReady((int)playerType);
                    ControllerMessageBox.Instance.ShowText("Ready!", -1.5f);
                    Debug.Log("Set Ready!");
                    AllPlayersJoinedCanSetReady = false;
                }
            }
            else
            {
                // possible R-key action
            }

            if ((ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.FullTrigger) || ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.FullTrigger)) && !HeartbeatAveraged)
            {
                if (!firstHeartbeat)
                {
                    BeginAveraging = true;

                }
            }

            if (ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Menu) || ViveInput.GetPressDown(HandRole.RightHand, ControllerButton.Menu))
            {
                Directory.CreateDirectory(@"c:\huntbeatscreens\");
                ScreenCapture.CaptureScreenshot(@"c:\huntbeatscreens\" + DateTime.Now.ToString("dd-M-yyyy--HH-mm-ss") + ".png");
            }

            // debug
            if (Input.GetKeyDown(KeyCode.Space))
            {
                CmdSetVisibilityPercent(Visibility + 0.1f);
                BeginAveraging = true;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                CmdSetVisibilityPercent(Visibility - 0.1f);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ShowOnGui = !ShowOnGui;
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                Interactable interactable = null;

                ExitGate exit = null;

                if (ClosestRayHit.collider != null)
                {
                    interactable = ClosestRayHit.collider.GetComponent<Interactable>();

                    exit = interactable.GetComponent<ExitGate>();
                }

                if (Inventory.Instance.CarriedKey != null && interactable != null && exit != null)
                {
                    interactable.CheckInteractionPossibleAndInteract(this);
                }
                else if (Inventory.Instance.CarriedKey != null)
                {
                    Inventory.Instance.Drop();
                }
                else
                {
                    if (interactable != null && interactable.Enabled)
                    {
                        interactable.CheckInteractionPossibleAndInteract(this);
                    }
                }
            }

            //if (Input.GetKeyDown(KeyCode.P))
            //{
            //    CmdInstantiateAndLaunchDecoy();
            //}
        }
    }

    private bool inRange;

    private Ray ray;

    private void CastInteractionRay()
    {
        ray = DefaultCameraObject.GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        //Physics.Raycast(ray, out RayHits,);

        var rayHits = Physics.SphereCastAll(ray, 0.2f, 2f/*, 9*/).ToList();

        rayHits = rayHits.OrderBy(h => h.distance).ToList();

        //foreach (var raycastHit in rayHits)
        //{
        //    var ownPlayer = raycastHit.transform.gameObject.GetComponent<Player>();

        //    if (ownPlayer != null && ownPlayer.playerType == PlayerType.Hunter)
        //    {
        //        rayHits.Remove(raycastHit);
        //        break;
        //    }
        //}

        for (int i = 0; i < rayHits.Count; i++)
        {
            var hit = rayHits[i];

            var interactable = hit.collider.GetComponent<Interactable>();

            var player = hit.collider.GetComponentInParent<Player>();

            if (interactable == null || (player != null && player == this) || interactable.Enabled == false)
            {
                rayHits.RemoveAt(i);
                i--;
            }
        }

        if (rayHits.Count >= 1)
        {
            ClosestRayHit = rayHits[0];
        }
        else
        {
            ClosestRayHit = new RaycastHit();
        }
    }

    [Command]
    private void CmdSpawnTrace(Vector3 origin)
    {
        var trace = Instantiate(GameSettings.Instance.TracePrefab.gameObject, origin + Vector3.down * 0.5f, Quaternion.identity).GetComponent<Trace>();
        trace.Creator = this;
        NetworkServer.Spawn(trace.gameObject);
    }

    //IEnumerator ReduceVisibility()
    //{
    //    CmdSetVisibilityPercent(0);

    //    while (true)
    //    {
    //        yield return new WaitForSeconds(0.2f);

    //        CmdSetVisibilityPercent(Visibility -= VisibilityReductionRate);
    //    }
    //}

    private void OnGUI()
    {
        if (isLocalPlayer)
        {
            if (playerType == PlayerType.Prey && ShowOnGui)
            {
                GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height / 2), new Vector2(100, 100)), "Visibility: " + /*Mathf.FloorToInt(*/Visibility /** 100)*/);

                //if (ClosestRayHit.collider != null)
                //{
                //    var interactable = ClosestRayHit.collider.GetComponent<Interactable>();

                //    if (interactable != null && interactable.Enabled)
                //    {
                //        if (Inventory.Instance.CarriedKey != null && interactable.GetComponent<ExitGate>() != null)
                //        {
                //            GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height - 100), new Vector2(100, 100)), "Press F to: " + interactable.InfoText);
                //        }
                //    }
                //}

                if (Inventory.Instance.CarriedKey != null)
                {
                    GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height - 100), new Vector2(100, 100)), "Press F to: Drop key");
                }
                else if (ClosestRayHit.collider != null)
                {
                    var interactable = ClosestRayHit.collider.GetComponent<Interactable>();

                    if (interactable != null && interactable.Enabled)
                    {
                        GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height - 100), new Vector2(100, 100)), "Press F to: " + interactable.InfoText);
                    }
                }
            }

            //if (playerType == PlayerType.Hunter)
            //{
            //    if (inRange)
            //    {
            //        GUI.Label(new Rect(new Vector2(Screen.width / 2, Screen.height / 2), new Vector2(100, 100)), "Catch prey");
            //    }
            //}

        }
    }

    public bool ShowOnGui = false;

    [SyncVar]
    private bool OngoingCatching = false;

    public readonly float CatchingCooldownTime = 30f;
    public readonly float CatchingMissedCooldownTime = 10f;

    [SyncVar]
    private float CatchingCoolingdownTime = 0f;

    [Command]
    public void CmdInitiateCatchingPrey(/*NetworkIdentity preyId*/)
    {
        if (OngoingCatching || CatchingCoolingdownTime > 0)
        {
            GameManager.Instance.PrintOnClient(PlayerType.Hunter, CatchingCoolingdownTime > 0 ? string.Format("Catching cooling down ({0} secs remaining)", CatchingCoolingdownTime) : "Catching ongoing");
            return;
        }

        OngoingCatching = true;

        Debug.Log("Prey found!");

        var preyId = GameManager.Instance.PreyId;

        // block movement on server. syncvar syncs it to the client
        var preyObj = NetworkServer.FindLocalObject(preyId);
        //preyObj.GetComponent<FirstPersonControls>().BlockMovement = true;

        //GetComponentInChildren<Interactable>().Enabled = false;

        var prey = preyObj.GetComponent<Player>();
        prey.TargetBlockMovement(prey.connectionToClient, true);
        prey.OngoingCatching = true;
        prey.TargetStartInstantiatingChains(prey.connectionToClient);
        prey.TargetDropCarriablesAndFailStations(prey.connectionToClient);
        prey.TargetSetActiveCatchingWarning(prey.connectionToClient, true);

        // set animation
        //  RpcSetPistolAnimation();
        //GetComponent<FirstPersonControls>().BlockMovement = true;
        BlockMovement(true);

        TargetInstantiateLaserbeam(NetworkServer.FindLocalObject(GameManager.Instance.HunterId).GetComponent<NetworkIdentity>().connectionToClient);

        SoundObjectPlayer.Instance.PlaySound("lasershoot", CameraBlasterObject.transform.position, true);

        GameManager.Instance.StartCatchingCountdown();
    }

    [TargetRpc]
    public void TargetSetActiveCatchingWarning(NetworkConnection conn, bool state)
    {
        GettingCaughtWarning.SetActive(state);
    }

    [TargetRpc]
    private void TargetDropCarriablesAndFailStations(NetworkConnection conn)
    {
        Station.FailAllStations(true);

        var carriables = Carriable.CurrentCarriedObjects.Values.ToList();

        foreach (var carriable in carriables)
        {
            if (carriable != null)
            {
                carriable.Drop();

                var tetrisPiece = carriable.GetComponent<TetrisPiece>();
                if (tetrisPiece != null)
                {
                    tetrisPiece.ResetPosition();
                }
            }
        }
    }

    [ClientRpc]
    private void RpcSetPistolAnimation()
    {
        var fps = GetComponent<FirstPersonControls>();

        if (fps.IsWalking)
        {
            Anim.SetTrigger("WalkingToPistol");
        }
        else
        {
            Anim.SetTrigger("IdleToPistol");
        }
    }

    private void SetPistolAnimation()
    {
        var fps = GetComponent<FirstPersonControls>();

        if (fps.IsWalking)
        {
            Anim.SetTrigger("WalkingToPistol");
        }
        else
        {
            Anim.SetTrigger("IdleToPistol");
        }
    }

    [ClientRpc]
    public void RpcResetPistolAnimation()
    {
        Anim.SetTrigger("PistolToIdle");
    }

    [Command]
    private void CmdDEBUGSetPistolAnimation()
    {
        RpcSetPistolAnimation();
    }

    //[TargetRpc]
    public void CatchingEnded(/*NetworkConnection connection*/)
    {
        // gets changed on server, syncvar syncs it over to client
        OngoingCatching = false;

        StartCoroutine(CatchingCooldownRoutine(CatchingCooldownTime));

    }

    [TargetRpc]
    public void TargetRemoveCapsuleStartCooldownUiAnimation(NetworkConnection conn, float cooldownTime, bool fadeCapsule)
    {
        HunterUiCanvas.StartCooldownAnimation(cooldownTime);
        if (fadeCapsule)
        {
            NetCapsule.Instance.StartAlphaAnimation(false, 1.5f, true);
        }
    }

    private IEnumerator CatchingCooldownRoutine(float cooldown)
    {
        CatchingCoolingdownTime = cooldown;

        while (CatchingCoolingdownTime > 0)
        {
            yield return new WaitForSeconds(1);
            CatchingCoolingdownTime--;
        }
    }

    [TargetRpc]
    public void TargetDestroyAllChains(NetworkConnection target)
    {
        ChainElement.DestroyAll();
    }

    [TargetRpc]
    public void TargetStartInstantiatingChains(NetworkConnection target)
    {
        StartCoroutine(InstantiateLocalChains());
    }

    [ContextMenu("Instantiate Chains")]
    private void DEBUGStartInstantiatingChains()
    {
        StartCoroutine(InstantiateLocalChains());
    }

    private IEnumerator InstantiateLocalChains()
    {
        for (int i = 0; i < 3; i++)
        {
            var chain = Instantiate(ChainPrefab, ActiveCam.transform.position + Random.onUnitSphere * RandomSphereModifier, Quaternion.identity).GetComponent<ChainElement>();
            chain.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);

            chain.transform.LookAt(ActiveCam.transform);
            chain.transform.rotation = Quaternion.Euler(chain.transform.rotation.eulerAngles.x, chain.transform.rotation.eulerAngles.y, 45);

            //chain.gameObject.SetActive(false);

            chain.SetChildrenActive(false);

            // work around to check if the object collides with something
            yield return null;
            chain.transform.position += Vector3.zero;

            while (/*chain.IsColliding || */!chain.CanBeSeenByCamera(ActiveCam))
            {
                chain.transform.position = ActiveCam.transform.position + Random.onUnitSphere * RandomSphereModifier;
                chain.transform.LookAt(ActiveCam.transform);
                chain.transform.rotation = Quaternion.Euler(chain.transform.rotation.eulerAngles.x, chain.transform.rotation.eulerAngles.y, 45);

                yield return null;
                chain.transform.position += Vector3.zero;
            }

            //chain.gameObject.SetActive(true);
            chain.SetChildrenActive(true);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawRay(ray);
    }

    private ControllerInteractor DecoySpawningInteractor;

    private readonly float RandomSphereModifier = 0.5f;

    public void InitiateSpawningDecoy(ControllerInteractor interactor)
    {
        if (DecoySpawningInteractor == null)
        {
            DecoySpawningInteractor = interactor;
            CmdSpawnDecoy();
        }
    }

    [Command]
    private void CmdSpawnDecoy()
    {
        var decoy = Instantiate(DecoyPrefab, transform.position, Quaternion.identity);
        NetworkServer.Spawn(decoy.gameObject);

        var preyId = GameManager.Instance.PreyId;
        var prey = NetworkServer.FindLocalObject(preyId).GetComponent<Player>();
        prey.TargetAttachDecoyToPlayer(prey.GetComponent<NetworkIdentity>().connectionToClient, decoy.gameObject);
    }

    [TargetRpc]
    private void TargetAttachDecoyToPlayer(NetworkConnection connection, GameObject decoyGO)
    {
        var dCarriable = decoyGO.GetComponent<Carriable>();
        dCarriable.Interactor = DecoySpawningInteractor;
        dCarriable.OnDropEvent.AddListener(() => CmdLaunchDecoy(decoyGO));
        dCarriable.Pickup(true);

        DecoySpawningInteractor = null;
    }

    [Command]
    public void CmdLaunchDecoy(GameObject decoyGO)
    {
        var decoy = decoyGO.GetComponent<Decoy>();

        if (decoy)
        {
            decoy.RpcLaunchDecoy();
        }
    }

    public void SetWalkingAnimationVR(bool state)
    {
        Anim.SetBool("Walking", state);
    }

    [ClientRpc]
    public void RpcResetCatching()
    {
        OngoingCatching = false;
    }

    public static Player GetPlayerByType(PlayerType pType)
    {
        foreach (var instance in Instances)
        {
            if (instance.playerType == pType)
            {
                return instance;
            }
        }

        return null;
    }

    private List<float> CollectedHeartbeats = new List<float>();

    private bool HeartbeatAveraged = false;
    private bool BeginAveraging = false;

    // averaged heartbeat when calm
    float invisibilityThreshold = 60f;

    float fullyVisibleThreshold = 100f;

    private Coroutine AveragingHeartbeatRoutine;
    private Coroutine ContinuousHeartbeatRoutine;

    public float CurrentHeartbeat { get; private set; }

    private bool firstHeartbeat = true;

    public bool RepeatHBOnPulse = true;

    private void HeartbeatReceived(float ibi, double timestamp)
    {
        CurrentHeartbeat = ibi;

        if (ContinuousHeartbeatRoutine == null)
        {
            ContinuousHeartbeatRoutine = StartCoroutine(ContinuousHeartbeat());
        }

        if (firstHeartbeat)
        {
            if (GameSettings.Instance.VR)
            {
                ControllerMessageBox.Instance.ShowText("Heartbeat detected!" + Environment.NewLine + "Calm down, then press a trigger to start averaging.", -1.5f);
                if (ControllerInteractor.Interactors.ContainsKey(HandRole.LeftHand))
                {
                    ControllerInteractor.Interactors[HandRole.LeftHand].AbilityMenu.Open(true);
                }
            }

            firstHeartbeat = false;
        }

        if (HeartbeatAveraged)
        {
            // if ibi is in milliseconds multiply by 6000 for heartbeats per minute

            float scaleMin = 0;
            float scaleMax = 1;

            var visibility = ((ibi - invisibilityThreshold) / (fullyVisibleThreshold - invisibilityThreshold)) * (scaleMax - scaleMin) + scaleMin;

            CmdSetVisibilityPercent(visibility);
        }
        else if (BeginAveraging)
        {
            if (AveragingHeartbeatRoutine == null)
            {
                AveragingHeartbeatRoutine = StartCoroutine(AverageHeartbeatRoutine());
            }

            CollectedHeartbeats.Add(ibi);
        }
    }

    private IEnumerator ContinuousHeartbeat()
    {
        if (HeartbeatAnimation.Instance != null)
        {
            while (true)
            {
                if (CurrentHeartbeat != 0)
                {
                    Instantiate(HeartbeatSoundPrefab, transform);

                    HeartbeatAnimation.Instance.StartHeartbeatAnimation(CurrentHeartbeat);

                    if (RepeatHBOnPulse && GameStarted)
                    {
                        HeartbeatReceived(CurrentHeartbeat, 0);
                    }

                    yield return new WaitForSeconds(60 / CurrentHeartbeat);
                }
            }
        }

        ContinuousHeartbeatRoutine = null;
    }

    private IEnumerator AverageHeartbeatRoutine()
    {
        Debug.Log("Averaging heartbeat started!");
        //ControllerMessageBox.Instance.ShowText("Averaging heartbeat", -1.5f);
        StartCoroutine(ShowAveragingHeartbeatCountdown(GameSettings.Instance.DebugMode ? 1f : 30f));
        yield return new WaitForSeconds(GameSettings.Instance.DebugMode ? 1f : 30f);

        float average = GameSettings.Instance.DebugMode ? 60f : 0;
        foreach (var value in CollectedHeartbeats)
        {
            average += value;
        }
        average /= CollectedHeartbeats.Count;
        invisibilityThreshold = average;
        fullyVisibleThreshold = average + 40f;
        Debug.Log("Averaging heartbeat done! Average: " + invisibilityThreshold + "; fully visible at: " + fullyVisibleThreshold);
        ControllerMessageBox.Instance.ShowText("Averaging heartbeat done!");
        HeartbeatAveraged = true;

        if (!AllPlayersJoinedCanSetReady)
        {
            ControllerMessageBox.Instance.ShowText("Waiting for other player!", -1.5f);
        }
        else
        {
            ControllerMessageBox.Instance.ShowText("All players joined. Press the trigger to ready up!", -1.5f);
        }

        AveragingHeartbeatRoutine = null;
    }

    private IEnumerator ShowAveragingHeartbeatCountdown(float time)
    {
        while (time >= 0)
        {
            ControllerMessageBox.Instance.ShowText("Averaging heartbeat" + Environment.NewLine + Mathf.FloorToInt(time), 1f);
            yield return new WaitForSeconds(1f);
            time--;
        }
    }

    [TargetRpc]
    public void TargetPrintHunterToastMessage(NetworkConnection conn, string text, float customTime)
    {
        HunterUiCanvas.QueueToastMessage(text, customTime);
    }

    public void HunterPowerUpCollected()
    {
        HunterPowerUpsCollected++;

        HunterUiCanvas.UpdatePowerUpUi(HunterPowerUpsCollected);
    }

    private void TryShowTraces()
    {
        if (HunterPowerUpsCollected == 3 || (GameSettings.Instance != null && GameSettings.Instance.DebugMode))
        {
            HunterPowerUpsCollected = 0;
            HunterPowerUpHandler.DestroyAllActivePowerUps();
            HunterUiCanvas.UpdatePowerUpUi(HunterPowerUpsCollected);
            HunterPowerUpHandler.Instance.InstantiatePowerUps();

            if (Trace.AllTraces.Count > 0)
            {
                Trace.AllTraces[0].ShowTraces();
            }
        }
    }

    [TargetRpc]
    public void TargetStartGameOnPlayer(NetworkConnection conn)
    {
        //todo: remove movement block (dont know if this still needs to be done) | probably not
        switch (playerType)
        {
            case PlayerType.Hunter:
                HunterUiCanvas.StartCountdown(this);
                break;
            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        ControllerMessageBox.Instance.ShowCountdown(this);
                        break;
                    case false:
                        Debug.Log("Countdown started!");
                        BlockMovement(false);
                        break;
                }
                break;
        }
    }

    public void GameStartedAfterCountdown()
    {
        GameStarted = true;
    }

    //[Command]
    //public void CmdBlockMovement(bool state)
    //{
    //    GetComponent<FirstPersonControls>().BlockMovement = state;
    //}

    //[ClientRpc]
    //public void RpcPlayerJoinedCallback()
    //{
    //    CmdBlockMovement(false);
    //}

    [TargetRpc]
    public void TargetBlockMovement(NetworkConnection conn, bool state)
    {
        BlockMovement(state);
    }

    // on server!
    public void BlockMovement(bool state)
    {
        if (GameSettings.Instance.DebugMode && state == false)
        {
            return;
        }

        switch (playerType)
        {
            case PlayerType.Hunter:
                GetComponent<FirstPersonControls>().BlockMovement = state;
                break;

            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        GetComponent<ArmSwinger>().armSwingingPaused = state;
                        break;
                    case false:
                        GetComponent<FirstPersonControls>().BlockMovement = state;
                        break;
                }
                break;
        }
    }

    //public void BlockMovementFromClient(bool state)
    //{
    //    CmdBlockMovement(state);
    //}

    //[Command]
    //private void CmdBlockMovement(bool state)
    //{
    //    BlockMovementFromServer(state);
    //}

    [TargetRpc]
    public void TargetAllPlayersJoined(NetworkConnection conn)
    {
        AllPlayersJoinedCanSetReady = true;
        switch (playerType)
        {
            case PlayerType.Hunter:
                HunterUiCanvas.QueueToastMessage("All players joined. Press R to ready up!", -1.5f);
                break;
            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        if (HeartbeatAveraged)
                        {
                            ControllerMessageBox.Instance.ShowText("All players joined. Press the trigger to ready up!", -1.5f);
                        }
                        //else
                        //{
                        //    StartCoroutine(ShowPlayersJoinedMessageAfterHearbeatAveraging());
                        //}
                        break;

                    case false:
                        if (HeartbeatAveraged)
                        {
                            Debug.Log("All players joined. Waiting for calculation of heartbeat average!");
                        }
                        //else
                        //{
                        //    Debug.Log("All players joined. Press R to ready up!");
                        //}
                        break;
                }
                break;
        }
    }

    private IEnumerator ShowPlayersJoinedMessageAfterHearbeatAveraging()
    {
        while (!HeartbeatAveraged || BeginAveraging)
        {
            yield return null;
        }

        ControllerMessageBox.Instance.ShowText("All players joined. Press the trigger to ready up!", -1.5f);
    }

    [TargetRpc]
    public void TargetPrintMessageToPlayerTypeDependent(NetworkConnection conn, string msg, float customTime, bool restorePrevText, bool nonQueue, float delay)
    {
        switch (playerType)
        {
            case PlayerType.Hunter:
                HunterUiCanvas.QueueToastMessage(msg, customTime);
                break;
            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        ControllerMessageBox.Instance.ShowText(msg, customTime, restorePrevText, nonQueue, delay);
                        break;
                    case false:
                        Debug.Log(msg);
                        break;
                }
                break;
        }
    }

    public float GetVisibility()
    {
        return Visibility;
    }

    [TargetRpc]
    public void TargetStartCountdownPlayerDependant(NetworkConnection conn, float duration)
    {
        switch (playerType)
        {
            case PlayerType.Hunter:
                HunterUiCanvas.StartCustomCountdown("Prey caught in: ", duration);
                break;
            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        ControllerMessageBox.Instance.StartCustomCountdown("Destroy the chains!" + Environment.NewLine + "Getting caught in: " + Environment.NewLine, duration);
                        break;
                    case false:
                        Debug.Log("Countdown started with duration: " + duration);
                        break;
                }
                break;
        }
    }

    [TargetRpc]
    public void TargetStopCountdownPlayerDependant(NetworkConnection conn)
    {
        switch (playerType)
        {
            case PlayerType.Hunter:
                HunterUiCanvas.StopCustomCountdown();
                break;
            case PlayerType.Prey:
                switch (GameSettings.Instance.VR)
                {
                    case true:
                        ControllerMessageBox.Instance.StopCustomCountdown();
                        break;
                    case false:
                        Debug.Log("Countdown stopped!");
                        break;
                }
                break;
        }
    }

    public void TargetDisconnectAfterTime(NetworkConnection conn, float time)
    {
        StartCoroutine(DisconnectAfterTimeRoutine(time));
    }

    IEnumerator DisconnectAfterTimeRoutine(float time)
    {
        yield return new WaitForSeconds(time);

        if (isServer)
        {
            NetworkManager.singleton.StopHost();
        }
        else
        {
            NetworkManager.singleton.StopClient();
        }
    }

    public void EnableAbilityMenu()
    {
        var menu = ControllerInteractor.Interactors[HandRole.LeftHand].AbilityMenu;
        menu.Disabled = false;
        menu.Close();
    }

    [TargetRpc]
    public void TargetSetInvisible(NetworkConnection conn, float time)
    {
        StartCoroutine(InvisibleForTime(time));

        StartCoroutine(InvisibleCountdownText(time));
    }

    private IEnumerator InvisibleCountdownText(float time)
    {
        yield return new WaitForSeconds(5f);

        if (GameSettings.Instance.VR)
        {
            ControllerMessageBox.Instance.StartCustomCountdown("Invisible for: ", time - 5);
        }
    }

    private bool IsInvisible = false;

    private IEnumerator InvisibleForTime(float time)
    {
        IsInvisible = true;

        yield return new WaitForSeconds(time);

        IsInvisible = false;
    }

    public void SetActiveDisconnectLabel(bool state)
    {
        if (playerType == PlayerType.Hunter)
        {
            HunterUiCanvas.DisconnectLabelText.gameObject.SetActive(state);
        }
    }
}
