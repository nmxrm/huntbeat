﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrUi : MonoBehaviour
{
    public GameObject PlayerMenu;
    public GameObject NetworkMenu;

    public void StartHost()
    {
        CustomNetworkManager.singleton.StartHost();
    }

    public void StartClient()
    {
        CustomNetworkManager.singleton.StartClient();
    }

    public void SelectHunter()
    {
        GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Hunter;
        OpenNetworkMenu();
    }

    public void SelectPrey()
    {
        GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Prey;
        OpenNetworkMenu();
    }

    public void OpenNetworkMenu()
    {
        PlayerMenu.SetActive(false);
        NetworkMenu.SetActive(true);
    }
}
