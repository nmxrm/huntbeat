﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityEnabler : MonoBehaviour
{

    public List<GameObject> Objects = new List<GameObject>();

    private void Start()
    {
        SetAllObjectsActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();

        if (player == null)
        {
            player = other.GetComponentInParent<Player>();
        }

        if (player != null && player.playerType == Player.PlayerType.Prey)
        {
            SetAllObjectsActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var player = other.GetComponent<Player>();

        if (player == null)
        {
            player = other.GetComponentInParent<Player>();
        }

        if (player != null && player.playerType == Player.PlayerType.Prey)
        {
            SetAllObjectsActive(false);
        }
    }

    private void SetAllObjectsActive(bool state)
    {
        foreach (var go in Objects)
        {
            go.SetActive(state);
        }
    }
}
