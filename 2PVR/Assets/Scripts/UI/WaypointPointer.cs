﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPointer : MonoBehaviour
{
    [HideInInspector]
    public bool ShowWaypoint;

    private Transform LookAtTransform;

    void Update()
    {
        if (ShowWaypoint)
        {
            var wp = Waypoint.GetCurrentlyActiveWaypoint();

            if (wp != null)
            {
                transform.LookAt(wp.transform);
            }
        }
        else if (LookAtTransform)
        {
            transform.LookAt(LookAtTransform);
        }
    }

    public void LookAtTransformForDuration(Transform t, float duration)
    {
        gameObject.SetActive(true);
        StartCoroutine(LookAtTransformForDurationRoutine(t, duration));
    }

    IEnumerator LookAtTransformForDurationRoutine(Transform t, float duration)
    {
        LookAtTransform = t;

        yield return new WaitForSeconds(duration);

        gameObject.SetActive(false);
        LookAtTransform = null;
    }
}
