﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class FoundGameToggle : MonoBehaviour
{
    public GameInfo AttachedGameInfo
    {
        get { return Info; }
        set
        {
            Info = value;
            Player.PlayerType missingType = Info.SelectedPlayerType == 0 ? (Player.PlayerType)1 : 0;
            TextSelf.text = string.Format("{0}   {1}   Missing player:{2}", Info.GameName, Info.IpAdress, missingType);
        }
    }

    private GameInfo Info;

    private Toggle ToggleSelf;

    private Text TextSelf;

    void Awake()
    {
        ToggleSelf = GetComponent<Toggle>();
        ToggleSelf.onValueChanged.AddListener(SetToggleActive);
        TextSelf = GetComponentInChildren<Text>();
    }

    public void SetToggleActive(bool state)
    {
        if (state)
        {
            MainMenu.Instance.ActiveToggle = this;
            MainMenu.Instance.JoinGameButton.interactable = true;
        }
    }
}
