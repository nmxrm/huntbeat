﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class OpenMenuButton : MonoBehaviour
{
    public MainMenu.Menu OpenMenu;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => MainMenu.Instance.OpenMenu(OpenMenu));
    }
}
