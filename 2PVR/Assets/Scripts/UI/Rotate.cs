﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float RotationSpeed;

    public Vector3 RotationVector;

    void Update()
    {
        transform.Rotate(RotationVector, Time.deltaTime * RotationSpeed);
    }
}
