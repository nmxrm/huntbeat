﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AnimatedPageView : MonoBehaviour
{
    private Vector3 EndPosition;

    private bool FirstRun = true;
    public AnimationCurve PageFlipCurve;
    public float PageFlipTime;

    private Coroutine PageRoutine;

    private RectTransform RectT;

    public int StartPage;

    private Vector3 StartPosition;

    private Vector2 ViewCenterPosition;

    public int CurrentPage { get; private set; }

    public int PageAmount { get; private set; }

    void Start()
    {
        RectT = GetComponent<RectTransform>();

        ViewCenterPosition = RectT.anchoredPosition;

        PageAmount = RectT.childCount;

        StartCoroutine(SetStartPage());
    }

    IEnumerator SetStartPage()
    {
        yield return null;

        ShowPage(StartPage, false);
    }

    IEnumerator PageFlipRoutine()
    {
        float i = 0;
        float rate = 1 / PageFlipTime;

        while (i < 1)
        {
            RectT.anchoredPosition = Vector3.Lerp(StartPosition, EndPosition, PageFlipCurve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        RectT.anchoredPosition = EndPosition;
        PageRoutine = null;
    }

    [ContextMenu("Next page")]
    public void NextPage()
    {
        int i = CurrentPage + 1;

        if (i >= PageAmount)
        {
            i = 0;
        }

        ShowPage(i);
    }

    [ContextMenu("Previous page")]
    public void PreviousPage()
    {
        int i = CurrentPage - 1;

        if (i < 0)
        {
            i = PageAmount - 1;
        }

        ShowPage(i);
    }

    public void ShowPage(int i, bool animated = true)
    {
        if (!isActiveAndEnabled)
        {
            return;
        }

        if (PageRoutine != null)
        {
            StopCoroutine(PageRoutine);
            PageRoutine = null;
        }

        CurrentPage = i;

        var child = RectT.transform.GetChild(CurrentPage).GetComponent<RectTransform>();

        StartPosition = RectT.anchoredPosition;
        if (CurrentPage == 0)
        {
            EndPosition = ViewCenterPosition;
        }
        else
        {
            EndPosition = new Vector2(ViewCenterPosition.x - (child.anchoredPosition.x - RectT.sizeDelta.x / 2),
                ViewCenterPosition.y);
        }

        if (animated)
        {
            PageRoutine = StartCoroutine(PageFlipRoutine());
        }
        else
        {
            RectT.anchoredPosition = EndPosition;
        }
    }

    public void DisplayStationDone()
    {
        ShowPage(0, false);
        PageAmount = 1;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (i == 0)
            {
                transform.GetChild(i).GetComponent<Text>().text = Environment.NewLine + Environment.NewLine + "Station done!";
            }
            else
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}