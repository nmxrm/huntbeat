﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartLogoAnimation : MonoBehaviour
{
    public Vector3 BeatScale;

    public AnimationCurve BeatCurve;

    public float WaitTime;

    private Vector3 DefaultScale;

    private Coroutine FullAnmiationRoutine;

    void Start()
    {
        DefaultScale = transform.localScale;
    }

    void OnEnable()
    {
        FullAnmiationRoutine = StartCoroutine(FullAnimation());
    }

    void OnDisable()
    {
        StopCoroutine(FullAnmiationRoutine);
        transform.localScale = DefaultScale;
    }

    private IEnumerator FullAnimation()
    {
        while (true)
        {
            yield return new WaitForSeconds(WaitTime);

            yield return StartCoroutine(SizeAnimation(0.15f, BeatScale));

            yield return StartCoroutine(SizeAnimation(0.15f, DefaultScale));

            yield return StartCoroutine(SizeAnimation(0.15f, BeatScale));

            yield return StartCoroutine(SizeAnimation(3f, DefaultScale)); 
        }
    }

    private IEnumerator SizeAnimation(float time, Vector3 endScale)
    {
        float i = 0;
        float rate = 1 / time;

        Vector3 startScale = transform.localScale;

        while (i < 1)
        {
            transform.localScale = Vector3.Lerp(startScale, endScale, BeatCurve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        transform.localScale = endScale;
    }
}
