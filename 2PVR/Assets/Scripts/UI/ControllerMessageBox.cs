﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerMessageBox : MonoBehaviour
{
    public static ControllerMessageBox Instance;

    private Text TextField;

    private string PrevMsg;

    private Coroutine TextTimeRoutine;

    public Coroutine CustomCountdownCoroutine;

    public AudioSource MessageAudio;

    void Awake()
    {
        TextField = GetComponentInChildren<Text>();
        TextField.gameObject.SetActive(false);
    }

    public void SetAsSingleton()
    {
        Instance = this;
    }

    [ContextMenu("Test message")]
    private void ShowTestMessage()
    {
        ShowTextWithTextfield("DIES IST EINE TESTNACHRICHT!");
    }

    private void ShowTextWithTextfield(string msg)
    {
        //if (!isActiveAndEnabled)
        //{
        //    return;
        //}

        TextField.gameObject.SetActive(true);

        //if (TextTimeRoutine != null)
        //{
        //    StopCoroutine(TextTimeRoutine);
        //}

        TextField.text = msg;
    }

    public void ShowText(string msg, float time = 5f, bool restorePrevText = false, bool nonQueue = false, float delay = 0f)
    {
        if (!isActiveAndEnabled)
        {
            return;
        }
        //gameObject.SetActive(true);

        StartCoroutine(QueueMessage(msg, time, restorePrevText, nonQueue, delay));
    }

    private IEnumerator QueueMessage(string msg, float time, bool restorePrevText, bool nonQueue = false, float delay = 0f)
    {
        if (delay > 0)
        {
            yield return new WaitForSeconds(delay);
        }

        if (!nonQueue)
        {
            while (TextTimeRoutine != null)
            {
                yield return null;
            }
        }
        else if (TextTimeRoutine != null)
        {
            StopCoroutine(TextTimeRoutine);
        }

        TextTimeRoutine = StartCoroutine(ShowTextForDuration(msg, time, restorePrevText));
    }

    public void ShowCountdown(Player player)
    {
        StartCoroutine(ShowCountdownRoutine(player));
    }

    private IEnumerator ShowCountdownRoutine(Player player)
    {
        yield return ShowTextForDuration("5", 1f);
        yield return ShowTextForDuration("4", 1f);
        yield return ShowTextForDuration("3", 1f);
        yield return ShowTextForDuration("2", 1f);
        yield return ShowTextForDuration("1", 1f);
        ShowText("Clear the stations and get the keys!", 5f);

        player.BlockMovement(false);
        player.EnableAbilityMenu();
        player.GameStartedAfterCountdown();
    }

    private IEnumerator ShowTextForDuration(string msg, float time, bool restorePrevText = false)
    {
        MessageAudio.Play();

        bool showPermanently = time <= -1f;

        if (restorePrevText)
        {
            PrevMsg = TextField.text;
        }

        ShowTextWithTextfield(msg);

        if (!showPermanently)
        {
            yield return new WaitForSeconds(time);
        }

        if (restorePrevText)
        {
            ShowTextWithTextfield(PrevMsg);
        }
        else if (!showPermanently)
        {
            HideBox();
        }

        TextTimeRoutine = null;
    }

    public void HideBox()
    {
        TextField.gameObject.SetActive(false);
    }

    public IEnumerator CustomCountdownRoutine(string msg, float duration)
    {
        float startTime = Time.time;
        float timeRemaining = duration;

        while (startTime + duration > Time.time)
        {
            StartCoroutine(ShowTextForDuration(msg + timeRemaining, 1f));
            timeRemaining--;
            yield return new WaitForSeconds(1f);
        }
    }

    public void StartCustomCountdown(string msg, float duration)
    {
        CustomCountdownCoroutine = StartCoroutine(CustomCountdownRoutine(msg, duration));
    }

    public void StopCustomCountdown()
    {
        if (CustomCountdownCoroutine != null)
        {
            StopCoroutine(CustomCountdownCoroutine);
        }
    }
}
