﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public enum Menu
    {
        MainMenu,
        PlayerSelect,
        HostJoinSelect,
        Host,
        ServerBrowser,
        HowToPlay
    }

    public static MainMenu Instance;

    public GameObject FoundGamePrefab;

    public GameObject GameBrowserContentObject;

    public Button RefreshButton, JoinGameButton;

    public InputField GameNameInputField;

    public Text DebugText;

    [Header("Menu objects")]
    public GameObject MainMenuObject;
    public GameObject PlayerSelectObject;
    public GameObject HostJoinSelectObject;
    public GameObject HostObject;
    public GameObject ServerBrowserObject;
    public GameObject HowToPlayObject;

    public GameObject AlertBoxObject;
    public Text AlertText;

    public Button BackButton;

    [HideInInspector]
    public FoundGameToggle ActiveToggle;

    private ToggleGroup FoundGamesToggleGroup;

    private Dictionary<string, GameObject> FoundGamesObjects = new Dictionary<string, GameObject>();

    private HostIPBroadcaster Broadcaster;
    private HostIPReceiver Receiver;

    private Menu CurrentOpenMenu;

    private Coroutine CreatingGameInfoTogglesRoutine;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        if (GameSettings.Instance.GameStartedFromMainMenuScene && SceneManager.GetActiveScene().name != "MainMenu")
        {
            Destroy(gameObject);
        }

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        Broadcaster = HostIPBroadcaster.Instance;
        Receiver = HostIPReceiver.Instance;
        FoundGamesToggleGroup = GetComponent<ToggleGroup>();
        MainMenu.Instance.JoinGameButton.interactable = false;
        FoundGamesToggleGroup = GetComponent<ToggleGroup>();

        AlertBoxObject.SetActive(false);

        //GameSettings.Instance.UnloadVr();

        OpenMenu(Menu.MainMenu);
    }

    public void SearchForGames()
    {
        ClearFoundGames();
        Receiver.StartLookingForGames();
        RefreshButton.interactable = false;
        JoinGameButton.interactable = false;
        CreatingGameInfoTogglesRoutine = StartCoroutine(InstantiateNewlyFoundGameInfoTogglesRoutine());
    }

    private IEnumerator InstantiateNewlyFoundGameInfoTogglesRoutine()
    {
        float starTime = Time.time;

        while (Time.time <= starTime + HostIPReceiver.Instance.RefreshTimoutTime)
        {
            var newGames = HostIPReceiver.Instance.GetMessagesValues();

            foreach (var gameInfo in newGames)
            {
                InstantiateGameInfoPrefab(gameInfo);
            }

            yield return null;
        }

        RefreshButton.interactable = true;
        CreatingGameInfoTogglesRoutine = null;
    }

    private void InstantiateGameInfoPrefab(GameInfo info)
    {
        if (!FoundGamesObjects.ContainsKey(info.IpAdress))
        {
            var toggle = Instantiate(FoundGamePrefab, GameBrowserContentObject.transform).GetComponent<FoundGameToggle>();
            toggle.AttachedGameInfo = info;
            toggle.GetComponent<Toggle>().group = FoundGamesToggleGroup;

            FoundGamesObjects.Add(toggle.AttachedGameInfo.IpAdress, toggle.gameObject);
        }
    }

    private void ClearFoundGames()
    {
        var gameInfoObjects = FoundGamesObjects.Values.ToList();

        foreach (var gameInfo in gameInfoObjects)
        {
            Destroy(gameInfo);
        }

        FoundGamesObjects.Clear();
    }

    public void JoinGame()
    {
        if (ActiveToggle)
        {
            if (ActiveToggle.AttachedGameInfo.SelectedPlayerType != (int)GameSettings.Instance.SelectedPlayerType)
            {
                //GameSettings.Instance.LoadVR();

                Receiver.StopLookingForGames();
                CustomNetworkManager.singleton.networkAddress = ActiveToggle.AttachedGameInfo.IpAdress;
                CustomNetworkManager.singleton.StartClient();
                SetActive(false);
            }
            else
            {
                ShowAlert("Cannot join game. Every game has to have a hunter and a prey.");
            }
        }
    }

    public void HostGame()
    {
        //GameSettings.Instance.LoadVR();

        GameSettings.Instance.Info.GameName = GameNameInputField.text;
        GameSettings.Instance.Info.SelectedPlayerType = (int)GameSettings.Instance.SelectedPlayerType;
        Broadcaster.StartSending();
        CustomNetworkManager.singleton.StartHost();
        SetActive(false);
    }

    public void SetActive(bool state)
    {
        gameObject.SetActive(state);
    }

    public void SetPlayerTypeHunter()
    {
        GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Hunter;
        OpenMenu(Menu.HostJoinSelect);

        if (GameSettings.Instance.VR)
        {
            ShowAlert("Warning! Hunter is currently designed to be played without VR! If you proceed you will encounter bugs!");
        }
    }

    public void SetPlayerTypePrey()
    {
        GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Prey;
        OpenMenu(Menu.HostJoinSelect);

        if (!GameSettings.Instance.VR)
        {
            ShowAlert("Warning! Prey is currently designed to be played with VR! If you proceed you will (probably) encounter bugs!");
        }
    }

    public void OpenMenu(Menu menu)
    {
        MainMenuObject.SetActive(false);
        PlayerSelectObject.SetActive(false);
        HostObject.SetActive(false);
        ServerBrowserObject.SetActive(false);
        HostJoinSelectObject.SetActive(false);
        HowToPlayObject.SetActive(false);

        switch (menu)
        {
            case Menu.MainMenu:
                MainMenuObject.SetActive(true);
                BackButton.gameObject.SetActive(false);
                break;
            case Menu.PlayerSelect:
                PlayerSelectObject.SetActive(true);
                BackButton.gameObject.SetActive(true);
                break;
            case Menu.Host:
                HostObject.SetActive(true);
                BackButton.gameObject.SetActive(true);
                break;
            case Menu.ServerBrowser:
                ServerBrowserObject.SetActive(true);
                BackButton.gameObject.SetActive(true);
                break;
            case Menu.HostJoinSelect:
                HostJoinSelectObject.SetActive(true);
                BackButton.gameObject.SetActive(true);
                break;
            case Menu.HowToPlay:
                HowToPlayObject.SetActive(true);
                BackButton.gameObject.SetActive(true);
                break;
        }

        CurrentOpenMenu = menu;
    }

    public void GoBack()
    {
        switch (CurrentOpenMenu)
        {
            case Menu.ServerBrowser:
                if (CreatingGameInfoTogglesRoutine != null)
                {
                    StopCoroutine(CreatingGameInfoTogglesRoutine);
                }
                Receiver.StopLookingForGames();
                ClearFoundGames();
                RefreshButton.interactable = true;
                JoinGameButton.interactable = false;
                OpenMenu(Menu.HostJoinSelect);
                break;
            case Menu.Host:
                OpenMenu(Menu.HostJoinSelect);
                break;
            case Menu.PlayerSelect:
                OpenMenu(Menu.MainMenu);
                break;
            case Menu.HostJoinSelect:
                OpenMenu(Menu.PlayerSelect);
                break;
            case Menu.HowToPlay:
                OpenMenu(Menu.MainMenu);
                break;
            default:
                break;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowAlert(string msg)
    {
        AlertText.text = msg;
        AlertBoxObject.SetActive(true);
    }
}
