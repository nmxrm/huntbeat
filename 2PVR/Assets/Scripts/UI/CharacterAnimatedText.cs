﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CharacterAnimatedText : MonoBehaviour
{
    private Text TextLabel;

    private Coroutine TextAnimationRoutine;

    void Start()
    {
        TextLabel = GetComponent<Text>();
        TextLabel.text = String.Empty;
    }

    public void ShowText(string text)
    {
        if (TextAnimationRoutine != null)
        {
            StopCoroutine(TextAnimationRoutine);
            TextAnimationRoutine = null;
        }

        TextAnimationRoutine = StartCoroutine(ShowTextRoutine(text));
    }

    IEnumerator ShowTextRoutine(string text)
    {
        TextLabel.text = string.Empty;

        foreach (var character in text)
        {
            TextLabel.text += character;
            yield return new WaitForSeconds(0.05f);
        }

        TextAnimationRoutine = null;
    }

    public void HideText()
    {
        if (TextAnimationRoutine != null)
        {
            StopCoroutine(TextAnimationRoutine);
            TextAnimationRoutine = null;
        }

        TextLabel.text = string.Empty;
    }
}
