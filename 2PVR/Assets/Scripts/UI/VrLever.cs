﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrLever : MonoBehaviour
{
    private bool Move;

    //private ControllerInteractor FoundInteractor;

    private GameObject Target;

    private Quaternion StartRotation;

    public Interactable AddToInteractable;

    public bool BlockXRotation, BlockYRotation, BlockZRotation;

    public float MaxXAngle;

    public bool MoveBack = true;

    public float MoveBackSpeed = 1f;

    void Start()
    {
        StartRotation = transform.rotation;

        if (AddToInteractable)
        {
            AddToInteractable.OnObjectInteractionEvent.AddListener(DecideMovementTowardsTarget);
        }
    }

    void Update()
    {
        if (Move)
        {
            Vector3 direction = Target.transform.position - transform.position;
            Vector3 rotation = Quaternion.LookRotation(direction).eulerAngles;

            if (BlockXRotation)
            {
                rotation = new Vector3(transform.rotation.eulerAngles.x, rotation.y, rotation.z);
            }
            if (BlockYRotation)
            {
                rotation = new Vector3(rotation.x, transform.rotation.eulerAngles.y, rotation.z);
            }
            if (BlockZRotation)
            {
                rotation = new Vector3(rotation.x, rotation.y, transform.rotation.eulerAngles.z);
            }

            var angle = Vector3.Angle(StartRotation.eulerAngles, transform.rotation.eulerAngles);

            if (angle >= MaxXAngle)
            {
                Debug.Log("Lever activated");
                Move = false;
            }
            //var cross = Vector3.Cross(StartRotation.eulerAngles, transform.rotation.eulerAngles);
            //if (cross.y < 0)
            //{
            //    angle = -angle;
            //}
            //Debug.Log(angle);

            //rotation = new Vector3(ClampAngle(rotation.x, StartRotation.eulerAngles.x, StartRotation.eulerAngles.x + MaxXAngle), rotation.y, rotation.z);
            //rotation = new Vector3(ClampAngle(rotation.x, StartRotation.eulerAngles.x, StartRotation.eulerAngles.x + MaxXAngle), rotation.y, rotation.z);

            //rotation = ClampRotationAroundXAxis(Quaternion.Euler(rotation), 0,
            //   MaxXAngle).eulerAngles;

            //float angle = Vector3.Angle(StartRotation.eulerAngles, rotation);

            //if (angle>MaxXAngle)
            //{
            //rotation = new Vector3(StartRotation.eulerAngles.x + MaxXAngle, rotation.y, rotation.z);

            //}
            //else if (angle<)
            //{

            //}

            transform.rotation = Quaternion.Euler(rotation);
        }
        else if (MoveBack)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, StartRotation, Time.deltaTime * MoveBackSpeed);
        }
    }

    public void MoveTowardsTarget(GameObject target)
    {
        Move = true;
        Target = target;
    }

    public void StopMovingTowardsTarget()
    {
        Move = false;
        Target = null;
    }

    public void DecideMovementTowardsTarget(GameObject target)
    {
        if (Move)
        {
            StopMovingTowardsTarget();
        }
        else
        {
            MoveTowardsTarget(target);
        }
    }

    //float ClampAngle(float angle, float from, float to)
    //{
    //    if (angle > 180) angle = 360 - angle;
    //    angle = Mathf.Clamp(angle, from, to);
    //    if (angle < 0) angle = 360 + angle;

    //    return angle;
    //}

    //Quaternion ClampRotationAroundXAxis(Quaternion q, float minAngle, float maxAngle)
    //{
    //    q.x /= q.w;
    //    q.y /= q.w;
    //    q.z /= q.w;
    //    q.w = 1.0f;

    //    float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

    //    angleX = Mathf.Clamp(angleX, minAngle, maxAngle);

    //    q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

    //    return q;
    //}

    //private float ClampAngle(float angle, float min, float max)
    //{
    //    if (angle < -360F)
    //    {
    //        angle += 360F;
    //    }

    //    if (angle > 360F)
    //    {
    //        angle -= 360F;
    //    }

    //    return Mathf.Clamp(angle, min, max);
    //}


    //private void OnTriggerEnter(Collider other)
    //{
    //    var interactor = other.GetComponent<ControllerInteractor>();

    //    if (interactor)
    //    {
    //        FoundInteractor = interactor;
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    var interactor = other.GetComponent<ControllerInteractor>();

    //    if (interactor)
    //    {
    //        FoundInteractor = null;
    //    }
    //}
}
