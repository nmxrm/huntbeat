﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrButton : MonoBehaviour
{
    public float ReturnSpeed;

    public bool lockX, lockY, lockZ;

    private Vector3 DefaultPosition;

    void Start()
    {
        DefaultPosition = transform.position;
    }

    void Update()
    {
        var pos = transform.position;

        if (lockX)
        {
            pos.x = DefaultPosition.x;
        }
        if (lockY)
        {
            pos.y = DefaultPosition.y;
        }
        if (lockZ)
        {
            pos.z = DefaultPosition.z;
        }

        transform.position = pos;

        transform.position = Vector3.Lerp(transform.position, DefaultPosition, ReturnSpeed * Time.deltaTime);
    }
}
