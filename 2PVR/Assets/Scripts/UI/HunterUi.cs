﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HunterUi : MonoBehaviour
{
    public Image CatchingCooldownImage;
    public Image SensingCooldownImage;

    public Text CatchingCooldownText;
    public Text PowerupsNeededText;

    public Text ToastMessageBoxText;

    public Text DisconnectLabelText;

    public float DefaultShowToastTime = 3f;

    private Coroutine CurrentToastRoutine;

    private Coroutine CustomCountdownCoroutine;

    public AudioSource CatchingCooldownAudio;
    public AudioSource MessageAudio;

    void Awake()
    {
        CatchingCooldownImage.fillAmount = 0;
        CatchingCooldownText.gameObject.SetActive(false);
        ToastMessageBoxText.gameObject.SetActive(false);
        DisconnectLabelText.gameObject.SetActive(false);
    }

    [ContextMenu("Test cooldown animation")]
    private void TestCooldown()
    {
        StartCooldownAnimation(10f);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="text"></param>
    /// <param name="time"> smaller than 0 = default time, smaller than -1 = show text permanently</param>
    /// <param name="showPermanently"></param>
    public void QueueToastMessage(string text, float time = -0.5f/*, bool showPermanently = false*/)
    {
        bool showPermanently = false;

        if (time < 0 && time > -1)
        {
            time = DefaultShowToastTime;
        }

        if (time <= -1)
        {
            showPermanently = true;
        }

        StartCoroutine(QueueToastRoutine(text, time, showPermanently));
    }

    private IEnumerator QueueToastRoutine(string text, float time, bool showPermanently = false, bool animate = true)
    {
        while (CurrentToastRoutine != null)
        {
            yield return null;
        }

        CurrentToastRoutine = StartCoroutine(ToastMessageAnimation(text, time, showPermanently, animate));
    }

    public void StartCooldownAnimation(float time)
    {
        StartCoroutine(CooldownImageRoutine(time));
        StartCoroutine(CooldownTextRoutine(time));
        //play sound delayed
        CatchingCooldownAudio.PlayDelayed(time);
    }

    private IEnumerator CooldownImageRoutine(float time)
    {
        float i = 0;
        float rate = 1 / time;

        while (i < 1)
        {
            CatchingCooldownImage.fillAmount = Mathf.Lerp(1, 0, i);
            i += rate * Time.deltaTime;
            yield return null;
        }

        CatchingCooldownImage.fillAmount = 0;
    }

    private IEnumerator CooldownTextRoutine(float time)
    {
        CatchingCooldownText.gameObject.SetActive(true);

        while (time >= 0)
        {
            CatchingCooldownText.text = "" + time;

            yield return new WaitForSeconds(1f);

            time -= 1f;
        }

        CatchingCooldownText.gameObject.SetActive(false);
    }

    [ContextMenu("Test queue toast")]
    private void TestQueueToastAnimation()
    {
        QueueToastMessage("dies ist eine testnachricht");
    }

    private IEnumerator ToastMessageAnimation(string text, float time, bool showPermanently = false, bool animate = true)
    {
        MessageAudio.Play();

        ToastMessageBoxText.text = string.Empty;
        ToastMessageBoxText.gameObject.SetActive(true);

        if (animate)
        {
            foreach (var character in text)
            {
                ToastMessageBoxText.text += character;
                yield return new WaitForSeconds(0.035f);
            }
        }
        else
        {
            ToastMessageBoxText.text = text;
        }

        if (!showPermanently)
        {
            yield return new WaitForSeconds(time);

            ToastMessageBoxText.gameObject.SetActive(false);
        }

        CurrentToastRoutine = null;
    }

    public void UpdatePowerUpUi(int powerUpAmountCollected)
    {
        int amountNeeded = 3 - powerUpAmountCollected;

        SensingCooldownImage.fillAmount = Mathf.Clamp01((1f / 3f) * amountNeeded);
        PowerupsNeededText.text = amountNeeded == 0 ? "Sense prey ready" : "Power ups needed: " + amountNeeded;

        if (amountNeeded == 0)
        {
            QueueToastMessage("Sensing prey ready!");
        }
    }

    public void StartCountdown(Player player)
    {
        StartCoroutine(CountdownRoutine(player));
    }

    private IEnumerator CountdownRoutine(Player player)
    {
        yield return StartCoroutine(QueueToastRoutine("5", 1f));
        yield return StartCoroutine(QueueToastRoutine("4", 1f));
        yield return StartCoroutine(QueueToastRoutine("3", 1f));
        yield return StartCoroutine(QueueToastRoutine("2", 1f));
        yield return StartCoroutine(QueueToastRoutine("1", 1f));
        StartCoroutine(QueueToastRoutine("Hunt!", 1f));
        player.BlockMovement(false);
        player.GameStartedAfterCountdown();
    }

    private IEnumerator CustomCountdownRoutine(string msg, float duration)
    {
        float startTime = Time.time;
        float timeRemaining = duration;

        while (startTime + duration > Time.time)
        {
            StartCoroutine(QueueToastRoutine(msg + timeRemaining, 1f, false, false));
            timeRemaining--;
            yield return new WaitForSeconds(1f);
        }
    }

    public void StartCustomCountdown(string msg, float duration)
    {
        CustomCountdownCoroutine = StartCoroutine(CustomCountdownRoutine(msg, duration));
    }

    public void StopCustomCountdown()
    {
        if (CustomCountdownCoroutine != null)
        {
            StopCoroutine(CustomCountdownCoroutine);
        }
    }
}
