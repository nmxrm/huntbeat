﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractorIsPreyPossibilityCheck : InteractablePossibilityCheck
{

    public override bool InteractionPossible(Player interactingPlayer, GameObject go)
    {
        LastInteractingPlayer = interactingPlayer;

        return interactingPlayer.playerType == Player.PlayerType.Prey;
    }
}
