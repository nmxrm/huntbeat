﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Interactable))]
public class Carriable : MonoBehaviour
{
    public static Dictionary<HandRole, Carriable> CurrentCarriedObjects = new Dictionary<HandRole, Carriable> { { HandRole.LeftHand, null }, { HandRole.RightHand, null } };

    [HideInInspector]
    public ControllerInteractor Interactor;

    private Interactable AttachedInteractable;

    public bool Carried { get; private set; }

    public UnityEvent OnDropEvent = new UnityEvent();

    public bool KeepKinematic;

    void Start()
    {
        AttachedInteractable = GetComponent<Interactable>();
        AttachedInteractable.OnInteractionEvent.AddListener(DecidePickUpDrop);
    }

    public void Pickup(bool dontSetParent = false)
    {
        if (Interactor)
        {
            var currentCarriedObject = CurrentCarriedObjects[Interactor.Hand];

            if (currentCarriedObject == null)
            {
                GetComponent<Rigidbody>().isKinematic = true;
                if (dontSetParent)
                {
                    CopyTransform copy = gameObject.GetComponent<CopyTransform>();
                    if (copy == null)
                    {
                        copy = gameObject.AddComponent<CopyTransform>();
                    }
                    copy.CopyPosition = Interactor.transform;
                    copy.CopyRotation = Interactor.transform;
                }
                else
                {
                    transform.SetParent(Interactor.transform);
                }
                CurrentCarriedObjects[Interactor.Hand] = this;

                Carried = true;
            }
        }
    }

    public void Drop()
    {
        var rb = GetComponent<Rigidbody>();
        transform.SetParent(null);
        if (!KeepKinematic)
        {
            rb.isKinematic = false;
        }

        var copy = GetComponent<CopyTransform>();
        if (copy)
        {
            copy.CopyPosition = null;
            copy.CopyRotation = null;
        }

        if (Interactor)
        {
            rb.velocity = VivePose.GetVelocity(Interactor.Hand);
            rb.angularVelocity = VivePose.GetAngularVelocity(Interactor.Hand);
        }

        //CurrentCarriedObject = null;
        RemoveThisCarriable();

        Carried = false;

        OnDropEvent.Invoke();
    }

    public void DecidePickUpDrop()
    {
        if (Carried)
        {
            if (Interactor!=null)
            {
                if (Interactor.Hand == GetCarryingHand())
                {
                    Drop();
                }
                else
                {
                    Drop();
                    Pickup();
                } 
            }
            else
            {
                Drop();
            }
        }
        else
        {
            Pickup();
        }
    }

    private void RemoveThisCarriable()
    {
        var keys = CurrentCarriedObjects.Keys.ToList();
        var values = CurrentCarriedObjects.Values.ToList();

        for (int i = 0; i < values.Count; i++)
        {
            var carriable = values[i];

            if (carriable == this)
            {
                CurrentCarriedObjects[keys[i]] = null;
                break;
            }
        }
    }

    private HandRole GetCarryingHand()
    {
        var keys = CurrentCarriedObjects.Keys.ToList();
        var values = CurrentCarriedObjects.Values.ToList();

        for (int i = 0; i < values.Count; i++)
        {
            var c = values[i];

            if (c == this)
            {
                return keys[i];
            }
        }

        return HandRole.Invalid;
    }
}
