﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(Interactable))]
public class Key : MonoBehaviour
{
    public static List<Key> Keys = new List<Key>();

    public enum KeyType
    {
        Red,
        Blue,
        Green
    }

    public KeyType KeyColor;

    [SerializeField] private bool SetColorOnStart = false;

    private Interactable AttachedInteractable;

    void Start()
    {
        ApplyKeyColor();

        Keys.Add(this);

        if (Keys.Count > 3)
        {
            Debug.LogWarning("There are more than 3 keys!");
        }

        AttachedInteractable = GetComponent<Interactable>();

        AttachedInteractable.OnInteractionEvent.AddListener(() => Inventory.Instance.Pickup(this));

        if (SetColorOnStart)
        {
            var missingTypes = ExitGate.Instance.GetMissingKeyTypes();

            foreach (var missingType in missingTypes)
            {
                if (!KeyTypeExists(missingType))
                {
                    KeyColor = missingType;
                    ApplyKeyColor();
                    break;
                }
            }
        }
    }

    void OnDestroy()
    {
        Keys.Remove(this);
    }

    public static bool KeyTypeExists(KeyType keyType)
    {
        foreach (var key in Keys)
        {
            if (key.KeyColor == keyType)
            {
                return true;
            }
        }

        return false;
    }

    private void ApplyKeyColor()
    {
        var renderer = GetComponent<Renderer>();
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        renderer.GetPropertyBlock(block);

        switch (KeyColor)
        {
            case KeyType.Blue:
                block.SetColor("_Color", Color.blue);
                break;
            case KeyType.Green:
                block.SetColor("_Color", Color.green);
                break;
            case KeyType.Red:
                block.SetColor("_Color", Color.red);
                break;
        }

        renderer.SetPropertyBlock(block);
    }
}
