﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class Trace : NetworkBehaviour
{
    public static List<Trace> AllTraces = new List<Trace>();

    [SerializeField]
    private static float DistanceBetweenTraces = 3.5f;

    // negative value means unlimited traces. probably not advisable.
    [SerializeField]
    private int MaxTracesAmount;

    [SerializeField]
    private int ShowTracesAmount;

    [SerializeField]
    private int ShowTracesTime;

    //private Trace Predecessor;

    private Renderer TraceRenderer;

    private Coroutine ShowTracesRoutine;

    private bool GetsDeleted = false;

    [HideInInspector]
    public Player Creator;

    private float TimeCreated;

    void Awake()
    {
        TraceRenderer = GetComponent<Renderer>();
        TraceRenderer.enabled = false;
    }

    void Start()
    {
        if (!CanBeSpawned(transform.position))
        {
            Destroy(gameObject);
        }

        TimeCreated = Time.time;

        AllTraces.Add(this);

        if (MaxTracesAmount > -1)
        {
            if (AllTraces.Count > MaxTracesAmount)
            {
                GetOldestNotFlaggedForDeletionTrace().FlagForDeletion();
            }
        }
    }

    void Update()
    {
        if (Creator != null && Creator.playerType == Player.PlayerType.Hunter)
        {
            FlagForDeletion();
        }
    }

    void OnDestroy()
    {
        if (AllTraces.Contains(this))
        {
            AllTraces.Remove(this);
        }
    }

    [ContextMenu("Flag for deletion")]
    public void FlagForDeletion()
    {
        GetsDeleted = true;

        if (TraceRenderer.enabled == false && ShowTracesRoutine == null)
        {
            Destroy(gameObject);
        }
        else
        {
            StartCoroutine(DeletionRoutine());
        }
    }

    private IEnumerator DeletionRoutine()
    {
        while (TraceRenderer.enabled || ShowTracesRoutine != null)
        {
            yield return null;
        }

        Destroy(gameObject);
    }

    public static bool CanBeSpawned(Vector3 position)
    {
        if (AllTraces.Count == 0)
        {
            return true;
        }

        return Vector3.Distance(AllTraces[AllTraces.Count - 1].transform.position, position) >
               DistanceBetweenTraces;
    }

    [ContextMenu("Show traces")]
    public void ShowTraces()
    {
        ShowTracesRoutine = StartCoroutine(ShowTracesForSeconds(ShowTracesTime));
    }

    IEnumerator ShowTracesForSeconds(float seconds)
    {
        Trace[] showTraces;
        int startIndex = 0;

        if (AllTraces.Count < ShowTracesAmount)
        {
            showTraces = new Trace[AllTraces.Count];
            AllTraces.CopyTo(showTraces);
        }
        else
        {
            startIndex = Random.Range(0, AllTraces.Count - ShowTracesAmount);
            showTraces = new Trace[ShowTracesAmount];

            for (int i = startIndex; i < startIndex + ShowTracesAmount; i++)
            {
                showTraces[i - startIndex] = AllTraces[i];
            }
        }

        float time = 0;

        for (int i = 0; i < showTraces.Length; i++)
        {
            time += showTraces[i].TimeCreated;
            showTraces[i].TraceRenderer.enabled = true;
        }

        time /= showTraces.Length;
        time = Time.time - time;
        time = Mathf.FloorToInt(time);

        TimeSpan formattedTime = TimeSpan.FromSeconds(time);

        if (time > 60)
        {
            GameManagerInteractor.Instance.CmdSendHunterToastMessage("The prey was here about " + string.Format("{0:D2}:{1:D2}", formattedTime.Minutes, formattedTime.Seconds) + " minutes ago!", 10f);
        }
        else
        {
            GameManagerInteractor.Instance.CmdSendHunterToastMessage("The prey was here about " + string.Format("{0:D2}", formattedTime.Seconds) + " seconds ago!", 10f);
        }

        yield return new WaitForSeconds(seconds);

        for (int i = 0; i < showTraces.Length; i++)
        {
            showTraces[i].TraceRenderer.enabled = false;
        }

        ShowTracesRoutine = null;
    }

    private static Trace GetOldestNotFlaggedForDeletionTrace()
    {
        foreach (var trace in AllTraces)
        {
            if (trace.GetsDeleted == false)
            {
                return trace;
            }
        }

        return null;
    }

    public static void FlagAllForDeletionCreatedByPlayer(Player player)
    {
        foreach (var trace in AllTraces)
        {
            if (trace.Creator == player)
            {
                trace.FlagForDeletion();
            }
        }
    }
}
