﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowNearestStationAbility : ControllerAbility
{
    public override bool UseAbility()
    {
        if (base.UseAbility())
        {
            var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();

            var nextStation = Station
                .GetClosestStation(Player.GetPlayerByType(Player.PlayerType.Prey).transform.position, true);

            if (nextStation != null)
            {
                pointer.LookAtTransformForDuration(nextStation.transform, 5f);
                return true;
            }
            return false;
        }

        return false;
    }

    // debug test

    //[ContextMenu("TEST")]
    //public void Debug()
    //{
    //    //var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();

    //    var nextStation = Station
    //        .GetClosestStation(Player.GetPlayerByType(Player.PlayerType.Prey).transform.position, true);

    //    if (nextStation != null)
    //    {
    //        //pointer.LookAtTransformForDuration(nextStation.transform, 5f);
    //    }
    //}
}
