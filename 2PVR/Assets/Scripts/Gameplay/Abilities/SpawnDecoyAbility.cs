﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDecoyAbility : ControllerAbility
{
    public override bool UseAbility()
    {
        if (base.UseAbility())
        {
            var currentCarriedObject = Carriable.CurrentCarriedObjects[OwningMenu.OwningInteractor.Hand];

            if (currentCarriedObject == null)
            {
                OwningMenu.OwningInteractor.GetPlayer().InitiateSpawningDecoy(OwningMenu.OwningInteractor);
                return true;
            }
        }

        return false;
    }
}
