﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHeartAbility : ControllerAbility
{
    private Player Prey;
    void Start()
    {
        Prey = GetComponentInParent<Player>();
        CurrentCooldownTime = 1;
    }

    void Update()
    {
        AbilityText = Prey.CurrentHeartbeat + Environment.NewLine + Mathf.FloorToInt(Prey.GetVisibility() * 100) + "%";
    }

    public override bool UseAbility()
    {
        return false;
    }
}
