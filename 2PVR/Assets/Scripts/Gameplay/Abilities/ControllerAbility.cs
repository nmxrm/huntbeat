﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAbility : MonoBehaviour
{
    // probably use seconds
    public float CooldownTime;

    public float CurrentCooldownTime { get; protected set; }

    public string AbilityText;

    private Coroutine CooldownRunningRoutine;

    [HideInInspector]
    public ControllerAbilityMenu OwningMenu;

    [ContextMenu("Use ability")]
    public virtual bool UseAbility()
    {
        if (CooldownRunningRoutine != null)
        {
            return false;
        }

        CooldownRunningRoutine = StartCoroutine(CooldownRoutine());
        return true;
    }

    private IEnumerator CooldownRoutine()
    {
        CurrentCooldownTime = CooldownTime;

        while (CurrentCooldownTime > 0)
        {
            yield return new WaitForSeconds(1f);
            CurrentCooldownTime--;
        }

        CooldownRunningRoutine = null;
    }
}
