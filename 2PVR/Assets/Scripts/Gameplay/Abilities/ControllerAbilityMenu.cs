﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerAbilityMenu : MonoBehaviour
{
    public bool CloseOnStart;
    public bool RotateIndex;
    public float TimeToClose = 5f;

    public float TransitionTime;

    public Transform AbilityParent;

    public Transform AbilityTempParent;

    public Text AbilityInfoText;

    public Canvas ButtonsCanvas;

    public bool IsOpen { get; private set; }

    [HideInInspector] public bool Disabled = true;

    [SerializeField]
    private AnimationCurve Curve;

    private ControllerAbility[] Abilities;

    private float CloseStartTime;

    private int CurrentAbilityIndex = 0;

    private Coroutine ClosingRoutine;
    private Coroutine TransitionRoutine;
    private Coroutine UpdateTextRoutine;

    private Vector3 DefaultPosition;

    private Renderer[] AllRenderers;

    private GameObject DisabledAbilities;

    public ControllerInteractor OwningInteractor { get; private set; }

    void Awake()
    {
        Abilities = GetComponentsInChildren<ControllerAbility>();

        DisabledAbilities = new GameObject("disabled");
        DisabledAbilities.transform.SetParent(transform);

        var disabledAbilities = GetComponentsInChildren<ControllerAbility>(true);
        foreach (var ability in disabledAbilities)
        {
            if (!ability.gameObject.activeSelf)
            {
                ability.transform.SetParent(DisabledAbilities.transform);
            }
        }
    }

    void Start()
    {
        AllRenderers = GetComponentsInChildren<Renderer>();
        DefaultPosition = transform.localPosition;

        if (CloseOnStart)
        {
            Close();
        }
    }

    public void SetInteractor(ControllerInteractor interactor)
    {
        OwningInteractor = interactor;

        foreach (var ability in Abilities)
        {
            ability.OwningMenu = this;
        }
    }

    // DEBUG
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            OpenOrNextAbility();
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            OpenOrPreviousAbility();
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (IsOpen)
            {
                UseCurrentAbility();
            }
        }
    }

    [ContextMenu("Open")]
    public void Open(bool keepOpen = false)
    {
        //gameObject.SetActive(true);
        if (ButtonsCanvas != null)
        {
            ButtonsCanvas.gameObject.SetActive(true);
        }
        SetRenderersActive(true);
        CloseStartTime = Time.time;
        AbilityInfoText.gameObject.SetActive(true);
        UpdateTextRoutine = StartCoroutine(UpdateAbilityText());
        IsOpen = true;
        if (!keepOpen)
        {
            ClosingRoutine = StartCoroutine(CloseAfterTime());
        }
    }

    private void SetAbilityText()
    {
        var currentAbility = Abilities[CurrentAbilityIndex];

        if (currentAbility.CurrentCooldownTime > 0 && !(currentAbility is ShowHeartAbility))
        {
            AbilityInfoText.text = currentAbility.AbilityText + " - " + currentAbility.CurrentCooldownTime;
        }
        else
        {
            AbilityInfoText.text = currentAbility.AbilityText;
        }
    }

    private IEnumerator UpdateAbilityText()
    {
        while (true)
        {
            SetAbilityText();
            yield return null;
        }
    }

    private IEnumerator CloseAfterTime()
    {
        while (Time.time <= CloseStartTime + TimeToClose)
        {
            yield return null;
        }

        ClosingRoutine = null;
        Close();
    }

    [ContextMenu("Close")]
    public void Close()
    {
        if (ClosingRoutine != null)
        {
            StopCoroutine(ClosingRoutine);
            ClosingRoutine = null;
        }

        if (TransitionRoutine != null)
        {
            StopCoroutine(TransitionRoutine);
            TransitionRoutine = null;
            AbilityParent.position = DefaultPosition;
        }

        if (UpdateTextRoutine != null)
        {
            StopCoroutine(UpdateTextRoutine);
        }

        IsOpen = false;
        //gameObject.SetActive(false);
        AbilityInfoText.gameObject.SetActive(false);
        SetRenderersActive(false);
        if (ButtonsCanvas != null)
        {
            ButtonsCanvas.gameObject.SetActive(false);
        }
    }

    [ContextMenu("Next Ability")]
    public void NextAbility()
    {
        if (Disabled)
        {
            return;
        }

        CloseStartTime = Time.time;

        if (RotateIndex)
        {
            CurrentAbilityIndex++;
            if (CurrentAbilityIndex >= Abilities.Length)
            {
                CurrentAbilityIndex = 0;
            }
        }
        else
        {
            CurrentAbilityIndex = Mathf.Clamp(++CurrentAbilityIndex, 0, Abilities.Length - 1);
        }

        TransistionToAbility(CurrentAbilityIndex);
    }

    private void TransistionToAbility(int abilityIndex)
    {
        AbilityInfoText.text = Abilities[CurrentAbilityIndex].AbilityText;

        if (TransitionRoutine != null)
        {
            StopCoroutine(TransitionRoutine);
        }

        TransitionRoutine = StartCoroutine(CenterAbility(CurrentAbilityIndex));
    }

    [ContextMenu("Prev Ability")]
    public void PreviousAbility()
    {
        if (Disabled)
        {
            return;
        }

        CloseStartTime = Time.time;

        if (RotateIndex)
        {
            CurrentAbilityIndex--;
            if (CurrentAbilityIndex < 0)
            {
                CurrentAbilityIndex = Abilities.Length - 1;
            }
        }
        else
        {
            CurrentAbilityIndex = Mathf.Clamp(--CurrentAbilityIndex, 0, Abilities.Length - 1);
        }

        TransistionToAbility(CurrentAbilityIndex);
    }

    private IEnumerator CenterAbility(int index)
    {
        ReparentAbilites(index);

        float rate = 1 / TransitionTime;
        float i = 0;

        Vector3 startPos = AbilityParent.transform.localPosition;

        while (i < 1)
        {
            AbilityParent.transform.localPosition = Vector3.Lerp(startPos, DefaultPosition, Curve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        AbilityParent.transform.localPosition = DefaultPosition;
        TransitionRoutine = null;
    }

    private void ReparentAbilites(int index)
    {
        for (int i = 0; i < AbilityParent.childCount; i++)
        {
            var child = AbilityParent.GetChild(i);
            if (child.gameObject.activeSelf)
            {
                child.SetParent(AbilityTempParent);
                i--;
            }
        }

        AbilityParent.transform.localPosition = AbilityTempParent.GetChild(index).localPosition;

        for (int i = 0; i < AbilityTempParent.childCount; i++)
        {
            var child = AbilityTempParent.GetChild(i);
            if (child.gameObject.activeSelf)
            {
                child.SetParent(AbilityParent);
                i--;
            }
        }
    }

    public void UseCurrentAbility()
    {
        if (Abilities[CurrentAbilityIndex].UseAbility())
        {
            Close();
        }
    }

    [ContextMenu("Open or next")]
    public void OpenOrNextAbility()
    {
        if (Disabled)
        {
            return;
        }

        if (!IsOpen)
        {
            Open();
        }
        else
        {
            NextAbility();
        }
    }

    [ContextMenu("Open or prev")]
    public void OpenOrPreviousAbility()
    {
        if (Disabled)
        {
            return;
        }

        if (!IsOpen)
        {
            Open();
        }
        else
        {
            PreviousAbility();
        }
    }

    [ContextMenu("Switch disabled")]
    private void SwitchDisabled()
    {
        Disabled = !Disabled;
        Debug.Log("Disabled: " + Disabled);
    }

    private void SetRenderersActive(bool state)
    {
        foreach (var renderer in AllRenderers)
        {
            renderer.enabled = state;
        }
    }
}
