﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.UI;
using Valve.Newtonsoft.Json.Utilities;

public class ControllerInteractor : MonoBehaviour
{
    public static Dictionary<HandRole, ControllerInteractor> Interactors =
        new Dictionary<HandRole, ControllerInteractor>();

    public HandRole Hand;

    public Material HighlightMaterial;

    public ControllerAbilityMenu AbilityMenu;

    public CharacterAnimatedText InteractableText;

    [SerializeField] private Player OwningPlayer;

    private Interactable otherInteractable;

    void Start()
    {
        // islocalplayer apparently gets set in awake
        if (OwningPlayer != null && OwningPlayer.isLocalPlayer)
        {
            if (Interactors.ContainsKey(Hand))
            {
                Debug.LogWarningFormat("Interactor for hand role: {0} already exists apparently!", Hand.ToString());
            }
            else
            {
                Interactors.Add(Hand, this);
            }

            if (AbilityMenu != null)
            {
                AbilityMenu.SetInteractor(this);

            }
        }
    }

    void OnDestroy()
    {
        Interactors.Remove(Hand);
    }

    public Player GetPlayer()
    {
        return OwningPlayer;
    }

    private Interactable OtherInteractable
    {
        get { return otherInteractable; }
        set
        {
            if (value != otherInteractable)
            {
                if (otherInteractable)
                {
                    RemoveHightlightMaterialFromRenderer(otherInteractable.gameObject);
                }
            }

            otherInteractable = value;

            if (otherInteractable)
            {
                //ViveInput.TriggerHapticPulse(Hand, 10000);
                AddHighlightMaterialToRenderer(otherInteractable.gameObject);
                if (InteractableText != null)
                {
                    InteractableText.ShowText(otherInteractable.InfoText);
                }
            }
            else
            {
                InteractableText.HideText();
            }
        }
    }

    private void AddHighlightMaterialToRenderer(GameObject go)
    {
        if (HighlightMaterial != null)
        {
            List<Renderer> renderers = new List<Renderer>();

            renderers.Add(go.GetComponent<Renderer>());
            renderers.AddRange(go.GetComponentsInChildren<Renderer>());

            if (renderers.Count > 0)
            {
                foreach (var renderer in renderers)
                {
                    if (renderer != null)
                    {
                        var mats = renderer.materials;
                        var matsList = mats.ToList();
                        if (!matsList.Find(material => material.name.Contains(HighlightMaterial.name)))
                        {
                            matsList.Add(HighlightMaterial);
                            renderer.materials = matsList.ToArray();
                        }
                    }
                }
            }
        }
    }

    private void RemoveHightlightMaterialFromRenderer(GameObject go)
    {
        if (HighlightMaterial != null)
        {
            List<Renderer> renderers = new List<Renderer>();

            renderers.Add(go.GetComponent<Renderer>());
            renderers.AddRange(go.GetComponentsInChildren<Renderer>());

            if (renderers.Count > 0)
            {
                foreach (var renderer in renderers)
                {
                    if (renderer != null)
                    {
                        var mats = renderer.materials;
                        var matsList = mats.ToList();
                        matsList.RemoveAll(material => material.name.Contains(HighlightMaterial.name));
                        renderer.materials = matsList.ToArray();
                    }
                }
            }
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    var interactable = other.GetComponent<Interactable>();

    //    if (interactable != null && interactable.Enabled)
    //    {
    //        OtherInteractable = interactable;
    //    }

    //    var carriable = other.GetComponent<Carriable>();

    //    if (carriable)
    //    {
    //        carriable.Interactor = this;
    //    }
    //}

    private void OnTriggerStay(Collider other)
    {
        var interactable = other.GetComponent<Interactable>();

        if (interactable != OtherInteractable)
        {
            if (interactable != null && interactable.Enabled)
            {
                OtherInteractable = interactable;
            }

            var carriable = other.GetComponent<Carriable>();

            if (carriable)
            {
                carriable.Interactor = this;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        OtherInteractable = null;

        var carriable = other.GetComponent<Carriable>();

        if (carriable)
        {
            carriable.Interactor = null;
        }
    }

    void Update()
    {
        if (ViveInput.GetPressDown(Hand, ControllerButton.FullTrigger))
        {
            var currentCarriedObject = Carriable.CurrentCarriedObjects[Hand];

            if (currentCarriedObject != null)
            {
                currentCarriedObject.GetComponent<Interactable>()
                    .CheckInteractionPossibleAndInteract(OwningPlayer, gameObject);
            }
            else if (OtherInteractable)
            {
                OtherInteractable.CheckInteractionPossibleAndInteract(OwningPlayer, gameObject);
                InteractableText.HideText();
            }

            //else
            //{
            //    Inventory.Instance.Drop();
            //}
        }

        if (Hand == HandRole.LeftHand)
        {
            if (ViveInput.GetPressDown(Hand, ControllerButton.Pad))
            {
                var axis = ViveInput.GetPadTouchAxis(Hand);
                if (axis.x > -0.3f && axis.x < 0.3f && axis.y > -0.3f && axis.y < 0.3f)
                {
                    if (AbilityMenu.IsOpen)
                    {
                        AbilityMenu.UseCurrentAbility();
                    }
                    else
                    {
                        AbilityMenu.Open();
                    }
                }
                else
                {
                    if (axis.x > 0)
                    {
                        //var currentCarriedObject = Carriable.CurrentCarriedObjects[Hand];

                        //if (currentCarriedObject == null)
                        //{
                        //    OwningPlayer.InitiateSpawningDecoy(this);
                        //}
                        ////else if (currentCarriedObject.GetComponent<Decoy>())
                        ////{
                        ////    currentCarriedObject.Drop();
                        ////}

                        AbilityMenu.OpenOrNextAbility();
                    }

                    if (axis.x < 0)
                    {
                        //var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();
                        //pointer.LookAtTransformForDuration(Station.GetClosestStation(OwningPlayer.transform.position).transform, 3f);
                        AbilityMenu.OpenOrPreviousAbility();
                    }
                }
            }
        }
    }
}
