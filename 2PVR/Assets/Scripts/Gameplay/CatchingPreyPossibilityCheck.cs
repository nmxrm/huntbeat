﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchingPreyPossibilityCheck : InteractablePossibilityCheck
{
    public override bool InteractionPossible(Player interactingPlayer, GameObject go)
    {
        return interactingPlayer.playerType == Player.PlayerType.Hunter;
    }
}
