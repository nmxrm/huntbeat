﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicallyResizeBoxColliderY : MonoBehaviour
{

    public BoxCollider Collider;

    public GameObject TrackedObject;

    public float Padding;

    private Bounds DefaultBounds;
    private Vector3 DefaultSize;

    void Start()
    {
        DefaultBounds = new Bounds(Collider.center, Collider.size);
        DefaultSize = Collider.size;
    }

    // Update is called once per frame
    void Update()
    {
        if (DefaultBounds.Contains(TrackedObject.transform.position))
        {
            //Collider.size
        }
    }
}
