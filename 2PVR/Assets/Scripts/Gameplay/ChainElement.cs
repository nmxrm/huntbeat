﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainElement : MonoBehaviour
{

    public static List<ChainElement> AllElements = new List<ChainElement>();

    public bool IsColliding { get; private set; }

    void Start()
    {
        AllElements.Add(this);

        //IgnorePlayerColliders();
    }

    public void SetChildrenActive(bool state)
    {
        var children = GetComponentsInChildren<Transform>(true);
        foreach (var child in children)
        {
            child.gameObject.SetActive(state);
        }

        // dont disable parent!
        gameObject.SetActive(true);
    }

    void IgnorePlayerColliders()
    {
        var preyColliders = Player.GetPlayerByType(Player.PlayerType.Prey).GetComponentsInChildren<Collider>();
        var chainColliders = GetComponentsInChildren<Collider>();

        foreach (var chainCollider in chainColliders)
        {
            foreach (var preyCollider in preyColliders)
            {
                Physics.IgnoreCollision(preyCollider, chainCollider);
            }
        }
    }

    [ContextMenu("Break chain")]
    public void BreakElement()
    {
        AllElements.Remove(this);

        GetComponent<Interactable>().Enabled = false;

        if (AllElements.Count == 0)
        {
            Debug.Log("Prey broke free");
            if (GameManagerInteractor.Instance)
            {
                GameManagerInteractor.Instance.CmdStopCatchingRoutine();
            }
        }

        var childColliders = GetComponentsInChildren<Collider>();
        foreach (var childCollider in childColliders)
        {
            childCollider.enabled = true;
        }

        var rbs = GetComponentsInChildren<Rigidbody>();

        foreach (var rb in rbs)
        {
            rb.isKinematic = false;
        }

        SoundObjectPlayer.Instance.PlaySound("chain", transform.position, false);
        Destroy(gameObject, 5f);
    }

    void OnDestroy()
    {
        if (AllElements.Contains(this))
        {
            AllElements.Remove(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IsColliding = true;
    }

    private void OnTriggerExit(Collider other)
    {
        IsColliding = false;
    }

    public static void DestroyAll()
    {
        foreach (var chainElement in AllElements)
        {
            Destroy(chainElement.gameObject);
        }
    }

    private Vector3 debugFromLine, debugToLine;

    public bool CanBeSeenByCamera(Camera playerCam)
    {
        RaycastHit chainToCamHit, camToChainHit;

        debugFromLine = transform.position;
        debugToLine = playerCam.transform.position;

        var ignorePlayer = ~LayerMask.GetMask("Interactable");

        Physics.Linecast(transform.position, playerCam.transform.position, out chainToCamHit);
        Physics.Linecast(playerCam.transform.position, transform.position, out camToChainHit, ignorePlayer);

        Player prey = null;

        if (chainToCamHit.collider != null)
        {
            prey = chainToCamHit.collider.GetComponentInParent<Player>();
        }

        ChainElement chain = null;

        if (camToChainHit.collider != null)
        {
            chain = camToChainHit.collider.GetComponent<ChainElement>();
        }

        return prey != null && prey.playerType == Player.PlayerType.Prey && chain != null;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(debugFromLine, debugToLine);
    }
}
