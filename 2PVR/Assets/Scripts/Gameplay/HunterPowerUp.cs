﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterPowerUp : MonoBehaviour
{
    public static List<HunterPowerUp> ActivePowerUps = new List<HunterPowerUp>();

    void Awake()
    {
        HunterPowerUpHandler.ActivePowerUps.Add(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        var hunter = other.GetComponent<Player>();

        if (hunter != null && hunter.playerType == Player.PlayerType.Hunter)
        {
            hunter.HunterPowerUpCollected();
            SoundObjectPlayer.Instance.PlaySound("powerup", transform.position, false);
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        HunterPowerUpHandler.ActivePowerUps.Remove(this);
    }
}
