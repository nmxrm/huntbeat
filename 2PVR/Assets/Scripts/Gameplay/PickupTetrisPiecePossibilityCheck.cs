﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupTetrisPiecePossibilityCheck : InteractablePossibilityCheck
{

    public override bool InteractionPossible(Player interactingPlayer, GameObject go)
    {
        var carriable = GetComponent<Carriable>();

        var interactor = go.GetComponent<ControllerInteractor>();

        //return Carriable.CurrentCarriedObject == null || Carriable.CurrentCarriedObject == carriable;

        // TODO!
        if (interactor)
        {
            return Carriable.CurrentCarriedObjects[interactor.Hand] == null ||
                   Carriable.CurrentCarriedObjects[interactor.Hand] == carriable;
        }

        return false;
    }
}
