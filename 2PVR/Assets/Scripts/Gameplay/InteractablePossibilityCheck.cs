﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractablePossibilityCheck : MonoBehaviour
{
    public Player LastInteractingPlayer { get; protected set; }

    public abstract bool InteractionPossible(Player interactingPlayer, GameObject go = null);

}
