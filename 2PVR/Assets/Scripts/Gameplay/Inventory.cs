﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance;

    [HideInInspector] public Key CarriedKey;

    //private Player AttachedPlayer;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //AttachedPlayer = GetComponent<Player>();
    }

    public void Pickup(Key key)
    {
        if (!GameSettings.Instance.VR)
        {
            CarriedKey = key;

            if (CarriedKey != null)
            {
                CarriedKey.GetComponent<Rigidbody>().isKinematic = true;
                CarriedKey.transform.SetParent(transform);
                CarriedKey.transform.localPosition = new Vector3(1, 1, 0);
            }
        }
    }

    public Key Drop()
    {
        CarriedKey.GetComponent<Rigidbody>().isKinematic = false;
        CarriedKey.transform.SetParent(null);

        var droppedKey = CarriedKey;

        CarriedKey = null;

        return droppedKey;
    }
}
