﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

public class Interactable : MonoBehaviour
{
    public string InfoText;

    public bool Enabled = true;

    public UnityEvent OnInteractionEvent = new UnityEvent();

    public class ObjectInteractionEvent : UnityEvent<GameObject> { };

    public ObjectInteractionEvent OnObjectInteractionEvent = new ObjectInteractionEvent();

    public InteractablePossibilityCheck PossibilityCheck { get; private set; }

    void Awake()
    {
        PossibilityCheck = GetComponent<InteractablePossibilityCheck>();
    }

    public void CheckInteractionPossibleAndInteract(Player interactingPlayer, GameObject go = null)
    {
        if (PossibilityCheck != null)
        {
            if (PossibilityCheck.InteractionPossible(interactingPlayer, go))
            {
                OnInteractionEvent.Invoke();
                OnObjectInteractionEvent.Invoke(go);
            }
        }
        else
        {
            OnInteractionEvent.Invoke();
            OnObjectInteractionEvent.Invoke(go);
        }
    }

    [ContextMenu("Invoke")]
    private void Invoke()
    {
        OnInteractionEvent.Invoke();
        OnObjectInteractionEvent.Invoke(null);
    }

    public void Deactivate()
    {
        Enabled = false;
    }
}
