﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryingKeyPossibilityCheck : InteractablePossibilityCheck
{
    public override bool InteractionPossible(Player interactingPlayer, GameObject go)
    {
        return Inventory.Instance.CarriedKey != null;
    }
}
