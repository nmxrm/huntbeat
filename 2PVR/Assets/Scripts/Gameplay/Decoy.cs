﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Decoy : NetworkBehaviour
{
    private AudioSource Audio;

    void Start()
    {
        Audio = GetComponent<AudioSource>();

        // excuse me why the fuck ...
        var collider = GetComponent<Collider>();

        foreach (var player in Player.Instances)
        {
            var playerCs = player.GetComponentsInChildren<Collider>();

            foreach (var c in playerCs)
            {
                Physics.IgnoreCollision(collider, c);
            }
        }
    }

    [ClientRpc]
    public void RpcLaunchDecoy()
    {
        GetComponent<Interactable>().Enabled = false;
        StartCoroutine(DecoyRoutine());
    }

    IEnumerator DecoyRoutine()
    {
        yield return new WaitForSeconds(3f);

        Audio.Play();

        yield return new WaitForSeconds(Audio.clip.length);

        //Destroy(gameObject);
        NetworkServer.Destroy(gameObject);
    }
}
