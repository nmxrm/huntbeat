﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using HTC.UnityPlugin.VRModuleManagement;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using Valve.VR;

public class FirstPersonControls : NetworkBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private GameObject body;

    [SerializeField] private MouseLook mouseLook;

    public float movementSpeed = 1000f;

    [SyncVar]
    public bool BlockMovement = false;

    private Rigidbody rigidBody;

    private Animator Anim;
    private NetworkAnimator NetworkAnim;

    public Player.PlayerType PType = Player.PlayerType.Prey;

    public bool IsWalking { get; private set; }

    void Start()
    {
        Anim = GetComponent<Animator>();
        NetworkAnim = GetComponent<NetworkAnimator>();

        if (isLocalPlayer)
        {
            mouseLook = new MouseLook();
            mouseLook.Init(body.transform, camera.transform);

            NetworkAnim.SetParameterAutoSend(8, true);
            NetworkAnim.SetParameterAutoSend(9, true);
        }
        else
        {
            camera.enabled = false;
        }
    }

    public void OnEnable()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    //void FixedUpdate()
    //{
    //    if (isLocalPlayer)
    //    {
    //        float movementHorizontal = Input.GetAxis("Horizontal");
    //        float movementVertical = Input.GetAxis("Vertical");

    //        Vector3 movement = new Vector3(body.transform.right * movementHorizontal, 0.0f, movementVertical);

    //        rigidBody.AddForce(movement * movementSpeed * Time.deltaTime);
    //    }
    //}

    private bool movementCanBeBlocked = false;

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {

            // look 
            mouseLook.LookRotation(body.transform, camera.transform);

            if (!BlockMovement)
            {
                movementCanBeBlocked = true;

                // movement
                Vector3 translation = Vector3.zero;

                Vector2 padPressInput = ViveInput.GetPadPressAxis(HandRole.RightHand);

                #region animation
                if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
                {
                    IsWalking = true;
                    if (PType == Player.PlayerType.Prey)
                    {
                        Anim.SetBool("Walking", IsWalking);
                    }
                    else
                    {
                        Anim.SetBool("PistolWalking", IsWalking);
                    }
                }

                if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
                {
                    if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
                    {
                        IsWalking = false;
                        if (PType == Player.PlayerType.Prey)
                        {
                            Anim.SetBool("Walking", IsWalking);
                        }
                        else
                        {
                            Anim.SetBool("PistolWalking", IsWalking);
                        }
                    }
                }
                #endregion

                if (Input.GetKey(KeyCode.W) || padPressInput.y > 0)
                {
                    translation += body.transform.forward;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    translation -= body.transform.right;
                }
                if (Input.GetKey(KeyCode.S) || padPressInput.y < 0)
                {
                    translation -= body.transform.forward;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    translation += body.transform.right;
                }

                if (rigidBody == null)
                {
                    body.transform.position += translation * (movementSpeed * Time.deltaTime);
                }
                else
                {
                    translation *= movementSpeed * Time.deltaTime;
                    translation = new Vector3(translation.x, rigidBody.velocity.y, translation.z);
                    rigidBody.velocity = translation;
                }
            }
            else if(movementCanBeBlocked)
            {
                IsWalking = false;
                if (PType == Player.PlayerType.Prey)
                {
                    Anim.SetBool("Walking", IsWalking);
                }
                else
                {
                    Anim.SetBool("PistolWalking", IsWalking);
                }

                rigidBody.velocity = Vector3.zero;
                movementCanBeBlocked = false;
            }
            //else
            //{
            //    float movementHorizontal=0;
            //    float movementVertical=0;

            //    if (Input.GetKey(KeyCode.W))
            //    {
            //        movementVertical = 1;
            //    }
            //    if (Input.GetKey(KeyCode.A))
            //    {
            //        movementHorizontal = -1;
            //    }
            //    if (Input.GetKey(KeyCode.S))
            //    {
            //        movementVertical = -1;
            //    }
            //    if (Input.GetKey(KeyCode.D))
            //    {
            //        movementHorizontal = 1;
            //    }

            //    //Vector3 movement = new Vector3(movementHorizontal, 0.0f, movementVertical);

            //    rigidBody.velocity =(movement * movementSpeed * Time.deltaTime);
            //}

        }
    }
}
