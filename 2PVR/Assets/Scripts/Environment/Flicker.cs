﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class Flicker : MonoBehaviour
{
    public float MinCooldownSeconds;
    public float MaxCooldownSeconds;

    public float MinPauseBetweenFlickers;
    public float MaxPauseBetweenFlickers;

    public int MinFlickerAmount;
    public int MaxFlickerAmount;

    private Coroutine FlickerCoroutine;

    private Light light;

    void Start()
    {
        light = GetComponent<Light>();
    }

    void OnEnable()
    {
        FlickerCoroutine = StartCoroutine(FlickerRoutine());
    }

    void OnDisable()
    {
        StopCoroutine(FlickerCoroutine);
    }

    IEnumerator FlickerRoutine()
    {
        while (true)
        {
            var cooldown = Random.Range(MinCooldownSeconds, MaxCooldownSeconds);
            var pause = Random.Range(MinPauseBetweenFlickers, MaxPauseBetweenFlickers);
            var amount = Random.Range(MinFlickerAmount, MaxFlickerAmount);

            //Debug.LogFormat("cooldown: {0}; pause: {1}; amount: {2}", cooldown, pause, amount);

            yield return new WaitForSeconds(cooldown);

            for (int i = 0; i < amount; i++)
            {
                light.enabled = false;
                yield return new WaitForSeconds(pause);
                light.enabled = true;
                yield return new WaitForSeconds(pause);
            }
        }
    }
}
