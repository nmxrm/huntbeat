﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMainMenuVrCam : MonoBehaviour
{

    public Vector3 Position;
    public Vector3 Rotation;

    void Start()
    {
        if (GameSettings.Instance.VR)
        {
            transform.position = Position;
            transform.rotation = Quaternion.Euler(Rotation);
        }
    }
}
