﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;

public class SetWalkingAnimationViaTracking : MonoBehaviour
{

    public Player ConnectedPlayer;

    private Vector3 LastPosition;
    private Quaternion LastRotation;

    // Update is called once per frame
    void Update()
    {
        SetAnimationIfPreyMovedViaTracking();
    }

    private void SetAnimationIfPreyMovedViaTracking()
    {
        if (ConnectedPlayer.playerType == Player.PlayerType.Prey)
        {
            if (!(ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Grip) ||
                  ViveInput.GetPress(HandRole.RightHand, ControllerButton.Grip)))
            {

                var distance = Vector3.Distance(LastPosition, transform.position);

                var rotation = Quaternion.Angle(LastRotation, transform.rotation);

                bool setAnim = false;

                if (distance > 0.01f)
                {
                    setAnim = true;
                }

                if (rotation > 1f)
                {
                    setAnim = true;
                }

                //Debug.Log("distance: " + distance);
                //Debug.Log("rotation: " + rotation);

                ConnectedPlayer.SetWalkingAnimationVR(setAnim);

                LastPosition = transform.position;
                LastRotation = transform.rotation;
            }
        }
    }
}
