﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyTransform : MonoBehaviour
{
    public Transform CopyPosition;
    public Transform CopyRotation;

    public bool CopyOnlyYRotation;

    public bool CopyOnlyXZPosition;

    //public float YOffset;

    void Update()
    {
        if (CopyPosition != null)
        {
            if (CopyOnlyXZPosition)
            {
                transform.position = new Vector3(CopyPosition.position.x, 0 /*+ YOffset*/, CopyPosition.position.z);
            }
            else
            {
                transform.position = new Vector3(CopyPosition.position.x, CopyPosition.position.y/* + YOffset*/, CopyPosition.position.z);
            }
        }

        if (CopyRotation != null)
        {
            if (CopyOnlyYRotation)
            {
                transform.rotation = Quaternion.Euler(0, CopyRotation.rotation.eulerAngles.y, 0);
            }
            else
            {
                transform.rotation = CopyRotation.rotation;
            }
        }
    }
}
