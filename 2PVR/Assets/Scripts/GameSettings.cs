﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class GameSettings : MonoBehaviour
{
    public static GameSettings Instance;

    public Player.PlayerType SelectedPlayerType;

    public GameObject TracePrefab;

    public bool VR;

    public StationPrefabHandler.StationSpawnBehaviour SpawnBehaviour;
    public StationPrefabHandler.StationType SpawnOnlyType;

    //public bool GameFull = false;
    public GameInfo Info;

    private bool ShowGUI = true;

    public bool GameStartedFromMainMenuScene { get; private set; }

    public bool DebugMode { get; private set; }

    private Dictionary<KeyCode, bool> KeysPushed = new Dictionary<KeyCode, bool>();

    [HideInInspector] public bool ServerDisconnected;
    [HideInInspector] public bool ClientDisconnected;

    void Awake()
    {
        if (Instance == null || Instance == this)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }

        Physics.IgnoreLayerCollision(9, 10);
        Application.runInBackground = true;

        if (VR)
        {
            LoadVR();
            ShowGUI = false;
        }

        //StartCoroutine(UnloadVrRoutine());
    }

    void Start()
    {
        Info = new GameInfo { IpAdress = HostIPBroadcaster.Instance.GetLocalIPAddress() };

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            GameStartedFromMainMenuScene = true;
        }
    }

    [ContextMenu("Load VR")]
    private void LoadVrTest()
    {
        LoadVR(true);
    }

    public void LoadVR(bool viaButton = false)
    {
        StartCoroutine(LoadVRRoutine(viaButton));
    }

    [ContextMenu("Load Non VR")]
    private void LoadNonVrTest()
    {
        LoadNonVr(true);
    }

    // loading non vr and then vr again seems to require a restart of steam vr. so dont use that i guess!
    public void LoadNonVr(bool viaButton = false)
    {
        StartCoroutine(LoadNonVrRoutine(viaButton));
    }

    private IEnumerator LoadNonVrRoutine(bool viaButton = false)
    {
        VR = false;
        XRSettings.LoadDeviceByName("None");
        yield return null;
        XRSettings.enabled = VR;

        if (viaButton)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
            var vive = GameObject.Find("[ViveInputUtility]");
            var debug = GameObject.Find("[Debug Updater]");

            if (vive)
            {
                Destroy(vive);
            }

            if (debug)
            {
                Destroy(debug);
            }
        }
    }

    public void UnloadVr()
    {
        StartCoroutine(UnloadVrRoutine());
    }

    IEnumerator UnloadVrRoutine()
    {
        XRSettings.LoadDeviceByName("None");
        yield return null;
        XRSettings.enabled = false;
    }

    IEnumerator LoadVRRoutine(bool viaButton = false)
    {
        if (!VR)
        {
            yield break;
        }

        //VR = true;
        XRSettings.LoadDeviceByName("OpenVR");
        yield return null;
        XRSettings.enabled = VR;

        if (viaButton)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                KeyPushed(KeyCode.D);
                CheckDebugTyped();
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                KeyPushed(KeyCode.E);
                CheckDebugTyped();
            }

            if (Input.GetKeyDown(KeyCode.B))
            {
                KeyPushed(KeyCode.B);
                CheckDebugTyped();
            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                KeyPushed(KeyCode.U);
                CheckDebugTyped();
            }

            if (Input.GetKeyDown(KeyCode.G))
            {
                KeyPushed(KeyCode.G);
                CheckDebugTyped();
            }

            if (ServerDisconnected)
            {
                MainMenu.Instance.ShowAlert("Server disconnected!");
                ServerDisconnected = false;
            }

            if (ClientDisconnected)
            {
                MainMenu.Instance.ShowAlert("Client disconnected!");
                ClientDisconnected = false;
            }
        }
    }

    private void CheckDebugTyped()
    {
        if (KeysPushed.ContainsKey(KeyCode.D) && KeysPushed[KeyCode.D])
        {
            if (KeysPushed.ContainsKey(KeyCode.E) && KeysPushed[KeyCode.E])
            {
                if (KeysPushed.ContainsKey(KeyCode.B) && KeysPushed[KeyCode.B])
                {
                    if (KeysPushed.ContainsKey(KeyCode.U) && KeysPushed[KeyCode.U])
                    {
                        if (KeysPushed.ContainsKey(KeyCode.G) && KeysPushed[KeyCode.G])
                        {
                            DebugMode = !DebugMode;
                            StartCoroutine(SetIgnorePlayersReadyWhenAvailable());
                            KeysPushed.Clear();
                            if (MainMenu.Instance != null)
                            {
                                MainMenu.Instance.DebugText.gameObject.SetActive(DebugMode);
                            }
                            Debug.Log("Debug mode set to: " + DebugMode);
                        }
                    }
                }
            }
        }
    }

    private IEnumerator SetIgnorePlayersReadyWhenAvailable()
    {
        while (GameManagerInteractor.Instance == null)
        {
            yield return null;
        }

        GameManagerInteractor.Instance.CmdSetIgnorePlayersReady(DebugMode);
    }

    private void KeyPushed(KeyCode key)
    {
        if (KeysPushed.ContainsKey(key))
        {
            KeysPushed[key] = true;
        }
        else
        {
            KeysPushed.Add(key, true);
        }
    }

    //void OnGUI()
    //{
    // BUGGY!!!

    //if (ShowGUI)
    //{
    //    if (GUI.Button(new Rect(new Vector2(Screen.width / 2, Screen.height / 2 - 120), new Vector2(100f, 50f)),
    //        "Launch VR"))
    //    {
    //        VR = true;
    //        DontDestroyOnLoad(gameObject);
    //        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    //        Awake();
    //    }
    //}
    //}
}
