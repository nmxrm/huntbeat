﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Workaround for unity issue:
/// Applying a material to a prefab will revert the material to the old one.
/// This alleviates the problem.
/// </summary>
public class ApplyMaterial : MonoBehaviour
{
    [SerializeField]
    private Material[] Materials;

    void Awake()
    {
        GetComponent<Renderer>().materials = Materials;
    }
}
