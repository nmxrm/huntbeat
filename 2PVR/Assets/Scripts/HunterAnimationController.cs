﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterAnimationController : MonoBehaviour
{
    private Animator Anim;

    void Awake()
    {
        Anim = GetComponent<Animator>();
    }

    void TransitionToWalking()
    {
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            Anim.SetTrigger("StartWalking");
        }
        else
        {
            Anim.SetTrigger("EndWalking");
        }
    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        TransitionToWalking();
    //    }
    //}
}
