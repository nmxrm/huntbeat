﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResetTetrisPieceOnExit : MonoBehaviour
{
    public TetrisPuzzleStation ConnectedStation;

    private void OnTriggerExit(Collider other)
    {
        if (ConnectedStation != null && !ConnectedStation.IsRunning)
        {
            return;
        }

        var player = other.GetComponent<Player>();

        if (player == null)
        {
            player = other.GetComponentInParent<Player>();
        }

        if (player != null && player.playerType == Player.PlayerType.Prey)
        {

            var carriedObjects = Carriable.CurrentCarriedObjects.Values.ToList();

            foreach (var carriedObject in carriedObjects)
            {
                if (carriedObject != null)
                {
                    var piece = carriedObject.GetComponent<TetrisPiece>();

                    if (piece != null)
                    {
                        carriedObject.Drop();
                        piece.ResetPosition();
                    }
                }
            }
        }
    }


}
