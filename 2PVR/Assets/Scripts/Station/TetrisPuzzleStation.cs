﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TetrisPuzzleStation : Station
{
    [SerializeField]
    private List<GameObject> PuzzlePrefabs = new List<GameObject>();

    private GameObject PanelGO;

    //private bool StationRunning;

    [SerializeField]
    private Transform PrefabSpawnPosition;

    void Start()
    {
        PanelGO = Instantiate(PuzzlePrefabs[Random.Range(0, PuzzlePrefabs.Count)], PrefabSpawnPosition);
        PanelGO.transform.localPosition = Vector3.zero;
        PanelGO.transform.localRotation = Quaternion.identity;

        PanelGO.GetComponentInChildren<TetrisPanel>().AssociatedStation = this;
        PanelGO.gameObject.SetActive(false);
    }

    [ContextMenu("Start station")]
    public override void StartStation()
    {
        if (StationDone)
        {
            return;
        }

        if (!IsRunning)
        {
            PanelGO.gameObject.SetActive(true);

            GetComponentInChildren<HorizontalLayoutGroup>().gameObject.SetActive(false);

            IsRunning = true;
        }
        else
        {
            PanelGO.GetComponentInChildren<TetrisPanel>().ResetAllPieces();
        }
    }

    [ContextMenu("Complete station")]
    public override void StationCompleted()
    {
        if (StationDone)
        {
            return;
        }

        var panel = PanelGO.GetComponentInChildren<TetrisPanel>();

        foreach (var piece in panel.NecessaryPieces)
        {
            Destroy(piece.gameObject);
        }

        PanelGO.gameObject.SetActive(false);

        base.StationCompleted();

        StationDone = true;

        IsRunning = false;
    }

    public override void StationFailed(bool fromCatching = false)
    {

    }
}
