﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementInTimeStation : Station
{
    [SerializeField]
    private int SecondsToComplete = 10;

    [SerializeField]
    private int SecondsGaining = 5;

    private int SecondsRemaining = 10;

    private Coroutine TimeRoutine;

    //private bool StationRunning = false;

    private Coroutine StartingStationRoutine;

    [ContextMenu("Start station")]
    public override void StartStation()
    {
        if (IsRunning || StationDone)
        {
            return;
        }

        base.StartStation();

        if (ConnectedInteractable != null)
        {
            ConnectedInteractable.Enabled = false;
        }

        //int waypointAmount = Random.Range(2, 4);
        int waypointAmount = 3;

        List<Waypoint> waypoints = new List<Waypoint>(Waypoint.Waypoints);

        var waypoint = waypoints[Random.Range(0, waypoints.Count - 1)];
        waypoint.gameObject.SetActive(true);
        waypoints.Remove(waypoint);

        CalculateAndAddAdditionalSeconds(transform.position, waypoint);

        for (int i = 0; i < waypointAmount; i++)
        {

            if (i == waypointAmount - 1)
            {
                waypoint.OnPreyEnteredWaypoint.AddListener(StationCompleted);
            }
            else
            {
                var nextWP = waypoints[Random.Range(0, waypoints.Count - 1)];
                waypoint.CurrentConnectedStation = this;
                waypoint.Successor = nextWP;
                waypoints.Remove(nextWP);
                waypoint.OnPreyEnteredWaypoint.AddListener(() =>
                {
                    nextWP.gameObject.SetActive(true);
                    //SecondsRemaining += CalculateAdditionalSeconds(waypoint.transform.position, nextWP);
                });
                waypoint = nextWP;
            }
        }

        IsRunning = true;

        StartingStationRoutine = StartCoroutine(StartStationAfterCountdown());
    }

    IEnumerator StartStationAfterCountdown()
    {
        ControllerMessageBox.Instance.ShowText("Station starts in:", 3f);

        yield return new WaitForSeconds(3f);

        yield return StartCoroutine(ControllerMessageBox.Instance.CustomCountdownRoutine("", 3f));

        var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();
        pointer.ShowWaypoint = true;
        pointer.gameObject.SetActive(true);

        TimeRoutine = StartCoroutine(DoInTimeRoutine());
    }

    [ContextMenu("Complete Station")]
    public override void StationCompleted()
    {
        if (!StationDone)
        {
            base.StationCompleted();

            Debug.Log("Movement station completed");

            StationDone = true;

            ResetStation();

            IsRunning = false;

            var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();
            pointer.ShowWaypoint = false;
            pointer.gameObject.SetActive(false);
        }
    }

    public override void StationFailed(bool fromCatching = false)
    {
        if (!StationDone)
        {
            base.StationFailed(fromCatching);

            Debug.Log("Movement station failed");

            if (TimeRoutine != null)
            {
                StopCoroutine(TimeRoutine);
            }

            if (StartingStationRoutine != null)
            {
                StopCoroutine(StartingStationRoutine);
            }

            ResetStation();

            IsRunning = false;

            if (ConnectedInteractable != null)
            {
                ConnectedInteractable.Enabled = true;
            }

            var pointer = SingletonObject.Singletons["ControllerDirectionPointer"].GetComponent<WaypointPointer>();
            pointer.ShowWaypoint = false;
            pointer.gameObject.SetActive(false);

            ControllerMessageBox.Instance.ShowText("Station failed!", 3f);
        }
    }

    private IEnumerator DoInTimeRoutine()
    {
        //SecondsRemaining = SecondsToComplete;

        while (SecondsRemaining > 0)
        {
            ControllerMessageBox.Instance.ShowText("" + SecondsRemaining, 1f);

            yield return new WaitForSeconds(1f);

            SecondsRemaining--;

            Debug.Log("Time remaining: " + SecondsRemaining);
        }

        StationFailed();
    }

    private void ResetStation()
    {
        if (TimeRoutine != null)
        {
            StopCoroutine(TimeRoutine);
        }

        foreach (var waypoint in Waypoint.Waypoints)
        {
            waypoint.OnPreyEnteredWaypoint.RemoveAllListeners();
            waypoint.gameObject.SetActive(false);
            waypoint.Successor = null;
            waypoint.CurrentConnectedStation = null;
        }
    }

    public void CalculateAndAddAdditionalSeconds(Vector3 position, Waypoint toWP)
    {
        var additionalTime = toWP.AdditionalTime;

        var wpDistance = Vector3.Distance(position, toWP.transform.position);

        var distanceInt = Mathf.CeilToInt(wpDistance);

        additionalTime += Mathf.CeilToInt(distanceInt * 1.15f);

        SecondsRemaining += additionalTime;
    }
}
