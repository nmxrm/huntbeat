﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;

public class PunchingBall : MonoBehaviour
{
    [SerializeField]
    private PunchingBallStation ConnectedStation;

    private void OnTriggerEnter(Collider other)
    {
        if (ConnectedStation.StationRunning)
        {
            var interactor = other.GetComponent<ControllerInteractor>();

            if (interactor != null)
            {
                //var localVelocity = transform.InverseTransformDirection(VivePose.GetVelocity(interactor.Hand));

                var speed = VivePose.GetVelocity(interactor.Hand).magnitude;
                //var speed = localVelocity.x;

                if (speed >= ConnectedStation.RequiredVelocity)
                {
                    ConnectedStation.StationCompleted();
                }
                else
                {
                    ConnectedStation.StationFailed();
                }

                Debug.Log("Punching ball hit with " + speed);
            }
        }
    }
}
