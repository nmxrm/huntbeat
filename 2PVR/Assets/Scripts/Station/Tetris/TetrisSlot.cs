﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisSlot : MonoBehaviour
{
    private TetrisPiece occupyingPiece;

    public Renderer SlotRenderer;

    [HideInInspector]
    public TetrisPanel OwningPanel;

    void Start()
    {
        SlotRenderer = GetComponent<Renderer>();
    }

    public TetrisPiece OccupyingPiece
    {
        get
        {
            return occupyingPiece;
        }

        set
        {
            occupyingPiece = value;
            if (OwningPanel)
            {
                OwningPanel.CheckPuzzleSolved();
            }
        }
    }

    void OnDrawGizmos()
    {
        var col = GetComponent<BoxCollider>();

        Gizmos.color = Color.blue;

        Gizmos.DrawCube(transform.TransformPoint(col.center), col.size);
    }

    //public void PlacePiece(TetrisPiece piece)
    //{
    //    if (piece.IsPlacable())
    //    {
    //        piece.transform.position = transform.position;

    //        occ
    //    }
    //}
}
