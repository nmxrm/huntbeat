﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisPiece : MonoBehaviour
{
    private List<TetrisBlock> Blocks = new List<TetrisBlock>();

    public enum Piece
    {
        I,
        S,
        Z,
        O,
        L,
        J,
        T
    }

    private List<TetrisSlot> PreviouslyPaintedSlots = new List<TetrisSlot>();

    private Quaternion Rotation;

    private bool Placed;

    private Carriable carriable;

    public float SlotCastLength = 1f;

    private Vector3 StartPosition;

    void Start()
    {
        SetupBlocks();

        carriable = GetComponent<Carriable>();

        //if (carriable)
        //{
        //    carriable.OnDropEvent.AddListener(Place);
        //}

        var interactable = GetComponent<Interactable>();

        if (interactable)
        {
            interactable.OnInteractionEvent.AddListener(DecidePlaceDetach);
        }

        Rotation = transform.rotation;

        StartPosition = transform.position;
    }

    void Update()
    {
        if (IsPlacable())
        {
            //Debug.Log("Piece is placeable");
            EnableSlotRenderer();
        }
        else
        {
            DisablePreviousSlotRenderer();
        }
    }

    void LateUpdate()
    {
        transform.rotation = Rotation;

        if (transform.parent != null && transform.parent.GetComponent<ControllerInteractor>())
        {
            transform.localPosition = Vector3.zero;
        }
    }

    private void EnableSlotRenderer()
    {
        //if (carriable != null && Carriable.CurrentCarriedObject != carriable)
        if (carriable != null && !carriable.Carried)
        {
            return;
        }

        for (int i = 0; i < Blocks.Count; i++)
        {
            var block = Blocks[i];

            if (!PreviouslyPaintedSlots.Contains(block.FoundSlot) && PreviouslyPaintedSlots.Count >= 4)
            {
                DisablePreviousSlotRenderer();
                return;
            }

            if (block.FoundSlot.OccupyingPiece == null)
            {
                block.FoundSlot.SlotRenderer.enabled = true;
                PreviouslyPaintedSlots.Add(block.FoundSlot);
            }
        }
    }

    private void DisablePreviousSlotRenderer()
    {
        if (PreviouslyPaintedSlots.Count > 0)
        {
            for (int i = 0; i < PreviouslyPaintedSlots.Count; i++)
            {
                PreviouslyPaintedSlots[i].SlotRenderer.enabled = false;
            }

            PreviouslyPaintedSlots.Clear();
        }
    }

    public void DecidePlaceDetach()
    {
        if (Placed)
        {
            Detach();
        }
        else
        {
            Place();
        }
    }

    [ContextMenu("Place")]
    public void Place()
    {
        if (IsPlacable())
        {
            for (int i = 0; i < Blocks.Count; i++)
            {
                var block = Blocks[i];
                block.FoundSlot.OccupyingPiece = this;
                block.OccupiedSlot = block.FoundSlot;
            }

            transform.position = Blocks[0].FoundSlot.transform.position;

            Placed = true;
        }
    }

    [ContextMenu("Detach")]
    public void Detach()
    {
        if (Placed)
        {
            for (int i = 0; i < Blocks.Count; i++)
            {
                var block = Blocks[i];
                if (block.OccupiedSlot != null)
                {
                    block.OccupiedSlot.OccupyingPiece = null;
                    block.OccupiedSlot = null;
                }
            }

            Placed = false;
        }
    }

    public bool IsPlacable()
    {
        foreach (var block in Blocks)
        {
            if (!(block.FoundSlot != null && block.FoundSlot.OccupyingPiece == null))
            {
                return false;
            }
        }

        return true;
    }

    void SetupBlocks()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);

            var block = child.gameObject.AddComponent<TetrisBlock>();
            block.SlotCastLength = SlotCastLength;

            Blocks.Add(block);
        }
    }

    public void ResetPosition()
    {
        transform.position = StartPosition;
    }

    //void CastSlotRay()
    //{
    //    for (int i = 0; i < transform.childCount; i++)
    //    {
    //        var child = transform.GetChild(i);

    //        var hits = Physics.RaycastAll(child.position, child.forward, 1f);

    //        for (int j = 0; j < hits.Length; j++)
    //        {
    //            var slot = hits[j].collider.GetComponent<TetrisSlot>();

    //            if (slot)
    //            {

    //            }
    //        }
    //    }
    //}
}
