﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisPanel : MonoBehaviour
{
    private List<List<TetrisSlot>> Slots = new List<List<TetrisSlot>>();

    public int PanelSizeX, PanelSizeY;

    public Vector3 SlotSize;

    public TetrisSlot SlotPrefab;

    public bool GenerateBorder = true;

    public GameObject BorderPrefab;

    public GameObject BlockPrefab;

    public SpriteRenderer BackgroundPrefab;

    public List<TetrisPiece.Piece> PiecesToCreate = new List<TetrisPiece.Piece>();

    // not necessary to set if pieces are created procedurally!
    public List<TetrisPiece> NecessaryPieces = new List<TetrisPiece>();

    public TetrisPuzzleStation AssociatedStation;

    void Start()
    {
        GeneratePanel(SlotPrefab);
    }

    public void GeneratePanel(TetrisSlot prefab = null)
    {
        for (int i = 0; i < PanelSizeY; i++)
        {
            List<TetrisSlot> row = new List<TetrisSlot>();

            for (int j = 0; j < PanelSizeX; j++)
            {
                TetrisSlot slot;

                if (prefab == null)
                {
                    slot = new GameObject("slot" + "" + i + "" + j).AddComponent<TetrisSlot>();
                    var col = slot.gameObject.AddComponent<BoxCollider>();
                    //col.size = SlotSize;
                    col.isTrigger = true;
                }
                else
                {
                    slot = Instantiate(prefab.gameObject).GetComponent<TetrisSlot>();
                    slot.GetComponent<Renderer>().enabled = false;
                }

                slot.OwningPanel = this;
                slot.transform.SetParent(transform);
                slot.transform.localScale = SlotSize;

                slot.transform.localPosition = new Vector3(j * SlotSize.x, i * -SlotSize.y, 0);

                row.Add(slot);
            }

            Slots.Add(row);
        }

        if (GenerateBorder)
        {
            var parent = new GameObject("borderparent");
            parent.transform.SetParent(transform.parent);
            parent.transform.localPosition = Vector3.zero;

            var leftBorder = Instantiate(BorderPrefab, parent.transform);
            leftBorder.transform.localRotation = transform.localRotation;
            leftBorder.transform.localPosition = transform.localPosition + Vector3.down * ((PanelSizeY - 1) * SlotSize.y / 2) + Vector3.left * SlotSize.x;
            leftBorder.transform.localScale = new Vector3(leftBorder.transform.localScale.x, (PanelSizeY + 1) * (SlotSize.y / 2), leftBorder.transform.localScale.z);

            var rightBorder = Instantiate(leftBorder, parent.transform);
            rightBorder.transform.localPosition = new Vector3(transform.localPosition.x + PanelSizeX * SlotSize.x, rightBorder.transform.localPosition.y, rightBorder.transform.localPosition.z);

            var topBorder = Instantiate(BorderPrefab, parent.transform);
            topBorder.transform.localRotation = Quaternion.Euler(topBorder.transform.localRotation.eulerAngles.x + 90f,
                topBorder.transform.localRotation.eulerAngles.y, topBorder.transform.localRotation.eulerAngles.z);
            topBorder.transform.localPosition = transform.localPosition + Vector3.right * ((PanelSizeX - 1) * SlotSize.x / 2) + Vector3.up * SlotSize.y;
            topBorder.transform.localScale = new Vector3(topBorder.transform.localScale.x, (PanelSizeX + 1) * (SlotSize.x / 2), topBorder.transform.localScale.z);

            var bottomBorder = Instantiate(topBorder, parent.transform);
            bottomBorder.transform.localPosition = new Vector3(bottomBorder.transform.localPosition.x, bottomBorder.transform.localPosition.y - (PanelSizeY + 1) * SlotSize.y, bottomBorder.transform.localPosition.z);

            parent.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }

        var bg = Instantiate(BackgroundPrefab.gameObject).GetComponent<SpriteRenderer>();
        bg.transform.parent = transform.parent;
        bg.size = new Vector2(PanelSizeX, PanelSizeY);
        bg.transform.localPosition = Vector3.zero + new Vector3(0, SlotSize.y * 0.5f, SlotSize.z * 0.5f);
        bg.transform.localScale = SlotSize;
        bg.transform.rotation = transform.rotation;
    }

    public void CheckPuzzleSolved()
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            var row = Slots[i];

            for (int j = 0; j < row.Count; j++)
            {
                if (row[j].OccupyingPiece == null)
                {
                    return;
                }
            }
        }

        Debug.Log("Puzzle solved!");

        // Disable pieces
        for (int i = 0; i < NecessaryPieces.Count; i++)
        {
            NecessaryPieces[i].GetComponent<Interactable>().Enabled = false;
        }

        if (AssociatedStation)
        {
            AssociatedStation.StationCompleted();
        }
    }

    public void ResetAllPieces()
    {
        for (int i = 0; i < NecessaryPieces.Count; i++)
        {
            var piece = NecessaryPieces[i];
            piece.Detach();
            piece.ResetPosition();
        }
    }

    //public void GeneratePanel(int rows, int columns)
    //{
    //    // bool matrix
    //    for (int i = 0; i < rows; i++)
    //    {
    //        List<bool> row = new List<bool>();

    //        for (int j = 0; j < columns; j++)
    //        {
    //            bool cell = false;

    //            row.Add(cell);
    //        }

    //        OccupiedSlots.Add(row);
    //    }
    //}

    //[ContextMenu("Placeable test")]
    //public void PlaceableTest()
    //{
    //    Debug.Log("Is piece placable? " + IsPlacable(PlaceTestX, PlaceTestY, TetrisPiece.Piece.L));
    //}

    //public bool IsPlacable(int x, int y, TetrisPiece.Piece piece)
    //{
    //    switch (piece)
    //    {
    //        case TetrisPiece.Piece.I:
    //            if (y + 3 > OccupiedSlots[0].Count)
    //            {
    //                return false;
    //            }
    //            else
    //            {
    //                if (!OccupiedSlots[y][x])
    //                {
    //                    if (!IndexIsOutOfBounds(x, y + 1) && !OccupiedSlots[y + 1][x])
    //                    {
    //                        if (!IndexIsOutOfBounds(x, y + 2) && !OccupiedSlots[y + 2][x])
    //                        {
    //                            if (!IndexIsOutOfBounds(x, y + 3) && !OccupiedSlots[y + 3][x])
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            break;
    //        case TetrisPiece.Piece.J:
    //            if (IndexIsOutOfBounds(0, y + 2) || IndexIsOutOfBounds(x - 1, 0))
    //            {
    //                return false;
    //            }
    //            else
    //            {
    //                if (!OccupiedSlots[y][x])
    //                {
    //                    if (!IndexIsOutOfBounds(x, y + 1) && !OccupiedSlots[y + 1][x])
    //                    {
    //                        if (!IndexIsOutOfBounds(x, y + 2) && !OccupiedSlots[y + 2][x])
    //                        {
    //                            if (!IndexIsOutOfBounds(x - 1, y + 2) && !OccupiedSlots[y + 2][x - 1])
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            break;
    //        case TetrisPiece.Piece.L:
    //            if (IndexIsOutOfBounds(0, y + 2) || IndexIsOutOfBounds(x + 1, 0))
    //            {
    //                return false;
    //            }
    //            else
    //            {
    //                if (!OccupiedSlots[y][x])
    //                {
    //                    if (!OccupiedSlots[y + 1][x])
    //                    {
    //                        if (!OccupiedSlots[y + 2][x])
    //                        {
    //                            if (!OccupiedSlots[y + 2][x + 1])
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            break;
    //        case TetrisPiece.Piece.O:
    //            if (IndexIsOutOfBounds(0, y + 1) || IndexIsOutOfBounds(x + 1, 0))
    //            {
    //                return false;
    //            }
    //            else
    //            {
    //                if (!OccupiedSlots[y][x])
    //                {
    //                    if (!OccupiedSlots[y + 1][x])
    //                    {
    //                        if (!OccupiedSlots[y][x + 1])
    //                        {
    //                            if (!OccupiedSlots[y + 1][x + 1])
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                    }
    //                }
    //            }

    //            break;
    //        //case:
    //        //    break;
    //        default:
    //            Debug.LogError("Unknown tetris piece!");
    //            break;

    //    }

    //    return false;
    //}

    //private bool IndexIsOutOfBounds(int x, int y)
    //{
    //    if (y >= OccupiedSlots.Count || y < 0)
    //    {
    //        return true;
    //    }

    //    if (x >= OccupiedSlots[0].Count || x < 0)
    //    {
    //        return true;
    //    }

    //    return false;
    //}

    [ContextMenu("Create pieces")]
    public void InstantiatePieces()
    {
        NecessaryPieces.Clear();

        foreach (var piece in PiecesToCreate)
        {
            GameObject pieceGO = new GameObject();
            var rb = pieceGO.AddComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.useGravity = false;
            var tPiece = pieceGO.AddComponent<TetrisPiece>();
            pieceGO.AddComponent<Interactable>();
            var c = pieceGO.AddComponent<Carriable>();
            c.KeepKinematic = true;

            GameObject block1 = Instantiate(BlockPrefab, pieceGO.transform);
            GameObject block2 = Instantiate(BlockPrefab, pieceGO.transform);
            GameObject block3 = Instantiate(BlockPrefab, pieceGO.transform);
            GameObject block4 = Instantiate(BlockPrefab, pieceGO.transform);

            block1.transform.localPosition = Vector3.zero;

            BoxCollider col = null;

            switch (piece)
            {
                case TetrisPiece.Piece.I:
                    block2.transform.localPosition = new Vector3(0, -1, 0);
                    block3.transform.localPosition = new Vector3(0, -2, 0);
                    block4.transform.localPosition = new Vector3(0, -3, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(0, -1.5f, 0);
                    col.size = new Vector3(1, 4, 1);

                    pieceGO.name = "I";
                    break;

                case TetrisPiece.Piece.J:
                    block2.transform.localPosition = new Vector3(0, -1, 0);
                    block3.transform.localPosition = new Vector3(0, -2, 0);
                    block4.transform.localPosition = new Vector3(-1, -2, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(-0.5f, -1f, 0);
                    col.size = new Vector3(2, 3, 1);

                    pieceGO.name = "J";
                    break;

                case TetrisPiece.Piece.L:
                    block2.transform.localPosition = new Vector3(0, -1, 0);
                    block3.transform.localPosition = new Vector3(0, -2, 0);
                    block4.transform.localPosition = new Vector3(1, -2, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(0.5f, -1f, 0);
                    col.size = new Vector3(2, 3, 1);

                    pieceGO.name = "L";
                    break;

                case TetrisPiece.Piece.O:
                    block2.transform.localPosition = new Vector3(1, 0, 0);
                    block3.transform.localPosition = new Vector3(0, -1, 0);
                    block4.transform.localPosition = new Vector3(1, -1, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(0.5f, -0.5f, 0);
                    col.size = new Vector3(2, 2, 1);

                    pieceGO.name = "O";
                    break;

                case TetrisPiece.Piece.S:
                    block2.transform.localPosition = new Vector3(1, 0, 0);
                    block3.transform.localPosition = new Vector3(1, 1, 0);
                    block4.transform.localPosition = new Vector3(2, 1, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(1f, 0.5f, 0);
                    col.size = new Vector3(3, 2, 1);

                    pieceGO.name = "S";
                    break;

                case TetrisPiece.Piece.T:
                    block2.transform.localPosition = new Vector3(1, 0, 0);
                    block3.transform.localPosition = new Vector3(2, 0, 0);
                    block4.transform.localPosition = new Vector3(1, 1, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(1f, 0.5f, 0);
                    col.size = new Vector3(3, 2, 1);

                    pieceGO.name = "T";
                    break;

                case TetrisPiece.Piece.Z:
                    block2.transform.localPosition = new Vector3(1, 0, 0);
                    block3.transform.localPosition = new Vector3(1, -1, 0);
                    block4.transform.localPosition = new Vector3(2, -1, 0);

                    col = pieceGO.AddComponent<BoxCollider>();
                    col.center = new Vector3(1f, -0.5f, 0);
                    col.size = new Vector3(3, 2, 1);

                    pieceGO.name = "Z";
                    break;
            }

            col.isTrigger = true;

            pieceGO.transform.localScale = SlotSize;

            NecessaryPieces.Add(tPiece);
        }
    }
}