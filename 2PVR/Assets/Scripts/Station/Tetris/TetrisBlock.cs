﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisBlock : MonoBehaviour
{
    public TetrisSlot FoundSlot;

    public TetrisSlot OccupiedSlot;

    [HideInInspector]
    public float SlotCastLength;

    void Update()
    {
        if (OccupiedSlot == null)
        {
            CastSlotRay();
        }
    }

    void CastSlotRay()
    {
        var hits = Physics.RaycastAll(transform.position, transform.forward, 1f);

        for (int j = 0; j < hits.Length; j++)
        {
            var slot = hits[j].collider.GetComponent<TetrisSlot>();

            if (slot)
            {
                FoundSlot = slot;
                return;
            }
        }

        FoundSlot = null;
    }
}
