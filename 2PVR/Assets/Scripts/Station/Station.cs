﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Valve.VR;

public class Station : NetworkBehaviour
{
    private static List<Station> ActiveStations = new List<Station>();

    [SerializeField]
    protected Key KeyPrefab;

    [SerializeField] protected Transform KeySpawnPoint;

    [SerializeField]
    protected LocalTransformAnimation KeyDoor;

    public Interactable ConnectedInteractable;

    public bool StationDone { get; protected set; }

    protected Key InstantiatedKey;
    protected Interactable KeyInteractable;
    protected Rigidbody KeyRb;

    public bool IsRunning { get; protected set; }

    protected void Awake()
    {
        if (KeyPrefab)
        {
            ActiveStations.Add(this);

            InstantiatedKey = Instantiate(KeyPrefab, KeySpawnPoint.transform.position, Quaternion.identity,
               transform);

            InstantiatedKey.transform.localRotation = Quaternion.Euler(90f, 0, 90f);

            KeyInteractable = InstantiatedKey.GetComponent<Interactable>();
            KeyRb = InstantiatedKey.GetComponent<Rigidbody>();

            KeyInteractable.Enabled = false;
            KeyRb.isKinematic = true;
        }
    }

    public virtual void StartStation()
    {
        if (StationDone)
        {
            return;
        }

        IsRunning = true;
    }

    public virtual void StationFailed(bool fromCatching = false)
    {
        if (StationDone)
        {
            return;
        }

        IsRunning = false;

        if (ControllerMessageBox.Instance && !fromCatching)
        {
            ControllerMessageBox.Instance.ShowText("Station failed!", 3f);
        }
    }

    public virtual void StationCompleted()
    {
        if (StationDone)
        {
            return;
        }

        StationDone = true;
        IsRunning = false;

        KeyDoor.OnRotationAnmiationEnded.AddListener(() =>
        {
            KeyInteractable.Enabled = true;
            //KeyRb.isKinematic = false;
        });
        KeyDoor.PlayRotationAnimation();

        GameManagerInteractor.Instance.CmdSetStationDoneEverywhere(gameObject);

        if (ControllerMessageBox.Instance)
        {
            ControllerMessageBox.Instance.ShowText("Station completed!", 3f);
        }

        GameManagerInteractor.Instance.CmdSendHunterToastMessage("The prey has completed a station!", 5f);
    }

    public void SetStationDone()
    {
        var pageView = GetComponentInChildren<AnimatedPageView>();
        if (pageView)
        {
            pageView.gameObject.SetActive(true);
            pageView.DisplayStationDone();
        }

        if (!StationDone)
        {
            KeyDoor.OnRotationAnmiationEnded.AddListener(() => Destroy(KeyInteractable.gameObject));
            KeyDoor.PlayRotationAnimation();
        }
    }

    public static Station GetClosestStation(Vector3 position, bool getOnlyUnfinished)
    {
        Station closestStation = null;
        float closestDistance = -1;

        for (int i = 0; i < ActiveStations.Count; i++)
        {
            var station = ActiveStations[i];

            var distance = Vector3.Distance(station.transform.position, position);

            if (getOnlyUnfinished)
            {
                if (!station.StationDone)
                {
                    if (closestStation == null)
                    {
                        closestStation = station;
                        closestDistance = distance;
                    }
                    else if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestStation = station;
                    }
                }
            }
            else
            {
                if (closestStation == null)
                {
                    closestStation = station;
                    closestDistance = distance;
                }
                else if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestStation = station;
                }
            }
        }

        return closestStation;
    }

    // if they aren't done yet and are running
    public static void FailAllStations(bool fromCatching)
    {
        foreach (var station in ActiveStations)
        {
            if (!(station is TetrisPuzzleStation))
            {
                if (station.IsRunning)
                {
                    station.StationFailed(fromCatching);
                }
            }
        }
    }
}