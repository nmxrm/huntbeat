﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterPowerUpSpawnPoint : MonoBehaviour
{
    private static List<HunterPowerUpSpawnPoint> AllSpawnPoints = new List<HunterPowerUpSpawnPoint>();

    private static List<HunterPowerUpSpawnPoint> UsedSpawnPoints = new List<HunterPowerUpSpawnPoint>();
    private static List<HunterPowerUpSpawnPoint> UnusedSpawnPoints = new List<HunterPowerUpSpawnPoint>();

    void Awake()
    {
        AllSpawnPoints.Add(this);
        UnusedSpawnPoints.Add(this);
    }

    //public static void ResetAll()
    //{
    //    AllSpawnPoints.Clear();
    //    UsedSpawnPoints.Clear();
    //    UnusedSpawnPoints.Clear();
    //}

    void OnDestroy()
    {
        AllSpawnPoints.Remove(this);
        UsedSpawnPoints.Remove(this);
        UnusedSpawnPoints.Remove(this);
    }

    //void Start()
    //{
    //Instantiate(StationPrefabHandler.Instance.GetRandomStationPrefab(), GetGroundPosition(), transform.rotation);
    //}

    public Vector3 GetGroundPosition()
    {
        Ray ray = new Ray(transform.position, Vector3.down);

        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            return hitInfo.point;
        }

        Debug.LogWarning("Spawn point could not find a ground position!");
        return Vector3.zero;
    }

    public static HunterPowerUpSpawnPoint GetRandomSpawnPoint()
    {
        return AllSpawnPoints[Random.Range(0, AllSpawnPoints.Count)];
    }

    public static HunterPowerUpSpawnPoint GetRandomUnusedSpawnPoint()
    {
        int i = Random.Range(0, UnusedSpawnPoints.Count);
        var point = UnusedSpawnPoints[i];
        UsedSpawnPoints.Add(point);
        UnusedSpawnPoints.RemoveAt(i);

        return point;
    }

    public static HunterPowerUpSpawnPoint[] GetCopyOfSpawnPoints()
    {
        HunterPowerUpSpawnPoint[] allPoints = new HunterPowerUpSpawnPoint[AllSpawnPoints.Count];
        AllSpawnPoints.CopyTo(allPoints);
        return allPoints;
    }

    public static void ResetUsedUnusedSpawnPoints()
    {
        UsedSpawnPoints.Clear();
        UnusedSpawnPoints.Clear();

        UnusedSpawnPoints.AddRange(AllSpawnPoints);
    }
}
