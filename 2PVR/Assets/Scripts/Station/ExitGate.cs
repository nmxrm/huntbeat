﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGate : MonoBehaviour
{
    public static ExitGate Instance { get; private set; }

    public bool BlueKeySubmitted { get; private set; }
    public bool RedKeySubmitted { get; private set; }
    public bool GreenKeySubmitted { get; private set; }

    [SerializeField]
    private LocalTransformAnimation BlueBar;
    [SerializeField]
    private LocalTransformAnimation RedBar;
    [SerializeField]
    private LocalTransformAnimation GreenBar;

    [SerializeField]
    private LocalTransformAnimation LockHole;

    [SerializeField]
    private LocalTransformAnimation RightDoor;

    [SerializeField]
    private LocalTransformAnimation LeftDoor;

    [SerializeField] private Animator TopCog;
    [SerializeField] private Animator MidCog;
    [SerializeField] private Animator BottomCog;

    private Key KeyGo;

    private bool KeyAnimationRunning;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        if (LockHole)
        {
            LockHole.OnRotationAnmiationEnded.AddListener(() => StartCoroutine(ResetKeyAnimation()));
        }

        //if (BlueBar)
        //{
        //    BlueBar.OnPositionAnimationEnded.AddListener(PlayDoorAnimationsOnAllKeysSubmitted);
        //    SetColor(BlueBar.gameObject, Key.KeyType.Blue);
        //}

        //if (RedBar)
        //{
        //    RedBar.OnPositionAnimationEnded.AddListener(PlayDoorAnimationsOnAllKeysSubmitted);
        //    SetColor(RedBar.gameObject, Key.KeyType.Red);
        //}

        //if (GreenBar)
        //{
        //    GreenBar.OnPositionAnimationEnded.AddListener(PlayDoorAnimationsOnAllKeysSubmitted);
        //    SetColor(GreenBar.gameObject, Key.KeyType.Green);
        //}
    }

    private void SetColor(GameObject go, Key.KeyType color)
    {
        var renderer = go.GetComponent<Renderer>();
        MaterialPropertyBlock block = new MaterialPropertyBlock();
        renderer.GetPropertyBlock(block);

        switch (color)
        {
            case Key.KeyType.Blue:
                block.SetColor("_Color", Color.blue);
                break;
            case Key.KeyType.Green:
                block.SetColor("_Color", Color.green);
                break;
            case Key.KeyType.Red:
                block.SetColor("_Color", Color.red);
                break;
        }

        renderer.SetPropertyBlock(block);
    }

    public void SubmitCarriedKey()
    {
        if (Inventory.Instance.CarriedKey != null)
        {
            SubmitKey(Inventory.Instance.CarriedKey.KeyColor);
            var key = Inventory.Instance.Drop();
            Destroy(key.gameObject);
        }
    }

    public void SetBarAnimation(Key.KeyType color)
    {
        switch (color)
        {
            case Key.KeyType.Blue:
                if (!BlueKeySubmitted)
                {
                    BlueBar.PlayPositionAnimation();
                    TopCog.enabled = true;
                }
                break;

            case Key.KeyType.Green:
                if (!GreenKeySubmitted)
                {
                    GreenBar.PlayPositionAnimation();
                    BottomCog.enabled = true;
                }
                break;

            case Key.KeyType.Red:
                if (!RedKeySubmitted)
                {
                    RedBar.PlayPositionAnimation();
                    MidCog.enabled = true;
                }
                break;
        }
    }

    [ContextMenu("debug submit green")]
    private void DebugSubmitGreen()
    {
        SubmitKey(Key.KeyType.Green);
    }

    [ContextMenu("debug submit red")]
    private void DebugSubmitRed()
    {
        SubmitKey(Key.KeyType.Red);
    }

    [ContextMenu("debug submit blue")]
    private void DebugSubmitBlue()
    {
        SubmitKey(Key.KeyType.Blue);
    }

    public void SubmitKey(Key.KeyType keyColor)
    {
        switch (keyColor)
        {
            case Key.KeyType.Blue:
                BlueKeySubmitted = true;
                LockHole.OnRotationAnmiationEnded.AddListener(() =>
                {
                    GameManagerInteractor.Instance.CmdSetBarAnimationEverywhere((int)keyColor);
                    BlueBar.PlayPositionAnimation();
                    TopCog.enabled = true;
                    SoundObjectPlayer.Instance.PlaySound("cogs", TopCog.transform.position, true);
                });
                break;
            case Key.KeyType.Red:
                RedKeySubmitted = true;
                LockHole.OnRotationAnmiationEnded.AddListener(() =>
                {
                    GameManagerInteractor.Instance.CmdSetBarAnimationEverywhere((int)keyColor);
                    RedBar.PlayPositionAnimation();
                    MidCog.enabled = true;
                    SoundObjectPlayer.Instance.PlaySound("cogs", MidCog.transform.position, true);
                });
                break;
            case Key.KeyType.Green:
                GreenKeySubmitted = true;
                LockHole.OnRotationAnmiationEnded.AddListener(() =>
                {
                    GameManagerInteractor.Instance.CmdSetBarAnimationEverywhere((int)keyColor);
                    GreenBar.PlayPositionAnimation();
                    BottomCog.enabled = true;
                    SoundObjectPlayer.Instance.PlaySound("cogs", BottomCog.transform.position, true);
                });
                break;
        }

        Debug.Log("Key submitted");
        GameManagerInteractor.Instance.CmdSendHunterToastMessage("The prey has submitted a key!", 5f);

        // Game ends
        if (GreenKeySubmitted && BlueKeySubmitted && RedKeySubmitted)
        {
            //GameManagerInteractor.Instance.RpcPrintGmMessageOnClients("Game ended, Prey wins!");
            GameManagerInteractor.Instance.CmdAllKeysSubmitted();
        }
    }

    // for testing only!
    [ContextMenu("End game")]
    private void DebugEndGame()
    {
        GameManagerInteractor.Instance.CmdAllKeysSubmitted();
    }

    private void OnTriggerEnter(Collider other)
    {
        var key = other.GetComponent<Key>();

        if (key && !KeyAnimationRunning)
        {
            KeyAnimationRunning = true;
            SubmitKey(key.KeyColor);
            var carriable = key.GetComponent<Carriable>();
            if (carriable.Carried)
            {
                key.GetComponent<Carriable>().Drop();
            }

            if (LockHole)
            {
                KeyGo = key;
                SetupKeyAnimation();
            }
            else
            {
                Destroy(key.gameObject);
            }
        }
    }

    private void SetupKeyAnimation()
    {
        KeyGo.GetComponent<Rigidbody>().isKinematic = true;
        KeyGo.transform.SetParent(LockHole.transform);
        KeyGo.transform.localPosition = Vector3.zero;
        KeyGo.transform.localRotation = Quaternion.identity;

        KeyGo.GetComponent<Interactable>().Enabled = false;

        LockHole.PlayRotationAnimation();
    }

    private IEnumerator ResetKeyAnimation()
    {
        yield return new WaitForSeconds(1f);

        ControllerMessageBox.Instance.ShowText("Door unlocked!", 5f);

        Destroy(KeyGo.gameObject);

        LockHole.ResetRotation();

        KeyGo = null;

        LockHole.OnRotationAnmiationEnded.RemoveAllListeners();
        LockHole.OnRotationAnmiationEnded.AddListener(() => StartCoroutine(ResetKeyAnimation()));

        KeyAnimationRunning = false;
    }

    public List<Key.KeyType> GetMissingKeyTypes()
    {
        List<Key.KeyType> missingTypes = new List<Key.KeyType>();

        if (!BlueKeySubmitted)
        {
            missingTypes.Add(Key.KeyType.Blue);
        }

        if (!RedKeySubmitted)
        {
            missingTypes.Add(Key.KeyType.Red);
        }

        if (!GreenKeySubmitted)
        {
            missingTypes.Add(Key.KeyType.Green);
        }

        return missingTypes;
    }

    public void PlayDoorAnimationsOnAllKeysSubmitted()
    {
        //if (GreenKeySubmitted && BlueKeySubmitted && RedKeySubmitted)
        //{
        RightDoor.PlayRotationAnimation();
        LeftDoor.PlayRotationAnimation();
        //}
    }
}
