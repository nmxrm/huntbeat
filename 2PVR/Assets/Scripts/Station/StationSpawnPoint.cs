﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationSpawnPoint : MonoBehaviour
{
    private static List<StationSpawnPoint> AllSpawnPoints = new List<StationSpawnPoint>();

    private static List<StationSpawnPoint> UsedSpawnPoints = new List<StationSpawnPoint>();
    private static List<StationSpawnPoint> UnusedSpawnPoints = new List<StationSpawnPoint>();

    void Awake()
    {
        AllSpawnPoints.Add(this);
        UnusedSpawnPoints.Add(this);
    }

    //void Start()
    //{
        //Instantiate(StationPrefabHandler.Instance.GetRandomStationPrefab(), GetGroundPosition(), transform.rotation);
    //}

    void OnDestroy()
    {
        AllSpawnPoints.Remove(this);
        UsedSpawnPoints.Remove(this);
        UnusedSpawnPoints.Remove(this);
    }

    public Vector3 GetGroundPosition()
    {
        Ray ray = new Ray(transform.position, Vector3.down);

        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo))
        {
            return hitInfo.point;
        }

        Debug.LogWarning("Spawn point could not find a ground position!");
        return Vector3.zero;
    }

    public static StationSpawnPoint GetRandomSpawnPoint()
    {
        return AllSpawnPoints[Random.Range(0, AllSpawnPoints.Count)];
    }

    public static StationSpawnPoint GetRandomUnusedSpawnPoint()
    {
        int i = Random.Range(0, UnusedSpawnPoints.Count);
        var point = UnusedSpawnPoints[i];
        UsedSpawnPoints.Add(point);
        UnusedSpawnPoints.RemoveAt(i);

        return point;
    }

    public static StationSpawnPoint[] GetCopyOfSpawnPoints()
    {
        StationSpawnPoint[] allPoints = new StationSpawnPoint[AllSpawnPoints.Count];
        AllSpawnPoints.CopyTo(allPoints);
        return allPoints;
    }
}
