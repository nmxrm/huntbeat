﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.Events;

public class Destructible : MonoBehaviour
{
    public UnityEvent OnControllerEnteredEvent = new UnityEvent();

    [HideInInspector]
    public DestroyInTimeStation ConnectedStation;

    public bool UseVelocity;
    public float RequiredSpeed = 1f;

    private AudioSource AudioClip;

    private LocalTransformAnimation TransformAnimation;

    private int OnEnableCounter = 0;

    void Start()
    {
        TransformAnimation = GetComponent<LocalTransformAnimation>();
        AudioClip = GetComponent<AudioSource>();

        OnControllerEnteredEvent.AddListener(() =>
        {
            if (SoundObjectPlayer.Instance)
            {
                SoundObjectPlayer.Instance.PlaySound("balloonpop", transform.position, false);
            }
        });

        TransformAnimation.OnScaleAnmiationEnded.AddListener(OnControllerEnteredEvent.Invoke);
    }

    void OnEnable()
    {
        OnEnableCounter++;

        if (OnEnableCounter > 1)
        {
            SoundObjectPlayer.Instance.PlaySound("balloonstart", transform.position, false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var interactor = other.GetComponent<ControllerInteractor>();

        if (interactor != null)
        {
            if (UseVelocity)
            {
                var speed = VivePose.GetVelocity(interactor.Hand).magnitude;

                if (speed >= RequiredSpeed)
                {
                    TransformAnimation.PlayScaleAnimation();
                    //OnControllerEnteredEvent.Invoke();
                }
            }
            else
            {
                TransformAnimation.PlayScaleAnimation();
                //OnControllerEnteredEvent.Invoke();
            }
        }
    }

    [ContextMenu("Invoke scale animation")]
    private void DEBUG_InvokeAnimation()
    {
        TransformAnimation.PlayScaleAnimation();
    }

    [ContextMenu("Invoke")]
    public void DEBUG_Invoke()
    {
        OnControllerEnteredEvent.Invoke();
    }

    public bool CanBeSeenByCamera(Camera playerCam)
    {
        RaycastHit hit;

        //debugFromLine = transform.position;
        //debugToLine = playerCam.transform.position;

        Physics.Linecast(transform.position, playerCam.transform.position, out hit);

        Player prey = null;

        if (hit.collider != null)
        {
            prey = hit.collider.GetComponentInParent<Player>();
        }

        return prey != null && prey.playerType == Player.PlayerType.Prey;
    }

    void OnDestroy()
    {
        if (ConnectedStation != null)
        {
            ConnectedStation.RemoveDestructible(this);

        }
    }
}
