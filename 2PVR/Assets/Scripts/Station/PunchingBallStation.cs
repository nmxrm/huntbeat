﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchingBallStation : Station
{
    public bool StationRunning { get; private set; }

    public float RequiredVelocity = 5f;

    public override void StartStation()
    {
        if (StationDone || StationRunning)
        {
            return;
        }

        StationRunning = true;
    }

    public override void StationCompleted()
    {
        if (StationDone)
        {
            return;
        }

        StationRunning = false;
    }

    public override void StationFailed(bool fromCatching = false)
    {
        if (StationDone)
        {
            return;
        }

        StationRunning = false;
    }
}
