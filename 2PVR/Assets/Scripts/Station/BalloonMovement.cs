﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonMovement : MonoBehaviour
{
    public AnimationCurve Curve;

    private Vector3 DefaultPosition;

    public float duration;
    private Vector3 MaxPosition, MinPosition;
    public Vector3 MaxPositionRelative, MinPositionRelative;

    public bool MoveOnStart;

    private Coroutine MovementRoutine;

    void Start()
    {
        if (MoveOnStart)
        {
            StartMovement();
        }
    }

    [ContextMenu("Start movement")]
    public void StartMovement()
    {
        DefaultPosition = transform.position;

        MaxPosition = DefaultPosition + MaxPositionRelative;
        MinPosition = DefaultPosition + MinPositionRelative;

        MovementRoutine = StartCoroutine(BalloonAnimationRoutine(MaxPosition));
    }

    public void StopMovement()
    {
        StopCoroutine(MovementRoutine);
    }

    IEnumerator BalloonAnimationRoutine(Vector3 endPosition)
    {
        float rate = 1 / duration;

        while (true)
        {
            float i = 0;
            var startPosition = transform.position;

            while (i < 1)
            {
                transform.position = Vector3.Lerp(startPosition, endPosition, Curve.Evaluate(i));
                i += Time.deltaTime * rate;
                yield return null;
            }

            transform.position = endPosition;

            if (endPosition == MaxPosition)
            {
                endPosition = MinPosition;
            }
            else if (endPosition == MinPosition)
            {
                endPosition = MaxPosition;
            }

            yield return null;
        }
    }
}