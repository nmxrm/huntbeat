﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class DestroyInTimeStation : Station
{
    public GameObject DestroyableObjectPrefab;

    public float Phase1Time;
    public float Phase2Time;
    public float Phase3Time;

    //private bool LaunchDestruction = false;

    private float SecondsRemaining;

    //private Coroutine Phase1TimeoutRoutine;
    //private Coroutine Phase2TimeoutRoutine;
    //private Coroutine Phase3TimeoutRoutine;

    //private Coroutine Phase1Coroutine;
    //private Coroutine Phase2Coroutine;
    //private Coroutine Phase3Coroutine;

    private Coroutine PlayerLeftRoutine;

    private List<Destructible> CurrentDestructibles = new List<Destructible>();

    //private bool StationRunning;

    private Player InteractingPlayer;

    private Destructible CurrentlyCreatedDestructible;

    //private Dictionary<string, Coroutine> DestructibleRoutines = new Dictionary<string, Coroutine>();

    private List<Coroutine> TimeRoutines = new List<Coroutine>();

    [ContextMenu("Start station")]
    public override void StartStation()
    {
        if (IsRunning || StationDone)
        {
            return;
        }

        base.StartStation();

        InteractingPlayer = ConnectedInteractable.PossibilityCheck.LastInteractingPlayer;

        IsRunning = true;

        AllPhasesRoutine = StartCoroutine(AllPhasesCompleteRoutine());
        PlayerLeftRoutine = StartCoroutine(PlayerWithinAreaRoutine());
    }

    [ContextMenu("DEBUG Start station")]
    private void DEBUGStartStation()
    {
        if (IsRunning || StationDone)
        {
            return;
        }

        base.StartStation();

        InteractingPlayer = Player.GetPlayerByType(Player.PlayerType.Prey);

        IsRunning = true;

        AllPhasesRoutine = StartCoroutine(AllPhasesCompleteRoutine());
        PlayerLeftRoutine = StartCoroutine(PlayerWithinAreaRoutine());
    }

    public override void StationFailed(bool fromCatching = false)
    {
        base.StationFailed(fromCatching);

        if (PlayerLeftRoutine != null)
        {
            StopCoroutine(PlayerLeftRoutine);
        }

        //if (Phase1Coroutine != null)
        //{
        //    StopCoroutine(Phase1Coroutine);
        //}
        //if (Phase2Coroutine != null)
        //{
        //    StopCoroutine(Phase2Coroutine);
        //}
        //if (Phase3Coroutine != null)
        //{
        //    StopCoroutine(Phase3Coroutine);
        //}

        //if (Phase1TimeoutRoutine != null)
        //{
        //    StopCoroutine(Phase1TimeoutRoutine);
        //}
        //if (Phase2TimeoutRoutine != null)
        //{
        //    StopCoroutine(Phase2TimeoutRoutine);
        //}
        //if (Phase3TimeoutRoutine != null)
        //{
        //    StopCoroutine(Phase3TimeoutRoutine);
        //}

        //var routines = DestructibleRoutines.Values.ToList();
        //foreach (var coroutine in routines)
        //{
        //    StopCoroutine(coroutine);
        //}
        StopAllCoroutines();

        StopCoroutine(AllPhasesRoutine);


        //StationRunning = false;
        IsRunning = false;

        Debug.Log("Station failed!");

        StartCoroutine(DestroyAllDestructibles());
    }

    IEnumerator DestroyAllDestructibles()
    {
        while (CurrentDestructibles.Count > 0)
        {
            var destructible = CurrentDestructibles[0];
            if (destructible != null)
            {
                Destroy(destructible.gameObject);
            }
            yield return null;
        }

        CurrentDestructibles.Clear();
    }

    [ContextMenu("Complete station")]
    public override void StationCompleted()
    {
        base.StationCompleted();

        if (PlayerLeftRoutine != null)
        {
            StopCoroutine(PlayerLeftRoutine);
        }

        //StationRunning = false;
        IsRunning = false;
        StationDone = true;
    }

    //IEnumerator Phase1Routine()
    //{
    //    ControllerMessageBox.Instance.ShowText("Station started!", 3f, false, true);
    //    Debug.Log("Station started!");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Phase 1", 3f, false, true);
    //    Debug.Log("Phase 1!");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Starts in:", 3f, false, true);
    //    Debug.Log("Starts in: ");

    //    yield return new WaitForSeconds(3f);

    //    yield return StartCoroutine(TimeoutRoutine(3f, false));

    //    yield return new WaitForSeconds(0.5f);

    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d1 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d2 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d3 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d4 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d5 = CurrentlyCreatedDestructible;

    //    d1.gameObject.SetActive(true);

    //    //d2.gameObject.SetActive(false);
    //    //d3.gameObject.SetActive(false);
    //    //d4.gameObject.SetActive(false);
    //    //d5.gameObject.SetActive(false);

    //    d1.ConnectedStation = this;
    //    d2.ConnectedStation = this;
    //    d3.ConnectedStation = this;
    //    d4.ConnectedStation = this;
    //    d5.ConnectedStation = this;

    //    CurrentDestructibles.AddRange(new[] { d1, d2, d3, d4, d5 });

    //    d1.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        d2.gameObject.SetActive(true);
    //        Destroy(d1.gameObject);
    //    });

    //    d2.OnControllerEnteredEvent.AddListener(() =>
    //          {
    //              d3.gameObject.SetActive(true);
    //              Destroy(d2.gameObject);
    //          });

    //    d3.OnControllerEnteredEvent.AddListener(() =>
    //          {
    //              d4.gameObject.SetActive(true);
    //              Destroy(d3.gameObject);
    //          });

    //    d4.OnControllerEnteredEvent.AddListener(() =>
    //          {
    //              d5.gameObject.SetActive(true);
    //              Destroy(d4.gameObject);
    //          });

    //    d5.OnControllerEnteredEvent.AddListener(() =>
    //          {
    //              Phase1Finished();
    //              Destroy(d5.gameObject);
    //          });

    //    Phase1TimeoutRoutine = StartCoroutine(TimeoutRoutine(Phase1Time, true, true));
    //}

    //IEnumerator Phase2Routine()
    //{
    //    //yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Good job!", 3f, false, true);
    //    Debug.Log("Good job!");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Phase 2", 3f, false, true);
    //    Debug.Log("Phase 2");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Starts in:", 3f, false, true);
    //    Debug.Log("Starts in:");

    //    yield return new WaitForSeconds(3f);

    //    yield return StartCoroutine(TimeoutRoutine(3f, false));

    //    yield return new WaitForSeconds(0.5f);

    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d1 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d2 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d3 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d4 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible());
    //    var d5 = CurrentlyCreatedDestructible;

    //    d1.ConnectedStation = this;
    //    d2.ConnectedStation = this;
    //    d3.ConnectedStation = this;
    //    d4.ConnectedStation = this;
    //    d5.ConnectedStation = this;

    //    d1.gameObject.SetActive(true);
    //    d2.gameObject.SetActive(true);

    //    //d3.gameObject.SetActive(false);
    //    //d4.gameObject.SetActive(false);
    //    //d5.gameObject.SetActive(false);

    //    CurrentDestructibles.AddRange(new[] { d1, d2, d3, d4, d5 });

    //    List<Destructible> actives12 = new List<Destructible>(new[] { d1, d2 });
    //    List<Destructible> actives34 = new List<Destructible>(new[] { d3, d4 });

    //    d1.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        actives12.Remove(d1);
    //        Destroy(d1.gameObject);
    //    });

    //    d2.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        actives12.Remove(d2);
    //        Destroy(d2.gameObject);
    //    });

    //    d3.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        actives34.Remove(d3);
    //        Destroy(d3.gameObject);
    //    });

    //    d4.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        actives34.Remove(d4);
    //        Destroy(d4.gameObject);
    //    });

    //    d5.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Phase2Finished();
    //        Destroy(d5.gameObject);
    //    });

    //    DestructibleRoutines.Add("a", StartCoroutine(DestructibleListDestroyedRoutine("a", actives12, () =>
    //      {
    //          d3.gameObject.SetActive(true);
    //          d4.gameObject.SetActive(true);
    //      })));
    //    DestructibleRoutines.Add("b", StartCoroutine(DestructibleListDestroyedRoutine("b", actives34, () => d5.gameObject.SetActive(true))));

    //    Phase2TimeoutRoutine = StartCoroutine(TimeoutRoutine(Phase2Time, true, true));
    //}

    //IEnumerator Phase3Routine()
    //{
    //    ControllerMessageBox.Instance.ShowText("Good job!", 3f, false, true);
    //    Debug.Log("Good job!");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Phase 3", 3f, false, true);
    //    Debug.Log("Phase 3");

    //    yield return new WaitForSeconds(3f);

    //    ControllerMessageBox.Instance.ShowText("Starts in:", 3f, false, true);
    //    Debug.Log("Starts in:");

    //    yield return new WaitForSeconds(3f);

    //    yield return StartCoroutine(TimeoutRoutine(3f, false));

    //    yield return new WaitForSeconds(0.5f);

    //    yield return StartCoroutine(InstantiateDestructible(0, 0.5f));
    //    var d1 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible(0, 0.5f));
    //    var d2 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible(0, 0.5f));
    //    var d3 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible(0, 0.5f));
    //    var d4 = CurrentlyCreatedDestructible;
    //    yield return StartCoroutine(InstantiateDestructible(0, 0.5f));
    //    var d5 = CurrentlyCreatedDestructible;

    //    d1.ConnectedStation = this;
    //    d2.ConnectedStation = this;
    //    d3.ConnectedStation = this;
    //    d4.ConnectedStation = this;
    //    d5.ConnectedStation = this;

    //    d1.gameObject.SetActive(true);
    //    d2.gameObject.SetActive(true);
    //    d3.gameObject.SetActive(true);
    //    d4.gameObject.SetActive(true);
    //    d5.gameObject.SetActive(true);

    //    CurrentDestructibles.AddRange(new[] { d1, d2, d3, d4, d5 });

    //    d1.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Destroy(d1.gameObject);
    //    });
    //    d2.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Destroy(d2.gameObject);
    //    });
    //    d3.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Destroy(d3.gameObject);
    //    });
    //    d4.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Destroy(d4.gameObject);
    //    });
    //    d5.OnControllerEnteredEvent.AddListener(() =>
    //    {
    //        Destroy(d5.gameObject);
    //    });

    //    DestructibleRoutines.Add("c", StartCoroutine(DestructibleListDestroyedRoutine("c", CurrentDestructibles, Phase3Finished)));

    //    Phase3TimeoutRoutine = StartCoroutine(TimeoutRoutine(Phase3Time, true, true));
    //}

    //public void Phase1Finished()
    //{
    //    StopCoroutine(Phase1TimeoutRoutine);

    //    StopCoroutine(Phase1Coroutine);

    //    Phase2Coroutine = StartCoroutine(Phase2Routine());
    //}

    //public void Phase2Finished()
    //{
    //    StopCoroutine(Phase2TimeoutRoutine);

    //    StopCoroutine(Phase2Coroutine);

    //    Phase3Coroutine = StartCoroutine(Phase3Routine());
    //}

    //public void Phase3Finished()
    //{
    //    StopCoroutine(Phase3TimeoutRoutine);

    //    StopCoroutine(Phase3Coroutine);

    //    StationCompleted();
    //}

    private IEnumerator TimeoutRoutine(float time, bool failOnFinish = true, bool showTimeRemaining = false)
    {
        SecondsRemaining = time;

        while (SecondsRemaining > 0)
        {
            if (ControllerMessageBox.Instance != null)
            {
                ControllerMessageBox.Instance.ShowText(showTimeRemaining ? "Time remaining: " + SecondsRemaining : "" + SecondsRemaining, 1f, false, true);
            }
            Debug.Log("Time remaining: " + SecondsRemaining);

            SecondsRemaining--;

            yield return new WaitForSeconds(1f);
        }

        //ControllerMessageBox.Instance.ShowText(showTimeRemaining ? "Time remaining: " + SecondsRemaining : "" + SecondsRemaining, 1f);

        if (failOnFinish)
        {
            StationFailed();
        }
    }

    IEnumerator PlayerWithinAreaRoutine()
    {
        var player = InteractingPlayer.ActiveCam.transform;

        while (Vector3.Distance(transform.position, player.position) <= 4f)
        {
            yield return new WaitForSeconds(1f);
        }

        PlayerLeftRoutine = null;
        StationFailed();
    }

    //private IEnumerator DestructibleListDestroyedRoutine(string id, List<Destructible> activeDestructibles, Action onAllDestroyed)
    //{
    //    while (activeDestructibles.Count > 0)
    //    {
    //        yield return null;
    //    }

    //    DestructibleRoutines.Remove(id);
    //    onAllDestroyed.Invoke();
    //}

    private IEnumerator InstantiateDestructible(float offsetX = 0f, float offsetY = 0f, float offsetZ = 0f)
    {
        Vector3 offset = new Vector3(offsetX, offsetY, offsetZ);

        Camera cam = InteractingPlayer.ActiveCam;

        CurrentlyCreatedDestructible = Instantiate(DestroyableObjectPrefab, cam.transform.position + offset + Random.onUnitSphere * 0.75f, Quaternion.identity).GetComponent<Destructible>();
        CurrentlyCreatedDestructible.gameObject.SetActive(false);

        while (!CurrentlyCreatedDestructible.CanBeSeenByCamera(cam))
        {
            CurrentlyCreatedDestructible.transform.position = cam.transform.position + offset + Random.onUnitSphere * 0.75f;

            yield return null;
        }
    }

    private Coroutine AllPhasesRoutine;

    public void RemoveDestructible(Destructible d)
    {
        CurrentDestructibles.Remove(d);
    }

    // debug
    [ContextMenu("Start complete routine")]
    private void StartCompleteRoutine()
    {
        AllPhasesRoutine = StartCoroutine(AllPhasesCompleteRoutine());
    }

    private IEnumerator AllPhasesCompleteRoutine()
    {
        yield return StartCoroutine(InstantiateDestructible(0, -0.1f));
        var d11 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, -0.1f));
        var d12 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, -0.1f));
        var d13 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, -0.1f));
        var d14 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, -0.1f));
        var d15 = CurrentlyCreatedDestructible;

        yield return StartCoroutine(InstantiateDestructible(0, 0.1f));
        var d21 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.1f));
        var d22 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.1f));
        var d23 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.1f));
        var d24 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.1f));
        var d25 = CurrentlyCreatedDestructible;

        yield return StartCoroutine(InstantiateDestructible(0, 0.175f));
        var d31 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.175f));
        var d32 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.175f));
        var d33 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.175f));
        var d34 = CurrentlyCreatedDestructible;
        yield return StartCoroutine(InstantiateDestructible(0, 0.175f));
        var d35 = CurrentlyCreatedDestructible;

        CurrentDestructibles = new List<Destructible>
            {d11, d12, d13, d14, d15, d21, d22, d23, d24, d25, d31, d32, d33, d34, d35};

        d11.ConnectedStation = this;
        d12.ConnectedStation = this;
        d13.ConnectedStation = this;
        d14.ConnectedStation = this;
        d15.ConnectedStation = this;
        d21.ConnectedStation = this;
        d22.ConnectedStation = this;
        d23.ConnectedStation = this;
        d24.ConnectedStation = this;
        d25.ConnectedStation = this;
        d31.ConnectedStation = this;
        d32.ConnectedStation = this;
        d33.ConnectedStation = this;
        d34.ConnectedStation = this;
        d35.ConnectedStation = this;

        d11.OnControllerEnteredEvent.AddListener(() =>
        {
            d12.gameObject.SetActive(true);
            Destroy(d11.gameObject);
        });
        d12.OnControllerEnteredEvent.AddListener(() =>
        {
            d13.gameObject.SetActive(true);
            Destroy(d12.gameObject);
        });
        d13.OnControllerEnteredEvent.AddListener(() =>
        {
            d14.gameObject.SetActive(true);
            Destroy(d13.gameObject);
        });
        d14.OnControllerEnteredEvent.AddListener(() =>
        {
            d15.gameObject.SetActive(true);
            Destroy(d14.gameObject);
        });
        d15.OnControllerEnteredEvent.AddListener(() =>
        {
            Destroy(d15.gameObject);
        });

        Debug.Log("Station started!");
        ControllerMessageBox.Instance.ShowText("Station started!", 3f, false, true);
        yield return new WaitForSeconds(3f);
        ControllerMessageBox.Instance.ShowText("Phase 1", 3f, false, true);
        Debug.Log("Phase 1");
        yield return new WaitForSeconds(3f);
        ControllerMessageBox.Instance.ShowText("starts in:", 3f, false, true);
        Debug.Log("starts in:");
        yield return new WaitForSeconds(3f);
        Coroutine timeRoutine1 = StartCoroutine(TimeoutRoutine(3f, false));
        TimeRoutines.Add(timeRoutine1);
        yield return timeRoutine1;

        var timeoutText = StartCoroutine(TimeoutRoutine(Phase1Time, false, true));
        TimeRoutines.Add(timeoutText);

        var timeout = StartCoroutine(AllPhasesTimeout());
        CurrentTimeoutTime = Phase1Time;
        Debug.Log("timeout set to: " + CurrentTimeoutTime);

        d11.gameObject.SetActive(true);

        while (d15 != null)
        {
            yield return null;
        }

        d21.OnControllerEnteredEvent.AddListener(() => Destroy(d21.gameObject));
        d22.OnControllerEnteredEvent.AddListener(() => Destroy(d22.gameObject));

        StopCoroutine(timeoutText);
        TimeoutPaused = true;

        Debug.Log("Good job!");
        ControllerMessageBox.Instance.ShowText("Good job!", 3f, false, true);
        yield return new WaitForSeconds(3f);
        ControllerMessageBox.Instance.ShowText("Phase 2", 3f, false, true);
        Debug.Log("Phase 2");
        yield return new WaitForSeconds(3f);
        ControllerMessageBox.Instance.ShowText("starts in:", 3f, false, true);
        Debug.Log("starts in:");
        yield return new WaitForSeconds(3f);
        var timeRoutine2 = StartCoroutine(TimeoutRoutine(3f, false));
        TimeRoutines.Add(timeRoutine2);
        yield return timeRoutine2;

        timeoutText = StartCoroutine(TimeoutRoutine(Phase2Time, false, true));
        TimeRoutines.Add(timeoutText);

        TimeoutStartTime = Time.time;
        CurrentTimeoutTime = Phase2Time;
        TimeoutPaused = false;
        Debug.Log("timeout set to: " + CurrentTimeoutTime);

        d21.gameObject.SetActive(true);
        d22.gameObject.SetActive(true);

        while (d21 != null || d22 != null)
        {
            yield return null;
        }

        d23.OnControllerEnteredEvent.AddListener(() => Destroy(d23.gameObject));
        d24.OnControllerEnteredEvent.AddListener(() => Destroy(d24.gameObject));

        d23.gameObject.SetActive(true);
        d24.gameObject.SetActive(true);

        while (d23 != null || d24 != null)
        {
            yield return null;
        }

        d25.OnControllerEnteredEvent.AddListener(() => Destroy(d25.gameObject));
        d25.gameObject.SetActive(true);

        while (d25 != null)
        {
            yield return null;
        }

        StopCoroutine(timeoutText);
        TimeoutPaused = true;

        Debug.Log("Good job!");
        ControllerMessageBox.Instance.ShowText("Good job!", 3f, false, true);
        yield return new WaitForSeconds(3f);
        Debug.Log("Phase 3");
        ControllerMessageBox.Instance.ShowText("Phase 3", 3f, false, true);
        yield return new WaitForSeconds(3f);
        Debug.Log("starts in:");
        ControllerMessageBox.Instance.ShowText("starts in:", 3f, false, true);
        yield return new WaitForSeconds(3f);
        var timeRoutine3 = StartCoroutine(TimeoutRoutine(3f, false));
        TimeRoutines.Add(timeRoutine3);
        yield return timeRoutine3;

        timeoutText = StartCoroutine(TimeoutRoutine(Phase3Time, false, true));
        TimeRoutines.Add(timeoutText);

        TimeoutStartTime = Time.time;
        CurrentTimeoutTime = Phase3Time;
        TimeoutPaused = false;
        Debug.Log("timeout set to: " + CurrentTimeoutTime);

        d31.OnControllerEnteredEvent.AddListener(() => Destroy(d31.gameObject));
        d32.OnControllerEnteredEvent.AddListener(() => Destroy(d32.gameObject));
        d33.OnControllerEnteredEvent.AddListener(() => Destroy(d33.gameObject));
        d34.OnControllerEnteredEvent.AddListener(() => Destroy(d34.gameObject));
        d35.OnControllerEnteredEvent.AddListener(() => Destroy(d35.gameObject));

        d31.gameObject.SetActive(true);
        d32.gameObject.SetActive(true);
        d33.gameObject.SetActive(true);
        d34.gameObject.SetActive(true);
        d35.gameObject.SetActive(true);

        while (d31 != null || d32 != null || d33 != null || d34 != null || d35 != null)
        {
            yield return null;
        }

        Debug.Log("complete routine ended!");
        StopCoroutine(timeoutText);
        StationCompleted();
        StopCoroutine(timeout);
    }

    private float TimeoutStartTime;
    private float CurrentTimeoutTime;
    private bool TimeoutPaused;

    private IEnumerator AllPhasesTimeout()
    {
        TimeoutStartTime = Time.time;

        while (Time.time <= TimeoutStartTime + CurrentTimeoutTime)
        {
            while (TimeoutPaused)
            {
                yield return null;
            }

            yield return null;
        }

        StationFailed();
    }

    private void StopAllCoroutines()
    {
        foreach (var routine in TimeRoutines)
        {
            if (routine != null)
            {
                StopCoroutine(routine);
            }
        }

        TimeRoutines.Clear();
    }
}
