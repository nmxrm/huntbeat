﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Waypoint : MonoBehaviour
{
    public static List<Waypoint> Waypoints = new List<Waypoint>();

    public UnityEvent OnPreyEnteredWaypoint = new UnityEvent();

    public int AdditionalTime = 3;

    [HideInInspector]
    public Waypoint Successor;

    [HideInInspector] public MovementInTimeStation CurrentConnectedStation;

    [SerializeField]
    private bool SetInactiveOnStart = false;

    void Awake()
    {
        Waypoints.Add(this);

        if (SetInactiveOnStart)
        {
            gameObject.SetActive(false);
        }
    }

    void OnEnable()
    {
        OnPreyEnteredWaypoint.AddListener(AddTimeToCurrentStation);
    }

    void OnDisable()
    {
        OnPreyEnteredWaypoint.RemoveListener(AddTimeToCurrentStation);
    }

    private void AddTimeToCurrentStation()
    {
        if (Successor && CurrentConnectedStation)
        {
            CurrentConnectedStation.CalculateAndAddAdditionalSeconds(transform.position, Successor);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var possiblePrey = other.GetComponentInParent<Player>();

        if (possiblePrey != null && possiblePrey.playerType == Player.PlayerType.Prey)
        {
            OnPreyEnteredWaypoint.Invoke();

            gameObject.SetActive(false);
        }
    }

    public static Waypoint GetCurrentlyActiveWaypoint()
    {
        foreach (var waypoint in Waypoints)
        {
            if (waypoint.gameObject.activeSelf)
            {
                return waypoint;
            }
        }

        return null;
    }

    public static void ResetSuccessorAndStationOnAllWaypoints()
    {
        foreach (var waypoint in Waypoints)
        {
            waypoint.Successor = null;
            waypoint.CurrentConnectedStation = null;
        }
    }
}
