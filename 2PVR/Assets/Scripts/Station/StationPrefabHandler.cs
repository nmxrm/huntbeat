﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Networking;

public class StationPrefabHandler : MonoBehaviour
{
    public enum StationSpawnBehaviour
    {
        Default,
        //SpawnSpecificStation,
        SpawnSpecificStationEverywhere
    }

    public enum StationType
    {
        DestroyInTime,
        MovementInTime,
        TetrisPuzzle
    }

    public static StationPrefabHandler Instance;

    public List<Station> StationPrefabs = new List<Station>();

    private List<Station> UsedStations = new List<Station>();
    private List<Station> UnusedStations = new List<Station>();

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        Station[] stations = new Station[StationPrefabs.Count];
        StationPrefabs.CopyTo(stations);
        UnusedStations = stations.ToList();
    }

    //void Start()
    //{
    //    SpawnStationsOnStart();
    //}

    public IEnumerator SpawnStationsOnStart()
    {
        yield return null;

        for (int i = 0; i < 3; i++)
        {
            var spawnPoint = StationSpawnPoint.GetRandomUnusedSpawnPoint();
            var station = Instantiate(GetRandomUnusedStationPrefab(), spawnPoint.GetGroundPosition(), spawnPoint.transform.rotation);
            NetworkServer.Spawn(station.gameObject);
        }
    }

    public IEnumerator SpawnOnlyStationsOfType<TStation>()
    {
        yield return null;

        var points = StationSpawnPoint.GetCopyOfSpawnPoints();

        for (int i = 0; i < points.Length; i++)
        {
            var spawnPoint = points[i];
            var station = Instantiate(GetStationPrefabByType<TStation>(), spawnPoint.GetGroundPosition(), spawnPoint.transform.rotation);
            NetworkServer.Spawn(station.gameObject);
        }
    }

    public Station GetStationPrefabByType<TStation>()
    {
        foreach (var stationPrefab in StationPrefabs)
        {
            if (stationPrefab is TStation)
            {
                return stationPrefab;
            }
        }

        return null;
    }

    public Station GetRandomUnusedStationPrefab()
    {
        var i = Random.Range(0, UnusedStations.Count);
        var station = UnusedStations[i];
        UnusedStations.RemoveAt(i);
        UsedStations.Add(station);

        return station;
    }

    public Station GetRandomStationPrefab()
    {
        return StationPrefabs[Random.Range(0, StationPrefabs.Count)];
    }
}
