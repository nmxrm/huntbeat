﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SingletonObject : MonoBehaviour
{
    public static Dictionary<string, GameObject> Singletons;

    [SerializeField]
    private bool DeactivateOnAwake;

    void Awake()
    {
        if (Singletons == null)
        {
            Singletons = new Dictionary<string, GameObject>();
        }

        GameObject go;

        if (!Singletons.TryGetValue(gameObject.name, out go))
        {
            Singletons.Add(gameObject.name, gameObject);
        }

        if (DeactivateOnAwake)
        {
            gameObject.SetActive(false);
        }
    }

    void OnDestroy()
    {
        Singletons.Remove(gameObject.name);
    }

    public static GameObject GetSingletonByPartialName(string partialName)
    {
        var keys = Singletons.Keys.ToList();
        var values = Singletons.Values.ToList();

        for (int i = 0; i < keys.Count; i++)
        {
            if (keys[i].Contains(partialName))
            {
                return values[i];
            }
        }

        return null;
    }
}
