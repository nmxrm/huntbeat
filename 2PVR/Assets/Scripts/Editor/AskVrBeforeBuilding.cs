﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.SceneManagement;
using UnityEngine;

class AskVrBeforeBuilding : IPreprocessBuild
{
    public int callbackOrder { get; private set; }

    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        Debug.Log("Setting vr sdk order to: None; OpenVR");
        UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Standalone, new[] { "None", "OpenVR" });

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/MainMenu.unity");

        var settings = GameObject.Find("GameSettings").GetComponent<GameSettings>();

        if (EditorUtility.DisplayDialog("VR", "Do you want to build as VR app?", "VR", "Normal"))
        {
            if (settings)
            {
                settings.VR = true;
            }
        }
        else
        {
            if (settings)
            {
                settings.VR = false;
            }
        }

        settings.SpawnBehaviour = StationPrefabHandler.StationSpawnBehaviour.Default;
        Debug.Log("Spawn behaviour set to default.");
    }
}
#endif