﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public static class SetVrSdkOrderOnPlay
{
    static SetVrSdkOrderOnPlay()
    {
        EditorApplication.playModeStateChanged += SetVrSdkOrder;
    }

    private static void SetVrSdkOrder(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredEditMode)
        {
            Debug.Log("Setting vr sdk order to: None; OpenVR");
            UnityEditorInternal.VR.VREditor.SetVREnabledDevicesOnTargetGroup(BuildTargetGroup.Standalone, new[] { "None", "OpenVR" });
        }
    }
}
#endif