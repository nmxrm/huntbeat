﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Editor = UnityEditor.Editor;

[InitializeOnLoadAttribute]
public static class PlayMainMenuScene
{
    static PlayMainMenuScene()
    {
        EditorApplication.playModeStateChanged += ReloadPreviousScene;
    }

    [MenuItem("Play/Play-Stop MainMenu scene")]
    public static void LoadAndPlayMainMenuScene()
    {
        if (EditorApplication.isPlaying)
        {
            EditorApplication.isPlaying = false;
            return;
        }

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorPrefs.SetString("prevScenePath", EditorSceneManager.GetActiveScene().path);

        EditorSceneManager.OpenScene("Assets/Scenes/MainMenu.unity");

        EditorPrefs.SetBool("sceneLaunched", true);
        EditorApplication.isPlaying = true;
    }

    static void ReloadPreviousScene(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredEditMode && EditorPrefs.GetBool("sceneLaunched"))
        {
            EditorPrefs.SetBool("sceneLaunched", false);

            EditorSceneManager.OpenScene(EditorPrefs.GetString("prevScenePath"));
        }
    }
}
