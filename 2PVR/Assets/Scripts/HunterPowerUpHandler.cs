﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HunterPowerUpHandler : MonoBehaviour
{
    public static HunterPowerUpHandler Instance;

    public static List<HunterPowerUp> ActivePowerUps = new List<HunterPowerUp>();

    public HunterPowerUp HunterPowerUpGameObject;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
    }

    public void InstantiatePowerUps()
    {
        HunterPowerUpSpawnPoint.ResetUsedUnusedSpawnPoints();

        for (int i = 0; i < 3; i++)
        {
            Instantiate(HunterPowerUpGameObject, HunterPowerUpSpawnPoint.GetRandomUnusedSpawnPoint().GetGroundPosition() + Vector3.up,
                Quaternion.identity);
        }
    }

    public static void DestroyAllActivePowerUps()
    {
        foreach (var powerUp in ActivePowerUps)
        {
            Destroy(powerUp.gameObject);
        }
    }
}
