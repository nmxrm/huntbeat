﻿using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.Vive;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

// obsolete i think
public class SceneHandler : MonoBehaviour
{
    public static SceneHandler Instance;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void RestartScene()
    {
        Destroy(NetworkManager.singleton.gameObject);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        Destroy(ViveInput.Instance.gameObject);

        StartCoroutine(ReloadSceneAfterUnloading());
    }

    private IEnumerator ReloadSceneAfterUnloading()
    {
        var sceneName = SceneManager.GetActiveScene().name;
        var unloadOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());

        while (!unloadOperation.isDone)
        {
            yield return null;

        }

        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
