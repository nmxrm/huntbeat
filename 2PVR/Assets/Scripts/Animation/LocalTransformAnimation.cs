﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LocalTransformAnimation : MonoBehaviour
{
    // this might have to a networkbehaviour
    public float PositionAnimationDelay;
    public float RotationAnimationDelay;
    public float ScaleAnimationDelay;

    public Vector3 TargetPosition;
    public float PositionTime;
    public AnimationCurve PositionCurve;
    public UnityEvent OnPositionAnimationStarted = new UnityEvent();
    public UnityEvent OnPositionAnimationEnded = new UnityEvent();

    public Vector3 TargetRotation;
    public float RotationTime;
    public AnimationCurve RotationCurve;
    public UnityEvent OnRotationAnimationStarted = new UnityEvent();
    public UnityEvent OnRotationAnmiationEnded = new UnityEvent();

    public Vector3 TargetScale;
    public float ScaleTime;
    public AnimationCurve ScaleCurve;
    public UnityEvent OnScaleAnimationStarted = new UnityEvent();
    public UnityEvent OnScaleAnmiationEnded = new UnityEvent();

    private Vector3 StartPosition;
    private Quaternion StartRotation;
    private Vector3 StartScale;

    void Start()
    {
        StartPosition = transform.localPosition;
        StartRotation = transform.localRotation;
        StartScale = transform.localScale;
    }

    [ContextMenu("Play position animation")]
    public void PlayPositionAnimation()
    {
        StartCoroutine(PositionAnimationRoutine());
    }

    [ContextMenu("Play rotation animation")]
    public void PlayRotationAnimation()
    {
        StartCoroutine(RotationAnimationRoutine());
    }

    [ContextMenu("Play scale animation")]
    public void PlayScaleAnimation()
    {
        StartCoroutine(ScaleAnimationRoutine());
    }

    private IEnumerator ScaleAnimationRoutine()
    {
        yield return new WaitForSeconds(ScaleAnimationDelay);

        float i = 0;
        float rate = 1 / ScaleTime;

        OnScaleAnimationStarted.Invoke();

        while (i < 1)
        {
            transform.localScale = Vector3.Lerp(StartScale, TargetScale, ScaleCurve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        transform.localScale = TargetScale;

        OnScaleAnmiationEnded.Invoke();
    }

    private IEnumerator PositionAnimationRoutine()
    {
        yield return new WaitForSeconds(PositionAnimationDelay);

        float i = 0;
        float rate = 1 / PositionTime;

        //StartPosition = transform.localPosition;

        OnPositionAnimationStarted.Invoke();

        while (i < 1)
        {
            transform.localPosition = Vector3.Lerp(StartPosition, TargetPosition, PositionCurve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        transform.localPosition = TargetPosition;

        OnPositionAnimationEnded.Invoke();
    }

    private IEnumerator RotationAnimationRoutine()
    {
        yield return new WaitForSeconds(RotationAnimationDelay);

        float i = 0;
        float rate = 1 / RotationTime;

        OnRotationAnimationStarted.Invoke();

        //StartRotation = transform.localRotation;
        var targetRotation = Quaternion.Euler(TargetRotation);

        while (i < 1)
        {
            transform.localRotation = Quaternion.Lerp(StartRotation, targetRotation, RotationCurve.Evaluate(i));
            i += Time.deltaTime * rate;
            yield return null;
        }

        transform.localRotation = targetRotation;

        OnRotationAnmiationEnded.Invoke();
    }

    public void ResetPosition()
    {
        transform.localPosition = StartPosition;
    }

    public void ResetRotation()
    {
        transform.localRotation = StartRotation;
    }

    public void ResetScale()
    {
        transform.localScale = StartScale;
    }
}
