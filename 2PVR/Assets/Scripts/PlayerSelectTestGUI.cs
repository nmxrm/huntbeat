﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSelectTestGUI : MonoBehaviour
{
    public NetworkManagerHUD hud;

    public bool show = true;

    private void OnGUI()
    {
        if (show)
        {
            if (GUI.Button(new Rect(new Vector2(Screen.width / 2 - 120, Screen.height / 2), new Vector2(100f, 50f)), "Hunter"))
            {
                GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Hunter;
                hud.showGUI = true;
                show = false;
            }

            if (GUI.Button(new Rect(new Vector2(Screen.width / 2, Screen.height / 2), new Vector2(100f, 50f)), "Prey"))
            {
                GameSettings.Instance.SelectedPlayerType = Player.PlayerType.Prey;
                hud.showGUI = true;
                show = false;
            }
        }
    }


}
