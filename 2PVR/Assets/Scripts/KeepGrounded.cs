﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepGrounded : MonoBehaviour
{
    public GameObject RaycastObject;

    void Update()
    {
        if (RaycastObject)
        {
            Ray ray = new Ray(RaycastObject.transform.position, Vector3.down);
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo);

            if (hitInfo.collider!=null)
            {
                transform.position = hitInfo.point;
            }
        }
    }
}
